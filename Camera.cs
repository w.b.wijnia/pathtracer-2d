﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Raytracer2.Scalar;

namespace Raytracer2
{
    public struct Camera
    {
        /// <summary>
        /// Represents the origin of the camera.
        /// </summary>
        public Point2 origin;

        /// <summary>
        /// Represents the half extend of the camera. The origin is in the center of the viewing box.
        /// </summary>
        public Vector2 halfExtend;

        public Camera (Point2 origin, Vector2 halfExtend)
        {
            this.origin = origin;
            this.halfExtend = halfExtend;
        }
    }
}
