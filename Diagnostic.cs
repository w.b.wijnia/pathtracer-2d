﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raytracer2
{
    public static class Diagnostic
    {
        public static long TimeClear;
        public static long TimeRender;
        public static long TimeAccelerationStructure;
        public static long TimeHierarchy;
        public static long TimeToSurface;
        public static long TimeToOpenGL;
        public static long TimeOther;

        public static int NumberOfRays;
        public static int NumberOfSkippedRays;
        public static int CircleCollisions;
        public static int LineSegmentCollisions;

        public static int NumberOfCircleCollisionChecks;
        public static int NumberOfBoundingBoxCollisionChecks;

        public static int timeTotal;
        public static int timeDiffuse;
        public static int timeLighting;
        public static int timeCombined;
        public static int timeBoundingVolumeHierarchy;

        public static void Reset()
        {
            TimeClear = 0;
            TimeRender = 0;
            TimeAccelerationStructure = 0;
            TimeToSurface = 0;
            TimeOther = 0;
            TimeToOpenGL = 0;
            TimeHierarchy = 0;

            timeTotal = 0;
            timeDiffuse = 0;
            timeLighting = 0;
            timeCombined = 0;
            timeBoundingVolumeHierarchy = 0;
        }

        public static void WriteToConsole()
        {
            Console.Clear();
            Console.SetCursorPosition(0, 0);

            Console.WriteLine("  Timings  ");
            Console.WriteLine();
            Console.WriteLine("Total:           (ms) " + timeTotal.ToString());
            Console.WriteLine(" - Render:       (ms) " + TimeRender.ToString());
            Console.WriteLine(" - - diffuse:    (ms) " + (timeDiffuse).ToString());
            Console.WriteLine(" - - lighting:   (ms) " + (timeLighting).ToString());
            Console.WriteLine(" - - combined:   (ms) " + (timeCombined).ToString());
 
            Console.WriteLine(" - Acceleration: (ms) " + TimeAccelerationStructure.ToString());
            Console.WriteLine(" - Hierarchy:    (ms) " + TimeHierarchy.ToString());
            Console.WriteLine(" - Other:        (ms) " + TimeOther.ToString());
        }
    }
}