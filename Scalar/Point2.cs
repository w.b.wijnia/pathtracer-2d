﻿
using System.Text;

using Raytracer2.Scalar.Primitives;

namespace Raytracer2.Scalar
{
    public struct Point2
    {
        /// <summary>
        /// represents the x coordinate.
        /// </summary>
        public float x { get; set; }

        /// <summary>
        /// represents the y coordinate.
        /// </summary>
        public float y { get; set; }

        /// <summary>
        /// constructs a point with the given x and y coordinate.
        /// </summary>
        public Point2(float x, float y)
        {
            this.x = x;
            this.y = y;
        }

        public Point2(Vector2 vector)
        {
            this.x = vector.x;
            this.y = vector.y;
        }

        public static Point2 Origin()
        {
            return new Point2(0, 0);
        }

        #region Functions

        /// <summary>
        /// returns a vector with the same coordinates as the points.
        /// </summary>
        public Vector2 ToVector()
        { return new Vector2(this.x, this.y); }

        /// <summary>
        /// Computes the vector that points from this point towards the target.
        /// </summary>
        public Vector2 DirectionTo(Point2 target)
        { return new Vector2(target.x - this.x, target.y - this.y); }

        /// <summary>
        /// Computes the distance to the other point.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public float DistanceTo(Point2 other)
        { return Point2.Distance(this, other); }

        /// <summary>
        /// Computes the squared distance to the other point.
        /// </summary>
        public float SquaredDistanceTo(Point2 other)
        { return Point2.DistanceSquared(this, other); }

        /// <summary>
        /// Computes the distance between the two given points.
        /// </summary>
        public static float Distance(Point2 p1, Point2 p2)
        { return p1.DirectionTo(p2).Length(); }

        /// <summary>
        /// Computes the squared distance between the two given points.
        /// </summary>
        public static float DistanceSquared(Point2 p1, Point2 p2)
        {
            return p1.DirectionTo(p2).LengthSquared();
        }

        #endregion

        #region Operators

        /// <summary>
        /// Adds a vector to this point.
        /// </summary>
        public static Point2 operator +(Point2 p, Vector2 v)
        { return new Point2(p.x + v.x, p.y + v.y); }

        /// <summary>
        /// Removes a vector from this point.
        /// </summary>
        public static Point2 operator -(Point2 p, Vector2 v)
        { return new Point2(p.x - v.x, p.y - v.y); }

        #endregion

        #region Intersection functions

        public bool IsInsidePrimitive(Point2 other)
        {
            return DistanceSquared(this, other) < 0.001f;
        }

        public bool IsInsidePrimitive(Circle other)
        {
            return other.ContainsPoints(this);
        }

        public bool IsInsidePrimitive(LineSegment other)
        {
            return other.ContainsPoints(this);
        }

        public bool IsInsidePrimitive(RectangleAxis other)
        {
            return other.ContainsPoints(this);
        }

        public bool IsInsidePrimitive(RectangleOriented other)
        {
            return other.ContainsPoints(this);
        }

        #endregion

        /// <summary>
        /// Rotates the point by n degrees around the origin.
        /// </summary>
        public void Rotate(float n)
        {
            // compute the amount of radians and the unit circle height / width.
            float radians = (float)((n / 360) * System.Math.PI * 2);
            float sine = (float)System.Math.Sin(radians);
            float cosine = (float)System.Math.Cos(radians);

            x = this.x * cosine - this.y * sine;
            y = this.x * sine + this.y * cosine;
        }

        /// <summary>
        /// Computes a rotated point of the point by n degrees, around the origin.
        /// </summary>
        public Point2 Rotated(float n)
        {
            // compute the amount of radians and the unit circle height / width.
            float radians = (float)((n / 360) * System.Math.PI * 2);
            float sine = (float)System.Math.Sin(radians);
            float cosine = (float)System.Math.Cos(radians);

            return new Point2(
                this.x * cosine - this.y * sine,
                this.x * sine + this.y * cosine
                );
        }

        #region Geometric functions

        public Point2[] GeometricShape()
        { return new Point2[] { this }; }

        #endregion

        public override string ToString()
        {
            // saves us precious computation time and memory!
            StringBuilder sb = new StringBuilder();

            // build up a string version of the vector.
            sb.Append("(");
            sb.Append(x.ToString());
            sb.Append(", ");
            sb.Append(y.ToString());
            sb.Append(")");

            // and return it!
            return sb.ToString();
        }

    }
}