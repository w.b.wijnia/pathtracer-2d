﻿
using Raytracer2.Scalar.Acceleration;
using Raytracer2.Scalar.Primitives;

namespace Raytracer2.Scalar
{
    public struct RenderComponent : iBoundingBox, iPrimitive
    {
        /// <summary>
        /// Represents the primitive that this graphics element contains.
        /// </summary>
        public Circle c;

        /// <summary>
        /// Represents the material used by this primitve.
        /// </summary>
        public Material m;

        public RenderComponent(Circle c, Material m)
        {
            this.c = c;
            this.m = m;
        }

        public bool CollidesWith(BoundingBox bb)
        {
            return c.CollidesWith(bb);
        }

        public BoundingBox ComputeBoundingBox()
        { return c.ComputeBoundingBox(); }

        public bool ContainsPoints(Point2 p)
        {
            return c.ContainsPoints(p);
        }

        public bool Intersect(ref Ray ray)
        { return c.Intersect(ref ray); }

        public Point2 RandomLocationInPrimitive(iRandom rng)
        { return c.RandomLocationInPrimitive(rng); }

        ///// <summary>
        ///// Represents a set of visual effects.
        ///// 1 = Cast shadows            (lighting kernel)
        ///// 2 = Illuminates             (lighting kernel)
        ///// 3 = Visible to camera       (diffuse kernel)
        ///// </summary>
        //internal int flags;

        //private const int flagCastShadows = 1;
        //private const int flagIlluminates = 2;
        //private const int flagVisibleToCamera = 3;

        //#region Flags

        //public void SetCastShadows(bool b)
        //{ SetFlag(b, flagCastShadows); }

        //public void SetIllumination(bool b)
        //{ SetFlag(b, flagIlluminates); }

        //public void SetVisibleToCamera(bool b)
        //{ SetFlag(b, flagVisibleToCamera); }

        //private void SetFlag(bool b, int i)
        //{
        //    if (b) { flags = flags | (1 << i); }
        //    else { flags = flags & (int.MaxValue - (1 << i)); }
        //}

        //public bool CheckCastShadows()
        //{ return CheckFlag(flagCastShadows); }

        //public bool CheckIllumination()
        //{ return CheckFlag(flagIlluminates); }

        //public bool CheckVisibileToCamera()
        //{ return CheckFlag(flagVisibleToCamera); }

        //private bool CheckFlag(int i)
        //{
        //    return false;
        //}

        //#endregion

    }
}
