﻿
using System.Text;

namespace Raytracer2.Scalar
{
    public struct Vector2
    {
        /// <summary>
        /// represents the x coordinate.
        /// </summary>
        public float x { get; set; }

        /// <summary>
        /// represents the y coordinate.
        /// </summary>
        public float y { get; set; }

        /// <summary>
        /// constructs a point with the given x and y coordinate.
        /// </summary>
        public Vector2(float x, float y)
        {
            this.x = x;
            this.y = y;
        }

        public static Vector2 Origin()
        {
            return new Vector2(0, 0);
        }


        #region Functionality

        /// <summary>
        /// Performs the dot product between this vector and vector v.
        /// </summary>
        public float Dot(Vector2 v)
        { return this * v; }

        /// <summary>
        /// Computes the length of the vector.
        /// </summary>
        public float Length()
        { return (float)System.Math.Sqrt(this.LengthSquared()); }

        /// <summary>
        /// Computes the squared length of the vector.
        /// </summary>
        public float LengthSquared()
        { return this * this; }

        /// <summary>
        /// Normalizes the vector.
        /// </summary>
        public void Normalize()
        {
            // compute the length.
            float invLength = 1 / this.Length();

            // multiply it with our components.
            this.x = x * invLength;
            this.y = y * invLength;
        }

        /// <summary>
        /// Returns a normalized version of this vector.
        /// </summary>
        /// <returns></returns>
        public Vector2 Normalized()
        {
            // compute the length.
            float invLength = 1 / this.Length();

            // return a new vector with this vector's components multiplied by the scalar invLength.
            return invLength * this;
        }

        /// <summary>
        /// Rotates the vector by n degrees.
        /// </summary>
        public void Rotate(float n)
        {
            // compute the amount of radians and the unit circle height / width.
            float radians = (float)((n / 360) * System.Math.PI * 2);
            float sine = (float)System.Math.Sin(radians);
            float cosine = (float)System.Math.Cos(radians);

            // some weird math - figure this out!
            x = this.x * cosine - this.y * sine;
            y = this.x * sine + this.y * cosine;
        }

        /// <summary>
        /// Computes a rotated vector of the vector by n degrees.
        /// </summary>
        public Vector2 Rotated(float n)
        {
            // compute the amount of radians and the unit circle height / width.
            float radians = (float)((n / 360) * System.Math.PI * 2);
            float sine = (float)System.Math.Sin(radians);
            float cosine = (float)System.Math.Cos(radians);

            // some weird math - figure this out!
            return new Vector2(
                this.x * cosine - this.y * sine,
                this.x * sine + this.y * cosine
                );
        }

        /// <summary>
        /// projects this vector onto the other.
        /// </summary>
        public void Project(Vector2 other)
        {
            // compute the squared length of the vector to project on.
            float d = other * other;
            float dp = this * other;

            // determine the scaling factor.
            float factor = dp / d;

            // scale us!
            this.x = other.x * factor;
            this.y = other.y * factor;
        }

        /// <summary>
        /// returns the projected version of the vector onto the other.
        /// </summary>
        public Vector2 Projected(Vector2 other)
        {
            // compute the squared length of the vector to project on.
            float d = other * other;
            float dp = this * other;

            // determine the scaling factor.
            float factor = dp / d;

            return new Vector2(other.x * factor, other.y * factor);
        }

        /// <summary>
        /// Computes and returns an orthogonal vector to the vector.
        /// </summary>
        /// <returns></returns>
        public Vector2 OrthogonalVector()
        {  return new Vector2(this.y, -this.x); }

        #endregion

        #region Operators


        /// <summary>
        /// Multiplies vector v with matrix m.
        /// </summary>
        public static Vector2 operator *(Matrix2x2 m, Vector2 v)
        { return new Vector2(v * m.GetRow(0), v * m.GetRow(1)); }

        /// <summary>
        /// Adds vector v1 to vector v2.
        /// </summary>
        public static Vector2 operator +(Vector2 v1, Vector2 v2)
        { return new Vector2(v1.x + v2.x, v1.y + v2.y); }

        /// <summary>
        /// Subtracts vector v2 from vector v1.
        /// </summary>
        public static Vector2 operator -(Vector2 v1, Vector2 v2)
        { return new Vector2(v1.x - v2.x, v1.y - v2.y); }

        /// <summary>
        /// Performs the dot product between vector v1 and vector v2.
        /// </summary>
        public static float operator * (Vector2 v1, Vector2 v2)
        { return v1.x * v2.x + v1.y * v2.y; }

        /// <summary>
        /// Multiplies the vector v with scalar s.
        /// </summary>
        public static Vector2 operator * (float s, Vector2 v)
        { return new Vector2(v.x * s, v.y * s); }

        #endregion

        #region Conversions

        /// <summary>
        /// Converts this vector into a float array.
        /// </summary>
        /// <returns></returns>
        public float[] ToArray()
        { return new float[] { x, y }; }

        /// <summary>
        /// Converts this vector into a string representation.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            // saves us precious computation time and memory!
            StringBuilder sb = new StringBuilder();

            // build up a string version of the vector.
            sb.Append("{");
            sb.Append(x.ToString());
            sb.Append(", ");
            sb.Append(y.ToString());
            sb.Append("}");

            // and return it!
            return sb.ToString();
        }

        #endregion

    }
}