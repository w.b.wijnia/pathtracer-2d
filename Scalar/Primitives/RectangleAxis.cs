﻿

namespace Raytracer2.Scalar.Primitives
{
    public class RectangleAxis : iPrimitive
    {
        /// <summary>
        /// Represents the origin of this primitive.
        /// </summary>
        public Point2 origin;

        /// <summary>
        /// represents the size of the rectangle.
        /// </summary>
        public Vector2 halfExtend { get; set; }

        /// <summary>
        /// Constructs a rectangle with the given origin and half extend.
        /// </summary>
        public RectangleAxis(Point2 origin, Vector2 halfExtend)
        {
            this.origin = origin;
            this.halfExtend = halfExtend;
        }

        /// <summary>
        /// Constructs a rectangle from the given x and y range.
        /// </summary>
        public RectangleAxis(Range xs, Range ys)
        {
            this.origin = new Point2(
                xs.min + 0.5f * (xs.max - xs.min),
                ys.min + 0.5f * (ys.max - ys.min));

            this.halfExtend = new Vector2(
                0.5f * (xs.max - xs.min),
                0.5f * (ys.max - ys.min));
        }

        #region Collision / intersection functions

        /// <summary>
        /// Computes whether or not the given ray and this primitive intersect, and if so where they intersect.
        /// </summary>
        public bool Intersect(ref Ray ray)
        { return ray.Intersect(this); }

        /// <summary>
        /// Computes whether or not this and the given primitive collide.
        /// </summary>
        public bool CollidesWith(Circle other)
        {
            // if the center is inside the rectangle, then we collide!
            if (this.ContainsPoints(other.origin))
                return true;

            // otherise, check all the distances of the segments to the circle!
            Point2[] segments = this.ComputeVerticesCCW();

            if (new LineSegment(segments[0], segments[1]).CollidesWith(other))
                return true;

            if (new LineSegment(segments[1], segments[3]).CollidesWith(other))
                return true;

            if (new LineSegment(segments[2], segments[3]).CollidesWith(other))
                return true;

            if (new LineSegment(segments[0], segments[2]).CollidesWith(other))
                return true;

            return false;
        }

        /// <summary>
        /// Computes whether or not this and the given primitive collide.
        /// </summary>
        public bool CollidesWith(Line other)
        {
            // Compute the points of the rectangle.
            Point2[] rectangleVertices = this.ComputeVerticesCCW();

            // take one direction (origin -> point 0) as a basis!
            Vector2 perpendicularDirection = other.direction.OrthogonalVector();

            float comparisionDot = perpendicularDirection * other.origin.DirectionTo(rectangleVertices[0]);

            // then for every other direction (origin -> point i), check the dot product!
            for (int j = 1, l = rectangleVertices.Length; j < l; j++)
            {
                float result = comparisionDot * (other.origin.DirectionTo(rectangleVertices[j]).Dot(perpendicularDirection));
                if (result < 0)
                    return true;
            }

            // if all are positive, it means all points are on one side!
            return false;
        }

        /// <summary>
        /// Computes whether or not this and the given primitive collide.
        /// </summary>
        public bool CollidesWith(LineSegment other)
        {
            // Compute the points of the rectangle.
            Point2[] pRectangle = this.ComputeVerticesCCW();
            Point2[] pLineSegment = new Point2[] { other.p1, other.p2 };

            // Determine if there is overlap in the x range!
            if (!Range.FromX(pRectangle).Overlap(Range.FromX(pLineSegment)))
                return false;

            // Determine if there is overlap in the y range!
            if (!Range.FromY(pRectangle).Overlap(Range.FromY(pLineSegment)))
                return false;

            // If there is overlap in both, check the points of the rectangle with the dot product!
            return this.CollidesWith(new Line(other));
        }

        /// <summary>
        /// Computes whether or not this and the given primitive collide.
        /// </summary>
        public bool CollidesWith(RectangleAxis other)
        {
            // compute the horizontal ranges.
            Range aHorizontal = new Range(this.origin.x - this.halfExtend.x, this.origin.x + this.halfExtend.x);
            Range bHorizontal = new Range(other.origin.x - other.halfExtend.x, other.origin.x + other.halfExtend.x);

            // compute the vertical ranges.
            Range aVertical = new Range(this.origin.y - this.halfExtend.y, this.origin.y + this.halfExtend.y);
            Range bVertical = new Range(other.origin.y - other.halfExtend.y, other.origin.y + other.halfExtend.y);

            // see if they overlap.
            return aHorizontal.Overlap(bHorizontal) && aVertical.Overlap(bVertical);
        }

        /// <summary>
        /// Computes whether or not this and the given primitive collide.
        /// </summary>
        public bool CollidesWith(RectangleOriented other)
        {
            return other.CollidesWith(this);
        }

        /// <summary>
        /// Computes the vertices (geometry) of this primitive.
        /// </summary>
        /// <returns></returns>
        internal Point2[] ComputeVerticesCCW()
        {
            Point2 lb = this.origin - this.halfExtend;
            Point2 rb = this.origin + new Vector2(this.halfExtend.x, -this.halfExtend.y);
            Point2 rt = this.origin + this.halfExtend;
            Point2 lt = this.origin + new Vector2(-this.halfExtend.x, this.halfExtend.y);

            return new Point2[] { lb, rb, rt, lt };
        }

        #endregion

        /// <summary>
        /// Computes the bounding box of this primitive.
        /// </summary>
        public RectangleAxis ComputeBoundingBox()
        {
            return new RectangleAxis(this.origin, this.halfExtend);
        }

        /// <summary>
        /// Computes a random position within the primitive.
        /// </summary>
        public Point2 RandomLocationInPrimitive(iRandom rng)
        {
            float r1 = (rng.Float() - 0.5f) * 2.0f;
            float r2 = (rng.Float() - 0.5f) * 2.0f;

            return origin + r1 * new Vector2(halfExtend.x, 0) + r2 * new Vector2(0, halfExtend.y);
        }

        /// <summary>
        /// Determines whether the given point is contained by the primitive.
        /// </summary>
        public bool ContainsPoints(Point2 p)
        {
            Point2[] rectangleVertices = this.ComputeVerticesCCW();

            Range rx = Range.FromX(rectangleVertices);
            Range ry = Range.FromY(rectangleVertices);

            if (rx.Contains(p.x) && ry.Contains(p.y))
                return true;
            return false;
        }
    }
}