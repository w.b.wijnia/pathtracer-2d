﻿
namespace Raytracer2.Scalar.Primitives
{
    public interface iPrimitive
    {


        /// <summary>
        /// Computes whether the given ray intersects this shape. If it does, returns information about the intersection.
        /// </summary>
        bool Intersect(ref Ray ray);

        /// <summary>
        /// Computes whether the point is inside the primitive.
        /// </summary>
        bool ContainsPoints(Point2 p);

        /// <summary>
        /// Computes a random position within the primitive.
        /// </summary>
        Point2 RandomLocationInPrimitive(iRandom rng);

    }
}