﻿using System;

using Raytracer2.Scalar;

namespace Raytracer2.Scalar.Primitives
{
    public struct Ray
    {
        /// <summary>
        /// Represents the starting point of the ray.
        /// </summary>
        public Point2 origin;

        /// <summary>
        /// Represents the direction of the ray. This is always normalised.
        /// </summary>
        public Vector2 direction;

        public float t;

        public Ray(Point2 origin, Vector2 direction, float t = float.MaxValue)
        {
            this.origin = origin;
            this.direction = direction;
            this.t = t;
        }

        public bool IntersectSlow(Circle other)
        {
            Vector2 c = this.origin.DirectionTo(other.origin);
            float t = c * this.direction;
            Vector2 q = c - t * this.direction;

            float p2 = q * q;
            float r2 = other.radius * other.radius;
            if (p2 > r2)
            { return false; }

            t -= (float)Math.Sqrt(r2 - p2);

            if ((t < this.t) && (t > 0))
            {
                this.t = t;
                return true;
            }

            return false;
        }

        public bool Intersect(Circle other)
        {
            // Step 0: Work everything out on paper!

            // Step 1: Gather all the relevant data.
            float ox = this.origin.x;
            float dx = this.direction.x;

            float oy = this.origin.y;
            float dy = this.direction.y;

            float x0 = other.origin.x;
            float y0 = other.origin.y;
            float cr = other.radius;

            // Step 2: compute the substitutions.
            float p = ox - x0;
            float q = oy - y0;

            float r = 2 * p * dx;
            float s = 2 * q * dy;

            // Step 3: compute the substitutions, check if there is a collision.
            float a = dx * dx + dy * dy;
            float b = r + s;
            float c = p * p + q * q - cr * cr;

            float DSqrt = b * b - 4 * a * c;

            // no collision possible!
            if (DSqrt < 0)
            { return false; }

            // Step 4: compute the substitutions.
            float D = (float)Math.Sqrt(DSqrt);

            //float t0 = (-b + D) / (2 * a);
            float t1 = (-b - D) / (2 * a);

            //float ti = Math.Min(t0, t1);
            if(t1 > 0 && t1 < t)
            {
                t = t1;
                return true;
            }

            return false;
        }

        public bool Intersect(Line other)
        {
            // Step 0: Work everything out on paper.
            if (this.direction * other.direction == 0)
            {
                return false;
            }

            // Step 1: Gather all the relevant data.
            Matrix2x2 inverse = new Matrix2x2(this.direction, -1 * other.direction).Inversed();
            Vector2 rhs = this.origin.DirectionTo(other.origin);
            Point2 intersection = origin + (inverse * rhs).x * direction;

            // Step 2: Compute intersection
            float ti = (intersection.x - this.origin.x) / this.direction.x;
            if (ti < t)
            {
                t = ti;
                return true;
            }

            return false;
        }

        public bool Intersect(LineSegment other)
        {
            // Step 0: Work everything out on paper!

            // Step 1: Get the proper representations.
            Line line = new Line(other);

            // Step 0: Work everything out on paper.
            if (this.direction * line.direction == 0)
            {
                return false;
            }

            // Step 1: Gather all the relevant data.
            Matrix2x2 inverse = new Matrix2x2(this.direction, line.direction).Inversed();
            Vector2 rhs = this.origin.DirectionTo(other.p1);
            Point2 intersection = origin + (inverse * rhs).x * direction;

            // Step 2: Compute intersection
            bool inBetween = other.p1.DirectionTo(intersection) * other.p2.DirectionTo(intersection) < 0;
            if (!inBetween)
            {
                return false;
            }

            float ti = (intersection.x - this.origin.x) / this.direction.x;
            if (ti < t && ti > 0)
            {
                t = ti;
                return true;
            }

            return false;
        }

        public bool Intersect(RectangleAxis other)
        {
            // The points of the rectangle and the segment that represents the edges of the rectangle.
            LineSegment segment;
            Point2[] ps = other.ComputeVerticesCCW();

            // Go over every segment...
            for (int j = 0; j < 4; j++)
            {
                // Construct the segment, check if it collides.
                segment = new LineSegment(ps[j], ps[(j + 1) % 4]);

                // todo: find smallest t?
                // take note that the t is updated internally in the intersect call.
                if (this.Intersect(segment))
                { return true; }
            }

            return false;
        }

        public bool Intersect(RectangleOriented other)
        {
            // The points of the rectangle and the segment that represents the edges of the rectangle.
            LineSegment segment;
            Point2[] ps = other.ComputeVerticesCCW();

            // Go over every segment...
            for (int j = 0; j < 4; j++)
            {
                // Construct the segment, check if it collides.
                segment = new LineSegment(ps[j], ps[(j + 1) % 4]);

                // todo: find smallest t?
                // take note that the t is updated internally in the intersect call.
                if (this.Intersect(segment))
                { return true;  }
            }

            return false;
        }
    }
}