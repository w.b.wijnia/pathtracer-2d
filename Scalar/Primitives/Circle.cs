﻿
using Raytracer2.Scalar.Acceleration;

namespace Raytracer2.Scalar.Primitives
{
    public struct Circle : iPrimitive, iBoundingBox
    {

        /// <summary>
        /// Represents the origin of this primitive.
        /// </summary>
        public Point2 origin;

        /// <summary>
        /// represents the radius of the circle.
        /// </summary>
        public float radius { get; set; }

        /// <summary>
        /// Constructs a circle.
        /// </summary>
        public Circle(Point2 origin, float radius)
        {
            this.origin = origin;
            this.radius = radius;
        }

        public Point2 RandomLocationInPrimitive(iRandom rng)
        {
            Vector2 dir = new Vector2(rng.Float() - 0.5f, rng.Float() - 0.5f);
            return this.origin + (0.90f * rng.Float() * this.radius) * dir.Normalized();
        }

        public bool ContainsPoints(Point2 p)
        {
            return Point2.DistanceSquared(this.origin, p) < this.radius * this.radius;
        }

        public BoundingBox ComputeBoundingBox()
        {
            BoundingBox bb = new BoundingBox(
                new Range(this.origin.x - this.radius, this.origin.x + this.radius),
                new Range(this.origin.y - this.radius, this.origin.y + this.radius)
                );

            return bb;
        }

        public bool CollidesWith(BoundingBox other)
        {
            // we do not have to be accurate, for now. We do need to be complete.
            BoundingBox bb = this.ComputeBoundingBox();
            return bb.CollidesWith(other);



            // we collide with the circle if we contain its origin.
            if (bb.Contains(this.origin))
            { return true; }

            // check per line segment.

            return false;
        }

        #region Collision / intersection functions

        public bool Intersect(ref Ray ray)
        { return ray.Intersect(this); }

        /// <summary>
        /// Determines whether the given primitive collides with this circle in a discrete manner.
        /// </summary>
        public bool CollidesWith(Circle other)
        {
            // compute the squared distance between the two origins.
            float distance = (this.origin.DirectionTo(other.origin)).LengthSquared();

            // compute the sum of the squared radia.
            float radia = this.radius * this.radius + other.radius * other.radius;

            // if we have more radia than distance, we're colliding!
            return radia >= distance;
        }

        /// <summary>
        /// Determines whether the given primitive collides with this circle in a discrete manner.
        /// </summary>
        public bool CollidesWith(Line other)
        {
            return other.CollidesWith(this);
        }

        /// <summary>
        /// Determines whether the given primitive collides with this circle in a discrete manner.
        /// </summary>
        public bool CollidesWith(LineSegment other)
        {
            return other.CollidesWith(this);
        }

        /// <summary>
        /// Determines whether the given primitive collides with this circle in a discrete manner.
        /// </summary>
        public bool CollidesWith(RectangleAxis other)
        {
            return other.CollidesWith(this);
        }

        /// <summary>
        /// Determines whether the given primitive collides with this circle in a discrete manner.
        /// </summary>
        public bool CollidesWith(RectangleOriented other)
        {
            return other.CollidesWith(this);
        }

        #endregion


        /// <summary>
        /// Returns a string representation of the class.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        { return $"C({origin.ToString()}, {radius.ToString()})"; }
    }
}


