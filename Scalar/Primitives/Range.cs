﻿
using System;

namespace Raytracer2.Scalar.Primitives
{
    public struct Range
    {
        /// <summary>
        /// represents the minimal value of this range.
        /// </summary>
        public float min { get; set; }

        /// <summary>
        /// represents the maximal value of this range.
        /// </summary>
        public float max { get; set; }

        /// <summary>
        /// constructs a simple range object.
        /// </summary>
        public Range(float min, float max)
        {
            this.min = min;
            this.max = max;
        }

        public Range (Range r1, Range r2)
        {
            this.min = r1.min < r2.min ? r1.min : r2.min;
            this.max = r1.max > r2.max ? r1.max : r2.max;
        }

        public static Range Combine(Range a, Range b)
        {
            float min = a.min < b.min ? a.min : b.min;
            float max = a.max > b.max ? a.max : b.max;
            return new Range(min, max);
        }

        /// <summary>
        /// Constructs a range of the x component of the given vectors.
        /// </summary>
        public static Range FromX(Vector2[] vectors)
        {
#if DEBUG
            if (vectors == null || vectors.Length == 0)
                throw new Exception($"Given vector array is null or empty: {vectors}");
#endif
            // copy the values from the first vector.
            Range r = new Range(vectors[0].x, vectors[0].x);

            // go over all the vectors, find a bigger / smaller one.
            foreach(Vector2 v in vectors)
            {
                if (v.x < r.min)
                    r.min = v.x;
                if (v.x > r.max)
                    r.max = v.x;
            }

            // return the range.
            return r;

        }

        /// <summary>
        /// Constructs a range of the y component of the given vectors.
        /// </summary>
        public static Range FromY(Vector2[] vectors)
        {
#if DEBUG
            if (vectors == null || vectors.Length == 0)
                throw new Exception($"Given vector array is null or empty: {vectors}");
#endif
            // copy the values from the first vector.
            Range r = new Range(vectors[0].y, vectors[0].y);

            // go over all the vectors, find a bigger / smaller one.
            foreach (Vector2 v in vectors)
            {
                if (v.y < r.min)
                    r.min = v.y;
                if (v.y > r.max)
                    r.max = v.y;
            }

            // return the range.
            return r;
        }

        /// <summary>
        /// Constructs a range of the y component of the given points.
        /// </summary>
        public static Range FromY(Point2[] points)
        {
#if DEBUG
            if (points == null || points.Length == 0)
                throw new Exception($"Given vector array is null or empty: {points}");
#endif
            // copy the values from the first vector.
            Range r = new Range(points[0].y, points[0].y);

            // go over all the vectors, find a bigger / smaller one.
            foreach (Point2 v in points)
            {
                if (v.y < r.min)
                    r.min = v.y;
                if (v.y > r.max)
                    r.max = v.y;
            }

            // return the range.
            return r;
        }

        /// <summary>
        /// Constructs a range of the x component of the given points.
        /// </summary>
        public static Range FromX(Point2[] points)
        {
#if DEBUG
            if (points == null || points.Length == 0)
                throw new Exception($"Given vector array is null or empty: {points}");
#endif
            // copy the values from the first vector.
            Range r = new Range(points[0].x, points[0].x);

            // go over all the vectors, find a bigger / smaller one.
            foreach (Point2 v in points)
            {
                if (v.x < r.min)
                    r.min = v.x;
                if (v.x > r.max)
                    r.max = v.x;
            }

            // return the range.
            return r;
        }

        /// <summary>
        /// Ensures the minimal is always smaller than the maximum.
        /// </summary>
        public void CheckRange()
        {
            if (this.min > this.max)
            {
                float local = min;
                min = max;
                max = local;
            }
        }

        public float Distance()
        { return max - min; }

        public float DistanceFrom(float t)
        {
            float d1 = Math.Abs(max - t);
            float d2 = Math.Abs(min - t);
            return Math.Min(d1, d2);
        }

        public float Centroid()
        {
            return (min + max) * 0.5f;
        }

        public bool Contains(float v)
        { return min <= v && v <= max; }

        /// <summary>
        /// Checks whether the given ranges overlap.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Overlap(Range other)
        { return other.min <= this.max && this.min <= other.max; }

        public override string ToString()
        { return $"({min}, {max})"; }

    }
}