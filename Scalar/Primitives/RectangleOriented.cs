﻿
using System;
using System.Linq;

namespace Raytracer2.Scalar.Primitives
{
    public class RectangleOriented : iPrimitive
    {
        /// <summary>
        /// Represents the origin of this primitive.
        /// </summary>
        public Point2 origin;

        /// <summary>
        /// represents the size of the rectangle, in both directions.
        /// </summary>
        public Vector2 halfExtend { get; set; }

        /// <summary>
        /// represents the rotation of the rectangle.
        /// </summary>
        public float rotation { get; set; }

        public RectangleOriented(Point2 center, Vector2 halfExtend, float rotation)
        {
            this.origin = center;
            this.halfExtend = halfExtend;
            this.rotation = rotation;
        }

        public RectangleOriented(RectangleAxis rectangle)
        {
            this.origin = rectangle.origin;
            this.halfExtend = rectangle.halfExtend;
            this.rotation = 0.0f;
        }

        #region Collision / intersection functions

        public bool Intersect(ref Ray ray)
        { return ray.Intersect(this); }

        public bool CollidesWith(Circle other)
        {
            Point2 adjustedOrigin = other.origin - Point2.Origin().DirectionTo(this.origin);
            Point2 rotatedOrigin = adjustedOrigin.Rotated(-this.rotation) + this.halfExtend;

            Circle adjusted = new Circle(rotatedOrigin, other.radius);
            RectangleAxis rectangleAA = this.ToRectangleAxis2D(new Point2(0, 0));

            return rectangleAA.CollidesWith(adjusted);
        }

        public bool CollidesWith(Line other)
        {
            Point2 adjustedOrigin = other.origin - Point2.Origin().DirectionTo(this.origin);
            Point2 rotatedOrigin = adjustedOrigin.Rotated(-this.rotation) + this.halfExtend;
            Vector2 rotatedDirection = other.direction.Rotated(-this.rotation);

            Line adjusted = new Line(rotatedOrigin, rotatedDirection);
            RectangleAxis rectangleAA = this.ToRectangleAxis2D(new Point2(0, 0));

            return rectangleAA.CollidesWith(adjusted);
        }


        public bool CollidesWith(LineSegment other)
        {
            Point2 adjustedP1 = other.p1 - Point2.Origin().DirectionTo(this.origin);
            Point2 rotatedP1 = adjustedP1.Rotated(-this.rotation) + this.halfExtend;

            Point2 adjustedP2 = other.p2 - Point2.Origin().DirectionTo(this.origin); 
            Point2 rotatedP2 = adjustedP2.Rotated(-this.rotation) + this.halfExtend;

            LineSegment adjusted = new LineSegment(rotatedP1, rotatedP2);
            RectangleAxis rectangleAA = this.ToRectangleAxis2D(new Point2(0, 0));

            return rectangleAA.CollidesWith(adjusted);
        }

        public bool CollidesWith(RectangleAxis other)
        {
            RectangleOriented rectangleO = new RectangleOriented(other);
            return rectangleO.CollidesWith(this);
        }

        public bool CollidesWith(RectangleOriented other)
        {
            // Compute axis of rectangle 1.
            Line r1a1, r1a2;
            this.CollisionAxis(out r1a1, out r1a2);

            // Compute axis of rectangle 2.
            Line r2a1, r2a2;
            other.CollisionAxis(out r2a1, out r2a2);

            // project them!
            if (!this.ProjectOntoAxis(r1a1).Overlap(other.ProjectOntoAxis(r1a1)))
                return false;

            if (!this.ProjectOntoAxis(r1a2).Overlap(other.ProjectOntoAxis(r1a2)))
                return false;

            if (!this.ProjectOntoAxis(r2a1).Overlap(other.ProjectOntoAxis(r2a1)))
                return false;

            if (!this.ProjectOntoAxis(r2a2).Overlap(other.ProjectOntoAxis(r2a2)))
                return false;

            return true;
        }

        private RectangleAxis ToRectangleAxis2D(Point2 center)
        { return new RectangleAxis(center, 2.0f * this.halfExtend); }


        /// <summary>
        /// Retrieves a single single edge as a segment.
        /// </summary>
        /// <param name="n"></param>
        /// <returns></returns>
        private LineSegment GetEdge(int n)
        {
            // the points of the line segment!
            Vector2 p1 = new Vector2(halfExtend.x, halfExtend.y);
            Vector2 p2 = new Vector2(halfExtend.x, halfExtend.y);

            // find the coordinates of the selected edge.
            switch (n)
            {
                // top horizontal edge.
                case 0:
                    p1.x = -p1.x;
                    break;

                // right vertical edge.
                case 1:
                    p2.y = -p2.y;
                    break;

                // left vertical edge.
                case 2:
                    p1.y = -p1.y;
                    p2.x = -p2.x;
                    p2.y = -p2.y;
                    break;

                // bottom horizontal edge.
                case 3:
                    p2.x = -p2.x;
                    p1.x = -p1.x;
                    p1.y = -p1.y;
                    break;

                // What? :)
                default:
                    throw new Exception($"Unknown edge: {n}. Needs to be between 0 and 3.");
            }

            // rotate them the same way the box is!
            p1.Rotate(rotation);
            p2.Rotate(rotation);

            return new LineSegment(origin + p1, origin + p2);
        }

        private Range ProjectOntoAxis(Line axis)
        {
            // compute the intersections.
            Point2[] intersections = ProjectOntoAxisIntersections(axis);

            // compute the distances to the origin of the axis.
            float[] distances = new float[intersections.Length];
            for (int j = 0, l = intersections.Length; j < l; j++)
                distances[j] = axis.origin.DistanceTo(intersections[j]) * Math.Sign(axis.origin.DirectionTo(intersections[j]) * axis.direction);

            // sort them. (ouch?)
            distances = distances.OrderBy(x => x).ToArray();

            // create the range!
            Range r = new Range(distances[0], distances[intersections.Length - 1]);
            return r;
        }

        private Point2[] ProjectOntoAxisIntersections(Line axis)
        {
            // retrieve the points.
            Point2[] ps = this.ComputeVerticesCCW();
            Point2[] intersections = new Point2[ps.Length];

            // compute the intersection points.
            for (int j = 0, l = ps.Length; j < l; j++)
            {
                Point2 p = ps[j];
                intersections[j] = axis.ClosestPointOnLine(p);
            }

            return intersections;
        }

        private void CollisionAxis(out Line axis1, out Line axis2)
        {
            Vector2 p1 = new Vector2(-halfExtend.x, halfExtend.y).Rotated(rotation);
            Vector2 p2 = new Vector2(halfExtend.x, -halfExtend.y).Rotated(rotation);
            Vector2 he = this.halfExtend.Rotated(rotation);

            axis1 = new Line(new LineSegment(origin + he, origin + p1));
            axis2 = new Line(new LineSegment(origin + he, origin + p2));
        }

        internal Point2[] ComputeVerticesCCW()
        {
            Point2 lb = this.origin - this.halfExtend.Rotated(rotation);
            Point2 rb = this.origin - new Vector2(this.halfExtend.x, -this.halfExtend.y).Rotated(rotation);
            Point2 rt = this.origin + this.halfExtend.Rotated(rotation);
            Point2 lt = this.origin - new Vector2(-this.halfExtend.x, this.halfExtend.y).Rotated(rotation);


            return new Point2[] { lb, rb, rt, lt };
        }

        #endregion

        public RectangleAxis ComputeBoundingBox()
        {
            throw new NotImplementedException();
        }

        public Point2 RandomLocationInPrimitive(iRandom rng)
        {
            throw new NotImplementedException();

            //float r1 = ((float)rng.NextDouble() - 0.5f) * 2.0f;
            //float r2 = ((float)rng.NextDouble() - 0.5f) * 2.0f;

            //Vector2 v1 = origin.DirectionTo(origin + new Vector2(size.x, 0));
            //Vector2 v2 = origin.DirectionTo(origin + new Vector2(0, size.y));

            //return origin + r1 * v1 + r2 * v2;
        }

        public bool ContainsPoints(Point2 p)
        {
            // bring it into the local space of the cube!
            Point2 adjusted = this.origin - this.origin.DirectionTo(p);
            Point2 rotated = adjusted.Rotated(-this.rotation) + this.halfExtend;
            RectangleAxis rectangleAA = this.ToRectangleAxis2D(new Point2(0, 0));

            return rectangleAA.ContainsPoints(rotated);
        }

    }
}