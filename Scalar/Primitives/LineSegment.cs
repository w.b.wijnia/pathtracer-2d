﻿
namespace Raytracer2.Scalar.Primitives
{
    public class LineSegment : iPrimitive
    {
        /// <summary>
        /// Represents the origins of this primitive.
        /// </summary>
        public Point2 p1, p2;

        public LineSegment(Point2 p1, Point2 p2)
        {
            this.p1 = p1;
            this.p2 = p2;
        }

        #region Collision / intersection functions

        public bool Intersect(ref Ray ray)
        { return ray.Intersect(this); }

        public bool CollidesWith(Circle other)
        {
            if (other.ContainsPoints(this.p1))
                return true;

            if (other.ContainsPoints(this.p2))
                return true;

            Line line = new Line(this);
            Point2 closest = line.ClosestPointOnLine(other.origin);

            if ((closest.SquaredDistanceTo(other.origin) > other.radius * other.radius))
                return false;

            return this.p1.DirectionTo(closest) * this.p2.DirectionTo(closest) < 0;
        }

        public bool CollidesWith(Line other)
        {
            return !other.SegmentOnOneSide(this);
        }

        public bool CollidesWith(LineSegment other)
        {
            // check whether the other segment crosses our 'line'.
            Line axisA = new Line(this);
            if (axisA.SegmentOnOneSide(other))
                return false;

            // check whether our segment crosses the other 'line'.
            Line axisB = new Line(other);
            if (axisB.SegmentOnOneSide(this))
                return false;

            return true;
        }

        public bool CollidesWith(RectangleAxis other)
        {
            return other.CollidesWith(this);
        }

        public bool CollidesWith(RectangleOriented other)
        {
            return other.CollidesWith(this);
        }

        #endregion

        public RectangleAxis ComputeBoundingBox()
        {
            float xmin = p1.x < p2.x ? p1.x : p2.x;
            float xmax = p1.x > p2.x ? p1.x : p2.x;
            float ymin = p1.y < p2.y ? p1.y : p2.y;
            float ymax = p1.y > p2.y ? p1.y : p2.y;

            Point2 origin = new Point2(xmin, ymin);
            Vector2 size = origin.DirectionTo(new Point2(xmax, ymax));

            return new RectangleAxis(origin, size);
        }

        public Point2 RandomLocationInPrimitive(iRandom rng)
        {
            float r1 = rng.Float();
            float r2 = 1 - r1;
            return new Point2(r1 * p1.x + r2 * p2.x, r1 * p1.y + r2 * p2.y);
        }

        public bool ContainsPoints(Point2 p)
        {
            return false;
            //// check whether the point is anywhere near the line!
            //Line2 line = new Line2(this);
            //if (!line.Contains(p))
            //    return false;

            //// check if it's between the two points!
            //return this.P1.DirectionTo(p) * this.P2.DirectionTo(p) < 0;
        }
    }
}