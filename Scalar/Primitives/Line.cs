﻿
using System;
using System.Text;

namespace Raytracer2.Scalar.Primitives
{
    public class Line : iPrimitive
    {
        public Point2 origin;

        /// <summary>
        /// represents the direction of the line.
        /// </summary>
        public Vector2 direction { get; set; }

        /// <summary>
        /// Constructs a line from an origin and a direction.
        /// </summary>
        public Line(Point2 origin, Vector2 direction)
        {
            this.origin = origin;
            this.direction = direction;
        }

        /// <summary>
        /// Constructs a line from a line segment.
        /// </summary>
        public Line(LineSegment segment)
        {
            this.origin = segment.p1;
            this.direction = segment.p1.DirectionTo(segment.p2).Normalized();
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("[o: ");
            sb.Append(origin.ToString());
            sb.Append(", d: ");
            sb.Append(direction.ToString());
            sb.Append("]");

            return sb.ToString();
        }

        public bool ContainsPoints(Point2 other)
        {
            Point2 closest = this.ClosestPointOnLine(other);
            return closest.IsInsidePrimitive(other);
        }

        public Point2 RandomLocationInPrimitive(iRandom rng)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Checks whether the entire segment is on one side of the line (axis).
        /// </summary>
        /// <param name="segment"></param>
        /// <returns></returns>
        internal bool SegmentOnOneSide(LineSegment segment)
        {
            Vector2 normal = this.direction.OrthogonalVector();
            Vector2 p1ToOrigin = origin.DirectionTo(segment.p1);
            Vector2 p2ToOrigin = origin.DirectionTo(segment.p2);

            return 0 < (normal * p1ToOrigin) * (normal * p2ToOrigin);
        }

        /// <summary>
        /// Computes the closest point on this line for given point p.
        /// </summary>
        internal Point2 ClosestPointOnLine(Point2 p)
        {
            Vector2 CenterToOrigin = this.origin.DirectionTo(p);
            Vector2 ProjectionOntoAxis = CenterToOrigin.Projected(this.direction);
            return this.origin + ProjectionOntoAxis;
        }

        /// <summary>
        /// Computes the intersection point between this line and the given line.
        /// </summary>
        private Point2 LineIntersectionPoint(Line other)
        {
            Matrix2x2 inverse = new Matrix2x2(this.direction, other.direction).Inversed();
            Vector2 p3 = this.origin.DirectionTo(other.origin);

            return origin + (inverse * p3).x * direction;
        }

        #region Collision / intersection functions

        /// <summary>
        /// Computes whether or not the given ray and this primitive intersect, and if so where they intersect.
        /// </summary>
        public bool Intersect(ref Ray ray)
        { return ray.Intersect(this); }

        /// <summary>
        /// Computes whether the given line and line intersect. If the angle is small, they are considered parallel.
        /// </summary>
        public bool CollidesWith(Line other)
        {
            // the lines are too parallel - they do intersect but very, very far way!
            return Math.Abs(this.direction * other.direction) > 0.04f;
        }

        /// <summary>
        /// Computes whether the given line and circle intersect.
        /// </summary>
        public bool CollidesWith(Circle other)
        {
            Point2 closest = this.ClosestPointOnLine(other.origin);
            return Point2.DistanceSquared(closest, other.origin) < other.radius * other.radius;
        }

        /// <summary>
        /// Computes whether the given line and line segment intersect.
        /// </summary>
        public bool CollidesWith(LineSegment other)
        { return other.CollidesWith(this); }


        /// <summary>
        /// Computes whether the given line and rectangle intersect.
        /// </summary>
        public bool CollidesWith(RectangleAxis other)
        { return other.CollidesWith(this); }

        /// <summary>
        /// Computes whether the given line and oriented rectangle collide.
        /// </summary>
        public bool CollidesWith(RectangleOriented other)
        { return other.CollidesWith(this); }

        #endregion

    }
}