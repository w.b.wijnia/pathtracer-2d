﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Raytracer2.Scalar.Primitives;

namespace Raytracer2.Scalar
{
    internal class Hierarchy
    {

        private RenderComponent[] occluders;

        private RenderComponent[] lightSources;

        private int numberOfOccluders, numberOfLightSources;

        public Hierarchy(int n)
        {
            this.occluders = new RenderComponent[n];
            this.lightSources = new RenderComponent[n];
        }

        public void Construct(Span<RenderComponent> rcs)
        {
            numberOfOccluders = 0;
            numberOfLightSources = 0;

            // determine the number of entities of a given category.
            for (int j = 0, l = rcs.Length; j < l; j++)
            {
                RenderComponent rc = rcs[j];

                // keep track of the occluding category.
                if (true)
                {
                    occluders[numberOfOccluders] = rc;
                    numberOfOccluders++;
                }

                // keep track of the light category.
                if (rc.m.emissive > 0.01f)
                {
                    lightSources[numberOfLightSources] = rc;
                    numberOfLightSources++;
                }
            }
        }

        public Span<RenderComponent> GetOccluders
        { get { return new Span<RenderComponent>(occluders, 0, numberOfOccluders); } }

        public Span<RenderComponent> GetLightSources
        { get { return new Span<RenderComponent>(lightSources, 0, numberOfLightSources); } }
    }
}