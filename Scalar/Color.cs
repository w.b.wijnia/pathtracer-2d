﻿using System;

using System.Runtime.InteropServices;

namespace Raytracer2.Scalar
{
    public struct Color
    {
        /// <summary>
        /// Represents the components (red, green, blue, alpha) of this color.
        /// </summary>
        public float r, g, b, a;

        public Color(float r, float g, float b)
        {
            this.r = r;
            this.g = g;
            this.b = b;
            this.a = 0.0f;
        }

        public float Length()
        {
            float re = r;
            if (g > re)
            {
                re = g;
            }

            if (b > re)
            {
                re = b;
            }

            return re;
        }

        /// <summary>
        /// Computes the color where both colors are added together. Does not normalize the color.
        /// </summary>
        /// <returns></returns>
        public static Color operator + (Color c1, Color c2)
        {
            return new Color(c1.r + c2.r, c1.g + c2.g, c1.b + c2.b);
        }

        /// <summary>
        /// Computes the color that is multiplied by the scalar. Does not normalize the color.
        /// </summary>
        /// <returns></returns>
        public static Color operator * (float f, Color c)
        {
            return new Color(f * c.r, f * c.g, f * c.b);
        }

        /// <summary>
        /// Computes the color that is multiplied by the scalar. Does not normalize the color.
        /// </summary>
        /// <returns></returns>
        public static Color operator * (Color c1, Color c2)
        {
            return new Color(c1.r * c2.r, c1.g * c2.g, c1.b * c2.b);
        }

        public Color(int color)
        {
            throw new NotImplementedException();
        }

        public float[] ToArray()
        {
            return new float[] { r, g, b };
        }

        public int ToInt()
        {
            int ri = (int)(r * 255);
            int gi = (int)(g * 255);
            int bi = (int)(b * 255);

            return (ri << 16) + (gi << 8) + bi;
        }

        public override string ToString()
        {
            return $"({r}, {b}, {g})";
        }
    }
}