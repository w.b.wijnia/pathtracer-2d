﻿
using System.Threading;

namespace Raytracer2.Scalar
{
    public struct Material
    {
        public Color diffuse { get; set; }

        /// <summary>
        /// If the emission is higher than a given threshold, the material is considered to emit light.
        /// </summary>
        public float emissive { get; set; }

        public Material(Color diffuse)
        {
            this.diffuse = diffuse;
            this.emissive = 0.0f;
        }

        public Material(Color diffuse, float emissive)
        {
            this.diffuse = diffuse;
            this.emissive = emissive;
        }
    }
}