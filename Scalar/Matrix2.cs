﻿namespace Raytracer2.Scalar
{
    public class Matrix2x2
    {
        /// <summary>
        /// represents the actual matrix.
        /// </summary>
        private float[] data;

        private Matrix2x2(float[] data)
        {
            this.data = data;
        }

        public Matrix2x2(Vector2 c1, Vector2 c2)
        {
            this.data = new float[4]
            {
                c1.x, c2.x,
                c1.y, c2.y
            };
        }

        /// <summary>
        /// Solves the given Ax = b equation!
        /// </summary>
        public Vector2 Solve(Vector2 rightSide)
        {
            Matrix2x2 inverse = this.Inversed();
            return inverse * rightSide;
        }

        /// <summary>
        /// Computes the inversed matrix.
        /// </summary>
        public Matrix2x2 Inversed()
        {
            float determinant = this.Determinant();
            return (1 / determinant) * new Matrix2x2(new float[4] { data[3], -data[1], -data[2], data[0] });
        }

        /// <summary>
        /// Computes the determinant of the matrix.
        /// </summary>
        /// <returns></returns>
        private float Determinant()
        { return data[0] * data[3] - data[1] * data[2]; }

        /// <summary>
        /// Computes the index.
        /// </summary>
        private int GetIndex(int x, int y)
        { return x + y * 2; }

        /// <summary>
        /// Retrieves the given row.
        /// </summary>
        public Vector2 GetRow(int y)
        { return new Vector2(data[GetIndex(0, y)], data[GetIndex(1, y)]); }

        /// <summary>
        /// Retrieves the given column.
        /// </summary>
        public Vector2 GetColumn(int x)
        { return new Vector2(data[GetIndex(x, 0)], data[GetIndex(x, 1)]); }

        /// <summary>
        /// Adds matrix m2 to matrix m1.
        /// </summary>
        public static Matrix2x2 operator +(Matrix2x2 m1, Matrix2x2 m2)
        {
            float[] data = new float[4];
            for (int j = 0; j < 4; j++)
                data[j] = m1.data[j] + m2.data[j];

            return new Matrix2x2(data);
        }

        /// <summary>
        /// Subtracts matrix m2 from matrix m1.
        /// </summary>
        public static Matrix2x2 operator -(Matrix2x2 m1, Matrix2x2 m2)
        {
            float[] data = new float[4];
            for (int j = 0; j < 4; j++)
                data[j] = m1.data[j] - m2.data[j];

            return new Matrix2x2(data);
        }

        /// <summary>
        /// Multiplies matrix m1 with matrix m2.
        /// </summary>
        public static Matrix2x2 operator *(Matrix2x2 m1, Matrix2x2 m2)
        {
            float[] data = new float[4];

            for (int y = 0; y < 2; y++)
                for (int x = 0; x < 2; x++)
                    data[x + y * 2] = m1.GetRow(y) * m2.GetColumn(x);

            return new Matrix2x2(data);
        }

        /// <summary>
        /// Multiples matrix m with scalar s.
        /// </summary>
        public static Matrix2x2 operator *(float scalar, Matrix2x2 m)
        {
            float[] data = new float[4];

            for (int j = 0; j < 4; j++)
                data[j] = m.data[j] * scalar;

            return new Matrix2x2(data);
        }
    }
}