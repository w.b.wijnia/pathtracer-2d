﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raytracer2.Scalar.Acceleration
{
    public struct BoundingBoxOriented
    {

        /// <summary>
        /// Represents the origin of this primitive.
        /// </summary>
        public Point2 origin;

        /// <summary>
        /// represents the size of the rectangle, in both directions.
        /// </summary>
        public Vector2 halfExtend { get; set; }

        /// <summary>
        /// represents the rotation of the rectangle.
        /// </summary>
        public float rotation { get; set; }

    }
}
