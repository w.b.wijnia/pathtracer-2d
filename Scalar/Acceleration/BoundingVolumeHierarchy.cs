﻿
using System;
using System.Collections.Generic;

using Raytracer2.Scalar.Primitives;

namespace Raytracer2.Scalar.Acceleration
{
    public class BoundingVolumeHierarchy<T> where T : iPrimitive, iBoundingBox
    {
        private const int leafThreshold = 8 ;

        internal enum Axis { X, Y };

        /// <summary>
        /// Represents the list of primitives.
        /// </summary>
        internal RenderComponent[] primitives;

        internal RenderComponent[] efficientPrimitives;

        /// <summary>
        /// Represents the cached bounding boxes of the primitives.
        /// </summary>
        internal BoundingBox[] containers;

        /// <summary>
        /// Represents the available BVH nodes.
        /// </summary>
        internal BoundingNode[] nodePool;

        public BoundingVolumeHierarchy(Span<RenderComponent> ps)
        { this.Reconstruct(ps); }

        public void Refit()
        { throw new NotImplementedException(); }

        public void Reconstruct(Span<RenderComponent> ps)
        {
            // toooddoooo
            this.primitives = ps.ToArray();


            this.containers = ConstructContainers(primitives);
            this.nodePool = ConstructPool(primitives, containers);
            this.efficientPrimitives = ConstructEfficientMemoryStructure(primitives, containers, nodePool);
        }

        private RenderComponent[] ConstructEfficientMemoryStructure(RenderComponent[] ps, BoundingBox[] containers, BoundingNode[] nodePool)
        {
            RenderComponent[] efficient = new RenderComponent[ps.Length];
            for(int j = 0; j < nodePool.Length; j++)
            {
                BoundingNode node = nodePool[j];
                if (node.Leaf)
                {
                    for (int i = node.First, l = node.First + node.Count; i < l; i++)
                    {
                        BoundingBox bb = containers[i];
                        efficient[i] = ps[bb.primitive];
                    }
                }
            }

            return efficient;
        }

        private BoundingBox[] ConstructContainers(RenderComponent[] ps)
        {
            BoundingBox[] bcs = new BoundingBox[ps.Length];
            for (int j = 0, l = ps.Length; j < l; j++)
            {
                RenderComponent component = ps[j];
                BoundingBox bb = component.c.ComputeBoundingBox();
                bb.primitive = j;
                bcs[j] = bb;
            }

            return bcs;
        }

        private BoundingNode[] ConstructPool(RenderComponent[] ps, BoundingBox[] bs)
        {
            int pLength = ps.Length;

            // initialize all of the nodes.
            int freeInPool = 1;
            BoundingNode[] nodePoolTemp = new BoundingNode[pLength * 2 - 1];
            Axis[] sortAxis = new Axis[pLength * 2 - 1];

            // construct the root.
            sortAxis[0] = Axis.X;
            nodePoolTemp[0] = new BoundingNode
            {
                Left = 0,
                First = 0,
                Count = pLength,
                BoundingBox = BoundingBox.Combine(bs)
            };

            for (int j = 0; j < freeInPool; j++)
            {
                // retrieve the (new) root. If the root is a leaf, then we're done.
                BoundingNode root = nodePoolTemp[j];
                Axis rootAxis = sortAxis[j];

                Axis nextAxis = Axis.X;
                if (rootAxis == Axis.X)
                    nextAxis = Axis.Y;

                // if we're a leaf then we do not have any children to reference to. Hence, Left will be 0.
                // if we're not a leaf, then we allocate where our left child will be. Later we'll do the actual allocating.
                if (root.Left == -1)
                { continue; }
                else
                { nodePoolTemp[j].Left = freeInPool; }

                // sorting ... boxes. Marie Kondo, anyone?
                SortBoundingBoxes(bs, rootAxis, root.First, root.Count);

                BoundingBox bbLeft, bbRight;
                int split = MedianSplit(bs, root.First, root.Count, out bbLeft, out bbRight);

                // construct the children. If they are a leaf, copy the boundingbox (bb).
                BoundingNode childLeft = new BoundingNode();
                childLeft.First = root.First;
                childLeft.Count = split;
                childLeft.BoundingBox = bbLeft;

                if (childLeft.Count < leafThreshold)
                { childLeft.Left = -1; }

                BoundingNode childRight = new BoundingNode();
                childRight.First = root.First + (split);
                childRight.Count = root.Count - (split);
                childRight.BoundingBox = bbRight;

                if (childRight.Count < leafThreshold)
                { childRight.Left = -1; }

                // add the children to the pool and allocate their locations. Take note that this is
                // where the allocation actually happens (referencing to an earlier comment)
                nodePoolTemp[freeInPool] = childLeft;
                sortAxis[freeInPool] = nextAxis;
                freeInPool++;
                nodePoolTemp[freeInPool] = childRight;
                sortAxis[freeInPool] = nextAxis;
                freeInPool++;
            }

            BoundingNode[] nodePool = new BoundingNode[freeInPool];
            for (int j = 0; j < freeInPool; j++)
            { nodePool[j] = nodePoolTemp[j]; }

            return nodePool;
        }

        public bool OnNode(Point2 p)
        {
            foreach (BoundingNode node in nodePool)
                if (node.BoundingBox.OnEdge(p))
                    return true;
            return false;
        }

        public bool Contains(ref Point2 p, out RenderComponent component)
        {
            component = new RenderComponent();

            // keep track of the nodes we are (possibly) contained by.
            Stack<int> poolPointers = new Stack<int>();
            poolPointers.Push(0);

            // for all nodes.
            while (poolPointers.Count != 0)
            {
                // find the actual bb, see if we're contained.
                int index = poolPointers.Pop();
                BoundingNode node = nodePool[index];
                BoundingBox bb = node.BoundingBox;

                if (bb.Contains(p))
                {
                    if (node.Leaf)
                    {
                        for (int j = node.First, l = node.First + node.Count; j < l; j++)
                        {
                            RenderComponent o = efficientPrimitives[j];
                            if (o.c.ContainsPoints(p))
                            {
                                // hooray!
                                component = o;
                                return true;
                            }
                        }
                    }
                    else
                    {
                        // if we are contained and the node is not a leaf, continue our journey.
                        poolPointers.Push(node.Left);
                        poolPointers.Push(node.Right);
                    }
                }
            }

            return false;
        }

        public bool Traverse(ref Ray r)
        {

            // construct the stack for keeping track of the nodes we hit. Add in the origin node.
            Stack<int> poolPointers = new Stack<int>();
            poolPointers.Push(0);

            // as long as we've hit unprocessed nodes...
            while (poolPointers.Count != 0)
            {
                // retrieve the node, check if the ray collides with it.
                int index = poolPointers.Pop();
                BoundingNode node = nodePool[index];
                BoundingBox bb = node.BoundingBox;
                bool collidesWithRay = bb.CollidesWith(r, out float tbb);
                bool containsRay = bb.Contains(r.origin);
                if ((collidesWithRay && tbb < r.t && tbb > 0) || containsRay)
                {
                    // if it does and it's a leaf, then we can check the primitive.
                    if (node.Leaf)
                    {
                        for (int j = node.First, l = node.First + node.Count; j < l; j++)
                        {
                            RenderComponent o = efficientPrimitives[j];
                            if(o.c.Intersect(ref r))
                            { return true; }
                        }
                    }
                    // if it does and it's a node / branch, then we add the children to the stack.
                    else
                    {
                        poolPointers.Push(node.Left);
                        poolPointers.Push(node.Right);
                    }
                }
            }

            return false;
        }

        private int MedianSplit(BoundingBox[] bbs, int First, int Count, out BoundingBox bbLeft, out BoundingBox bbRight)
        {
            int c = (Count >> 1);
            bbLeft = BoundingBox.Combine(bbs, First, c);
            bbRight = BoundingBox.Combine(bbs, First + c, Count - c);
            return c;
        }

        private int SAHSplit(BoundingBox[] bbs, int First, int Count, out BoundingBox bbLeft, out BoundingBox bbRight)
        {
            int lowestIndex = -1;
            float lowestValue = float.MaxValue;

            bbLeft = new BoundingBox();
            bbRight = new BoundingBox();

            for (int j = 1; j < Count; j++)
            {
                BoundingBox leftbb = BoundingBox.Combine(bbs, First, j);
                BoundingBox rightbb = BoundingBox.Combine(bbs, First + j, Count - j);

                float leftCosts = leftbb.ComputeArea() * j;
                float rightCosts = rightbb.ComputeArea() * (Count - j);

                float SAH = leftCosts + rightCosts;
                if (SAH < lowestValue)
                {
                    lowestIndex = j;
                    lowestValue = SAH;
                    bbLeft = leftbb;
                    bbRight = rightbb;
                }
            }

            return lowestIndex;
        }

        private int SAHSplitBinning(BoundingBox[] bbs, int First, int Count, out BoundingBox bbLeft, out BoundingBox bbRight)
        {
            // ------------------------------------------------------- \\
            // Determine the size of the bins.                         \\

            int numberOfBins = Math.Min(10, Count);
            int[] binSizes = new int[numberOfBins];

            float cCount = 0;
            float fCount = (float)Count / numberOfBins;
            for (int j = 0; j < numberOfBins; j++)
            {
                cCount += fCount;
                binSizes[j] = (int)Math.Round(cCount);
            }

            // ------------------------------------------------------- \\
            // Default values                                          \\

            int lowestIndex = -1;
            float lowestValue = float.MaxValue;

            bbLeft = new BoundingBox();
            bbRight = new BoundingBox();

            // ------------------------------------------------------- \\
            // Apply the SAH algorithm over the bins.                  \\

            for (int j = 0; j < numberOfBins - 1; j++)
            {
                int split = binSizes[j];

                BoundingBox leftbb = BoundingBox.Combine(bbs, First, split);
                BoundingBox rightbb = BoundingBox.Combine(bbs, First + split, Count - split);

                float leftCosts = leftbb.ComputeArea() * j;
                float rightCosts = rightbb.ComputeArea() * (Count - j);

                float SAH = leftCosts + rightCosts;
                if (SAH < lowestValue)
                {
                    lowestIndex = split;
                    lowestValue = SAH;
                    bbLeft = leftbb;
                    bbRight = rightbb;
                }
            }

            return lowestIndex;
        }

        private void SortBoundingBoxes(BoundingBox[] bbs, Axis a, int f, int c)
        {
            // this node is a leaf!
            if (c == 1)
            { return; }

            for (int j = f; j < f + c; j++)
            { SortStoreBoundingBoxes(bbs, a, f, j, j); }
        }

        private void SortStoreBoundingBoxes(BoundingBox[] bbs, Axis a, int l, int h, int i)
        {
            // keep track of the element we want to store.
            BoundingBox target = bbs[i];

            // find out where we need to store it. As long as the element we find is smaller
            // than our target, we keep searching.
            BoundingBox element = bbs[l];

            if (a == Axis.X)
            {
                float tRCX = target.rx.Centroid();
                float tECX = element.rx.Centroid();
                while (tRCX.CompareTo(tECX) < 0)
                {
                    l = l + 1;
                    element = bbs[l];

                    // we cannot search beyond the ordered section.
                    if (l >= h)
                        break;

                    tECX = element.rx.Centroid();
                }
            }
            else
            {
                float tRCY = target.ry.Centroid();
                float tECY = element.ry.Centroid();
                while (tRCY.CompareTo(tECY) < 0)
                {
                    l = l + 1;
                    element = bbs[l];

                    // we cannot search beyond the ordered section.
                    if (l >= h)
                        break;

                    tECY = element.ry.Centroid();
                }
            }

            // shift all of the elements one place to the right to make space.
            SortShiftElements(bbs, l, (h - l));

            // store the element where it belongs.
            bbs[l] = target;
        }

        private void SortShiftElements(BoundingBox[] a, int l, int c)
        {
            // shift the elements. Take note that if c == 0, no elements are shifted.
            for (int j = c + l; j > l; j--)
                a[j] = a[j - 1];
        }
    }
}
