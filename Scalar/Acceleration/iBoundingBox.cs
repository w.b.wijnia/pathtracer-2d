﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raytracer2.Scalar.Acceleration
{
    public interface iBoundingBox
    {

        BoundingBox ComputeBoundingBox();

        bool CollidesWith(BoundingBox bb);

    }
}
