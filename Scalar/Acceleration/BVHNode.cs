﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raytracer2.Scalar.Acceleration
{
    public struct BoundingNode
    {
        /// <summary>
        /// Represents the boundingbox of this node.
        /// </summary>
        public BoundingBox BoundingBox { get; set; }            // 16 bytes

        /// <summary>
        /// Represents the left child of this node. If set to -1, then this node has no children.
        /// </summary>
        public int Left;                                        // 4 bytes

        /// <summary>
        /// Represents a set of elements within the boundingbox array (primitive array on the GPU).
        /// </summary>
        public int First, Count;                                // 8 bytes

        /// <summary>
        /// A node is a leaf when 'Left' is equal to -1.
        /// </summary>
        public bool Leaf
        { get { return Left == -1; } }

        public int Right
        { get { return Left + 1; } }

        public override string ToString()
        { return $"{Left}, {First} -> {First + Count}, {BoundingBox}"; }

    }
}
