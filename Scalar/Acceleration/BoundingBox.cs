﻿using System;

using Raytracer2.Scalar.Primitives;

namespace Raytracer2.Scalar.Acceleration
{
    public struct BoundingBox
    {
        private const float debugRadius = 0.055f;

        /// <summary>
        /// Represents the range on the x-axis.
        /// </summary>
        public Range rx { get; set; }

        /// <summary>
        /// Represents the range on the y-axis.
        /// </summary>
        public Range ry { get; set; }

        /// <summary>
        /// Represents the index of the primitive this boundingbox supports.
        /// </summary>
        public int primitive;

        /// <summary>
        /// Constructs a bounding box with the given ranges.
        /// </summary>
        public BoundingBox(Range xs, Range ys)
        {
            this.rx = xs;
            this.ry = ys;
            this.primitive = -1;
        }

        /// <summary>
        /// Combines the two given bounding boxes.
        /// </summary>
        public static BoundingBox Combine(BoundingBox a, BoundingBox b)
        {
            return new BoundingBox(
                Range.Combine(a.rx, b.rx),
                Range.Combine(a.ry, b.ry)
                );
        }

        /// <summary>
        /// Combines the given bounding boxes.
        /// </summary>
        public static BoundingBox Combine(BoundingBox[] bbs)
        { return Combine(bbs, 0, bbs.Length); }

        /// <summary>
        /// Combines the given bounding boxes.
        /// </summary>
        public static BoundingBox Combine(BoundingBox[] bbs, int first, int count)
        {
            float minX = float.MaxValue;
            float maxX = float.MinValue;

            float minY = float.MaxValue;
            float maxY = float.MinValue;

            for (int j = first, l = first + count; j < l; j++)
            {
                Range xs = bbs[j].rx;
                if (xs.min < minX)
                {
                    minX = xs.min;
                }

                if (xs.max > maxX)
                {
                    maxX = xs.max;
                }

                Range ys = bbs[j].ry;
                if (ys.min < minY)
                {
                    minY = ys.min;
                }

                if (ys.max > maxY)
                {
                    maxY = ys.max;
                }
            }

            return new BoundingBox(
                new Range(minX, maxX),
                new Range(minY, maxY)
                );
        }

        /// <summary>
        /// Determines whether or not the ray passes through the bounding box.
        /// </summary>
        public bool CollidesWith(Ray ray, out float t)
        {
            // https://gamedev.stackexchange.com/questions/18436/most-efficient-aabb-vs-ray-collision-algorithms
            // r.dir is unit direction vector of ray
            float dirfracx = 1.0f / ray.direction.x;
            float dirfracy = 1.0f / ray.direction.y;
            // lb is the corner of AABB with minimal coordinates - left bottom, rt is maximal corner
            // r.org is origin of ray
            float t1 = (this.rx.min - ray.origin.x) * dirfracx;
            float t2 = (this.rx.max - ray.origin.x) * dirfracx;
            float t3 = (this.ry.min - ray.origin.y) * dirfracy;
            float t4 = (this.ry.max - ray.origin.y) * dirfracy;

            float tmin = Math.Max(Math.Min(t1, t2), Math.Min(t3, t4));
            float tmax = Math.Min(Math.Max(t1, t2), Math.Max(t3, t4));

            // if tmax < 0, ray (line) is intersecting AABB, but the whole AABB is behind us
            if (tmax < 0)
            {
                t = tmax;
                return false;
            }

            // if tmin > tmax, ray doesn't intersect AABB
            if (tmin > tmax)
            {
                t = tmax;
                return false;
            }

            t = tmin;
            return true;
        }

        public bool OnEdge(Point2 p)
        {
            if (rx.Contains(p.x))
            { return ry.DistanceFrom(p.y) < debugRadius; }

            if (ry.Contains(p.y))
            { return rx.DistanceFrom(p.x) < debugRadius * (640.0f / 480.0f); }

            return false;
        }

        public bool Contains(Point2 p)
        {
            return rx.Contains(p.x) && ry.Contains(p.y);
        }

        public bool Contains(Ray r)
        {
            return rx.Contains(r.origin.x) && ry.Contains(r.origin.y);
        }

        public bool CollidesWith(BoundingBox other)
        {
            bool xOverlap = this.rx.Overlap(other.rx);
            bool yOverlap = this.ry.Overlap(other.ry);

            return xOverlap && yOverlap;
        }

        public float ComputeArea()
        { return System.Math.Abs((rx.max - rx.min) * (ry.max - ry.min)); }

        public override string ToString()
        { return $"[{rx.ToString()}, {ry.ToString()}]"; }
    }
}
