﻿
using System.Text;
using System.Numerics;

using Raytracer2.Scalar;

namespace Raytracer2.Pack
{
    public struct PointPack
    {
        /// <summary>
        /// Represents the x coordinates of the points within the pack.
        /// </summary>
        public Vector<float> x;

        /// <summary>
        /// Represents the y coordinates of the points within the pack.
        /// </summary>
        public Vector<float> y;

        /// <summary>
        /// Constructs a packed version of the given point.
        /// </summary>
        /// <param name="p"></param>
        public PointPack(in Point2 p)
        {
            this.x = new Vector<float>(p.x);
            this.y = new Vector<float>(p.y);
        }

        /// <summary>
        /// Constructs a packed version of the given coordinates. An offset can be used to comply with the stream requirements. This is the most efficient approach for constructing a point pack.
        /// </summary>
        public PointPack(in float[] x, in float[] y, int offset = 0)
        {
            this.x = new Vector<float>(x, offset);
            this.y = new Vector<float>(y, offset);
        }

        /// <summary>
        /// Constructs a packed version by (deep) copying the given coordinates.
        /// </summary>
        public PointPack(Vector<float> x, Vector<float> y)
        {
            this.x = x;
            this.y = y;
        }

        #region Geometric functions

        /// <summary>
        /// Computes the vectors that point from this point pack to the other point pack.
        /// </summary>
        public VectorPack DirectionTo(in PointPack other)
        { return new VectorPack(other.x - this.x, other.y - this.y); }

        /// <summary>
        /// Computes a vector of lengths representing the distances between the points of this pack and the points of the other pack.
        /// </summary>
        public Vector<float> DistanceTo(in PointPack other)
        { return this.DirectionTo(other).Length(); }

        /// <summary>
        /// Computes a vector of squared lengths representing the distances between the points of this pack and the points of the other pack.
        /// </summary>
        public Vector<float> DistanceSquaredTo(in PointPack other)
        { return this.DirectionTo(other).LengthSquared(); }

        /// <summary>
        /// Rotates all points within the pack with respect to the origin. Rotates in degrees.
        /// </summary>
        public void Rotate(float n)
        {
            float radians = (float)((n / 360.0f) * System.Math.PI * 2);
            float sine = (float)System.Math.Sin(radians);
            float cosine = (float)System.Math.Cos(radians);

            this.x = this.x * cosine - this.y * sine;
            this.y = this.x * sine + this.y * cosine;
        }

        /// <summary>
        /// Rotates all points within the pack with respect to the origin. Rotates in degrees.
        /// </summary>
        public PointPack Rotated(float n)
        {
            PointPack v = new PointPack(this.x, this.y);
            v.Rotate(n);
            return v;
        }

        #endregion

        #region Operators

        public static PointPack operator + (PointPack pp, VectorPack vp)
        {
            return new PointPack(pp.x + vp.x, pp.y + vp.y);
        }

        public static PointPack operator -(PointPack pp, VectorPack vp)
        {
            return new PointPack(pp.x - vp.x, pp.y - vp.y);
        }

        #endregion


        public override string ToString()
        {
            StringBuilder b = new StringBuilder();
            b.Append('(');
            b.Append(x.ToString());
            b.Append(',');
            b.Append(y.ToString());
            b.Append(')');

            return b.ToString();
        }
    }
}