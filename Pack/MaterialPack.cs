﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Numerics;

using Raytracer2.Scalar;

namespace Raytracer2.Pack
{
    internal struct MaterialPack
    {

        public ColorPack cp;
        public Vector<float> ep;

        public MaterialPack(in Material m)
        {
            this.cp = new ColorPack(m.diffuse);
            this.ep = new Vector<float>(m.emissive);
        }

        public MaterialPack(in ColorPack cp, in float[] ep, int offset)
        {
            this.cp = cp;
            this.ep = new Vector<float>(ep, offset);
        }
    }
}