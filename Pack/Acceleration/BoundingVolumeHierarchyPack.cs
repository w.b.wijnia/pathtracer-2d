﻿
using System.Collections.Generic;


using Raytracer2.Scalar;
using Raytracer2.Scalar.Primitives;
using Raytracer2.Scalar.Acceleration;
using Raytracer2.Pack.Primitives;
using System.Numerics;

namespace Raytracer2.Pack.Acceleration
{
    internal class BoundingVolumeHierarchyPack
    {

        internal RenderComponentPack[] efficientPrimitives;

        /// <summary>
        /// Represents the available BVH nodes.
        /// </summary>
        internal BoundingNodePack[] nodePool;

        public BoundingVolumeHierarchyPack(BoundingVolumeHierarchy<Circle> bvh)
        {
            this.efficientPrimitives = new RenderComponentPack[bvh.efficientPrimitives.Length];
            this.nodePool = new BoundingNodePack[bvh.nodePool.Length];

            for(int j = 0; j < efficientPrimitives.Length; j++)
            {
                RenderComponent rc = bvh.efficientPrimitives[j];
                this.efficientPrimitives[j] = new RenderComponentPack(new CirclePack(rc.c), new MaterialPack(rc.m));
            }

            for(int j = 0; j < nodePool.Length; j++)
            {
                BoundingNode bbn = bvh.nodePool[j];
                nodePool[j] = new BoundingNodePack(bbn);
            }
        }

        public Vector<int> Traverse(ref RayPack r)
        {
            // ------------------------------------------------------- \\
            // Setup some default values.                              \\

            Vector<int> collided = new Vector<int>(0);

            Stack<int> poolPointers = new Stack<int>();
            poolPointers.Push(0);

            while (poolPointers.Count != 0)
            {
                int index = poolPointers.Pop();
                BoundingNodePack node = nodePool[index];
                BoundingBoxPack bb = node.BoundingBox;

                Vector<float> tbb;
                Vector<int> collidesWithRayPack = bb.CollidesWith(ref r, out tbb);

                // we need to use masking here?
                bool anyTSmaller = Vector.LessThanAny(tbb, r.t);
                bool collidesWithARay = Vector.Dot(collidesWithRayPack, collidesWithRayPack) > 0;
                if (collidesWithARay && anyTSmaller)
                {
                    if (node.Leaf)
                    {
                        for (int j = node.First, l = node.First + node.Count; j < l; j++)
                        {
                            RenderComponentPack o = efficientPrimitives[j];
                            collided = Vector.BitwiseOr(collided, o.cps.Intersect(ref r));

                            if(Vector.Dot(collided, collided) == Constants.numberOfSIMDLanes)
                            { return collided; }
                        }
                    }
                    else
                    {
                        poolPointers.Push(node.Left);
                        poolPointers.Push(node.Right);
                    }
                }
            }

            return collided;
        }
    }
}