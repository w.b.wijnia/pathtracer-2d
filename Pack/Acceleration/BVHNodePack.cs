﻿
using System;

using Raytracer2.Scalar.Acceleration;

namespace Raytracer2.Pack.Acceleration
{
    internal struct BoundingNodePack
    {
        /// <summary>
        /// Represents the boundingbox of this node.
        /// </summary>
        public BoundingBoxPack BoundingBox { get; set; }

        /// <summary>
        /// Represents the left child of this node. If set to -1, then this node has no children.
        /// </summary>
        public int Left;                                        // 4 bytes

        /// <summary>
        /// Represents a set of elements within the boundingbox array (primitive array on the GPU).
        /// </summary>
        public int First, Count;                                // 8 bytes

        public BoundingNodePack(BoundingNode bbn)
        {
            this.BoundingBox = new BoundingBoxPack(bbn.BoundingBox);
            this.Left = bbn.Left;
            this.First = bbn.First;
            this.Count = bbn.Count;
        }

        /// <summary>
        /// A node is a leaf when 'Left' is equal to -1.
        /// </summary>
        public bool Leaf
        { get { return Left == -1; } }

        public int Right
        { get { return Left + 1; } }

        public override string ToString()
        { return $"{Left}, {First} -> {First + Count}, {BoundingBox}"; }

    }
}
