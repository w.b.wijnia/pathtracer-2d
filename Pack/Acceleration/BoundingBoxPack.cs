﻿
using System;
using System.Numerics;

using Raytracer2.Scalar;
using Raytracer2.Scalar.Acceleration;
using Raytracer2.Pack.Primitives;

namespace Raytracer2.Pack.Acceleration
{
    public struct BoundingBoxPack
    {
        /// <summary>
        /// Represents the same ranges on the x and / or y axis over a vector width.
        /// </summary>
        public RangePack rx, ry;

        /// <summary>
        /// Represents the index of the primitive this bounding box refers to.
        /// </summary>
        public int primitive;

        public BoundingBoxPack(BoundingBox bb)
        {
            this.rx = new RangePack(bb.rx);
            this.ry = new RangePack(bb.ry);
            this.primitive = bb.primitive;
        }

        /// <summary>
        /// Determines whether the point is inside this packed bounding box.
        /// </summary>
        public Vector<int> Contains(Point2 p)
        { return Vector.BitwiseOr(rx.Contains(p.x), ry.Contains(p.y)); }

        /// <summary>
        /// Determines whether any of the given rays start (their origin is) inside this packed bounding box.
        /// </summary>
        /// <param name="r"></param>
        /// <returns></returns>
        public Vector<int> Contains(RayPack r)
        { return Vector.BitwiseOr(rx.Contains(r.origin.x), ry.Contains(r.origin.y)); }

        public Vector<int> CollidesWith(ref RayPack ray, out Vector<float> t)
        {
            Vector<float> ones = new Vector<float>(1.0f);
            Vector<float> zeros = new Vector<float>(0.0f);
            Vector<float> maxt = new Vector<float>(float.MaxValue);

            // ------------------------------------------------------- \\
            // compute the collision.                                  \\

            Vector<float> dirfracx = ones / ray.direction.x;
            Vector<float> dirfracy = ones / ray.direction.y;

            Vector<float> t1 = (this.rx.min - ray.origin.x) * dirfracx;
            Vector<float> t2 = (this.rx.max - ray.origin.x) * dirfracx;
            Vector<float> t3 = (this.ry.min - ray.origin.y) * dirfracy;
            Vector<float> t4 = (this.ry.max - ray.origin.y) * dirfracy;

            Vector<float> tmin = Vector.Max(Vector.Min(t1, t2), Vector.Min(t3, t4));
            Vector<float> tmax = Vector.Min(Vector.Max(t1, t2), Vector.Max(t3, t4));

            Vector<int> lessThanZeroMask = Vector.GreaterThan(tmax, zeros);
            Vector<int> greaterMask = Vector.LessThan(tmin, tmax);
            Vector<int> combinedMask = Vector.BitwiseOr(lessThanZeroMask, greaterMask);

            // ------------------------------------------------------- \\
            // Keep track of the t's that collided.                    \\

            t = Vector.ConditionalSelect(combinedMask, tmin, maxt);

            return combinedMask;
        }

        public override string ToString()
        { return $"[{rx.ToString()}, {ry.ToString()}]"; }

    }
}