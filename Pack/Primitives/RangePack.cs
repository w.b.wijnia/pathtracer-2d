﻿
using System;

using System.Numerics;

using Raytracer2.Scalar.Primitives;

namespace Raytracer2.Pack.Primitives
{
    public struct RangePack
    {

        internal Vector<float> min, max;

        public RangePack (Range r)
        {
            this.min = new Vector<float>(r.min);
            this.max = new Vector<float>(r.max);
        }

        public RangePack (in float[] mins, in float[] maxs, int offset)
        {
            this.min = new Vector<float>(mins, offset);
            this.max = new Vector<float>(maxs, offset);
        }

        public Vector<float> Distance()
        { return max - min; }

        public Vector<float> DistanceFrom(float t)
        {
            Vector<float> vt = new Vector<float>(t);
            return DistanceFrom(vt);
        }

        public Vector<float> DistanceFrom(Vector<float> vt)
        { 
            Vector<float> d1 = Vector.Abs(max - vt);
            Vector<float> d2 = Vector.Abs(min - vt);
            return Vector.Min(d1, d2);
        }

        public Vector<float> Centroid()
        {
            return (min + max) * 0.5f;
        }

        public Vector<int> Contains(float t)
        {
            Vector<float> vt = new Vector<float>(t);
            return Contains(vt);
        }

        public Vector<int> Contains(Vector<float> vt)
        {
            return Vector.BitwiseOr(
                Vector.LessThanOrEqual(min, vt),
                Vector.GreaterThanOrEqual(vt, max)
                );
        }

        /// <summary>
        /// Checks whether the given ranges overlap.
        /// </summary>
        public Vector<int> Overlap(RangePack other)
        {
            return Vector.BitwiseOr(
                Vector.LessThanOrEqual(other.min, this.max),
                Vector.LessThanOrEqual(this.min, other.max)
                );
        }

    }
}
