﻿
using System;

using System.Numerics;

using Raytracer2.Scalar.Primitives;

namespace Raytracer2.Pack.Primitives
{
    public struct RayPack
    {
        /// <summary>
        /// Represents the origins of the individual rays.
        /// </summary>
        public PointPack origin;

        /// <summary>
        /// Represents the directions of the individual rays.
        /// </summary>
        public VectorPack direction;

        public Vector<float> t;

        public RayPack(Ray r)
        {
            this.origin = new PointPack(r.origin);
            this.direction = new VectorPack(r.direction);
            this.t = new Vector<float>(r.t);
        }

        public RayPack(PointPack origin, VectorPack direction, Vector<float> t)
        {
            this.origin = origin;
            this.direction = direction;
            this.t = t;
        }

        public RayPack(float[] ox, float[] oy, float[] dx, float[] dy, float[] t, int offset = 0)
        {
            this.origin = new PointPack(ox, oy, offset);
            this.direction = new VectorPack(dx, dy, offset);
            this.t = new Vector<float>(t, offset);
        }

        public Vector<int> Intersect(CirclePack cir)
        {
            // ------------------------------------------------------- \\
            // Put all the data on the stack.                          \\

            Vector<float> zeros = new Vector<float>(0.0f);
            Vector<int> zerosi = new Vector<int>(0);
            Vector<float> twos = new Vector<float>(2.0f);
            Vector<float> fours = new Vector<float>(4.0f);

            // ------------------------------------------------------- \\
            // Compute whether the ray collides with the circle. This  \\
            // is stored in the 'mask' vector.                         \\

            Vector<float> p = this.origin.x - cir.origin.x;
            Vector<float> q = this.origin.y - cir.origin.y;

            Vector<float> a = this.direction.x * this.direction.x + this.direction.y * this.direction.y;
            Vector<float> b = twos * p * this.direction.x + twos * q * this.direction.y;
            Vector<float> c = p * p + q * q - cir.radius * cir.radius;

            Vector<float> dSqrt = b * b - fours * a * c;

            Vector<int> maskCollision = Vector.GreaterThan(dSqrt, zeros);

            if (!Vector.LessThanAny(maskCollision, zerosi))
            { return maskCollision; }

            // ------------------------------------------------------- \\
            // Update t if and only if there is a collision. Take      \\
            // note of the conditional where we compute t.             \\

            Vector<float> D = Vector.SquareRoot(dSqrt);
            Vector<float> tm = (Vector.Negate(b) - D) / (twos * a);

            Vector<int> maskInScope = Vector.BitwiseAnd(Vector.GreaterThan(tm, zeros), Vector.LessThan(tm, this.t));
            Vector<int> mask = Vector.BitwiseAnd(maskCollision, maskInScope);

            this.t = Vector.ConditionalSelect(mask, tm, t);

            return mask;
        }
    }
}