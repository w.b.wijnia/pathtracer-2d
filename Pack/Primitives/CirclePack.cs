﻿
using System;
using System.Numerics;

using Raytracer2.Scalar.Primitives;

namespace Raytracer2.Pack.Primitives
{
    public struct CirclePack : iPrimitivePack
    {
        /// <summary>
        /// Represents the origin of this pack of circles.
        /// </summary>
        public PointPack origin;

        /// <summary>
        /// Represents the radia of this pack of circles.
        /// </summary>
        public Vector<float> radius;

        public CirclePack(Circle c)
        {
            this.origin = new PointPack(new Vector<float>(c.origin.x), new Vector<float>(c.origin.y));
            this.radius = new Vector<float>(c.radius);
        }

        public CirclePack(in PointPack pp, in float[] radia, int offset)
        {
            this.origin = pp;
            this.radius = new Vector<float>(radia, offset);
        }

        public Vector<int> Contains(PointPack p)
        {
            return Vector.LessThan(p.DistanceSquaredTo(this.origin), this.radius * this.radius);
        }

        public Vector<int> Intersect(ref RayPack ray)
        {
            return ray.Intersect(this);
        }

        public PointPack RandomPosition(iRandom rng)
        {
            Vector<float> rx = rng.Floats();
            Vector<float> ry = rng.Floats();
            Vector<float> rr = rng.Floats();

            VectorPack dir = new VectorPack(rx, ry);
            return this.origin + ((new Vector<float>(0.98f) * rr * this.radius) * dir.Normalized());
        }
    }
}
