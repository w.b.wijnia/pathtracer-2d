﻿
using System.Numerics;

namespace Raytracer2.Pack.Primitives
{
    public interface iPrimitivePack
    {

        /// <summary>
        /// Computes a random position within the primitive.
        /// </summary>
        PointPack RandomPosition(iRandom rng);

        /// <summary>
        /// Computes whether the given ray intersects this shape. The function returns true if any ray intersects a primitive.
        /// </summary>
        Vector<int> Intersect(ref RayPack ray);

        /// <summary>
        /// Computes whether the point is inside the primitive. The function returns true if any primitive contains a point.
        /// </summary>
        Vector<int> Contains(PointPack p);

    }
}