﻿
using System;
using System.Text;

using System.Numerics;

using Raytracer2.Scalar;

namespace Raytracer2.Pack
{
    public struct VectorPack
    {
        /// <summary>
        /// Represents the x components of this vector pack.
        /// </summary>
        public Vector<float> x;

        /// <summary>
        /// Represents the y components of this vector pack.
        /// </summary>
        public Vector<float> y;

        public VectorPack(Scalar.Vector2 v)
        {
            this.x = new Vector<float>(v.x);
            this.y = new Vector<float>(v.y);
        }

        /// <summary>
        /// Constructs a pack of vectors with the given coordinates. Take note that the length of the arrays must be equal to the width of the SIMD lanes.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public VectorPack(in float[] x, in float[] y, int offset = 0)
        {
            this.x = new Vector<float>(x, offset);
            this.y = new Vector<float>(y, offset);
        }

        /// <summary>
        /// Constructs a pack of vectors with the given coordinates.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public VectorPack(Vector<float> x, Vector<float> y)
        {
            this.x = x;
            this.y = y;
        }

        #region Geometric functions

        /// <summary>
        /// Computes the squared length of all the vectors within this pack.
        /// </summary>
        /// <returns></returns>
        public Vector<float> LengthSquared()
        { return this * this; }

        /// <summary>
        /// Computes the length of all the vectors within this pack.
        /// </summary>
        /// <returns></returns>
        public Vector<float> Length()
        { return Vector.SquareRoot(this.LengthSquared()); }

        /// <summary>
        /// Normalizes all vectors within this pack of vectors.
        /// </summary>
        public void Normalize()
        {
            Vector<float> ones = new Vector<float>(1.0f);
            Vector<float> l = this.Length();
            Vector<float> il = Vector.Divide(ones, l);

            this.x = il * this.x;
            this.y = il * this.y;  
        }

        /// <summary>
        /// Normalizes a new pack of vectors with all the vectors normalized.
        /// </summary>
        /// <returns></returns>
        public VectorPack Normalized()
        {
            VectorPack vp = new VectorPack(this.x, this.y);
            vp.Normalize();
            return vp;
        }

        public void Rotate(float n)
        {
            float radians = (float)((n / 360.0f) * System.Math.PI * 2);
            float sine = (float)System.Math.Sin(radians);
            float cosine = (float)System.Math.Cos(radians);

            this.x = this.x * cosine - this.y * sine;
            this.y = this.x * sine + this.y * cosine;
        }

        public VectorPack Rotated(float n)
        {
            VectorPack v = new VectorPack(this.x, this.y);
            v.Rotate(n);
            return v;
        }

        public void Project(VectorPack other)
        {
            // compute the squared length of the vector to project on.
            Vector<float> d = other * other;
            Vector<float> dp = this * other;

            // determine the scaling factor.
            Vector<float> factor = dp / d;
             
            // scale us!
            this.x = other.x * factor;
            this.y = other.y * factor;
        }

        public VectorPack Projected(VectorPack other)
        {
            VectorPack vp = new VectorPack(this.x, this.y);
            vp.Project(other);
            return vp;
        }

        public VectorPack Orthogonal()
        {
            Vector<float> nones = new Vector<float>(-1.0f);
            return new VectorPack(this.y, nones * this.x);
        }

        #endregion

        #region Operators

        /// <summary>
        /// Adds the given vector packs together, coordinate-wise.
        /// </summary>
        public static VectorPack operator + (VectorPack v1, VectorPack v2)
        {
            Vector<float> x = v1.x + v2.x;
            Vector<float> y = v1.y + v2.y;
            return new VectorPack(x, y);
        }

        /// <summary>
        /// Subtracts the given vector packs from one another, coordinate-wise.
        /// </summary>
        public static VectorPack operator - (VectorPack v1, VectorPack v2)
        {
            Vector<float> x = v1.x - v2.x;
            Vector<float> y = v1.y - v2.y;
            return new VectorPack(x, y);
        }

        /// <summary>
        /// Multiplies the given pack of vectors by the given scalars.
        /// </summary> 
        public static VectorPack operator * (Vector<float> s, VectorPack v)
        {
            Vector<float> x = s * v.x;
            Vector<float> y = s * v.y;
            return new VectorPack(x, y);
        }

        /// <summary>
        /// Multiplies the given pack of vectors by the given scalar.
        /// </summary>
        public static VectorPack operator * (float s, VectorPack v)
        {
            Vector<float> vs = new Vector<float>(s);
            return vs * v;
        }

        /// <summary>
        /// Computes the dot product between all the vectors of the pack.
        /// </summary>
        public static Vector<float> operator * (VectorPack v1, VectorPack v2)
        {
            Vector<float> x = v1.x * v2.x;
            Vector<float> y = v1.y * v2.y;
            return x + y;
        }

        #endregion

        public override string ToString()
        {
            StringBuilder b = new StringBuilder();
            b.Append('(');
            b.Append(x.ToString());
            b.Append(',');
            b.Append(y.ToString());
            b.Append(')');

            return b.ToString();
        }
    }
}