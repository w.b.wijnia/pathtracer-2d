﻿
using System;

using Raytracer2.Scalar;
using Raytracer2.Pack.Primitives;

namespace Raytracer2.Pack
{
    internal struct RenderComponentPack
    {
        /// <summary>
        /// Represents the primitive that this graphics element contains.
        /// </summary>
        public CirclePack cps;

        /// <summary>
        /// Represents the material used by this primitve.
        /// </summary>
        public MaterialPack mp;

        public RenderComponentPack(in RenderComponent rc)
        {
            this.cps = new CirclePack(rc.c);
            this.mp = new MaterialPack(rc.m);
        }

        public RenderComponentPack(in CirclePack cp, in MaterialPack mp)
        {
            this.cps = cp;
            this.mp = mp;
        }
    }
}
