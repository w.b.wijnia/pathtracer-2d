﻿
using System;
using System.Runtime.InteropServices;

using Raytracer2.Scalar;

using System.Numerics;

namespace Raytracer2.Pack
{
    internal struct ColorPack
    {
        public Vector<float> r, g, b;

        public ColorPack(Color c)
        {
            this.r = new Vector<float>(c.r);
            this.g = new Vector<float>(c.g);
            this.b = new Vector<float>(c.b);
        }

        public ColorPack(in float[] r, in float[] g, in float[] b, int offset = 0)
        {
            this.r = new Vector<float>(r, offset);
            this.g = new Vector<float>(g, offset);
            this.b = new Vector<float>(b, offset);
        }

        public ColorPack(Vector<float> r, Vector<float> g, Vector<float> b)
        {
            this.r = r;
            this.g = g;
            this.b = b;
        }

        #region Operators

        /// <summary>
        /// Computes the color where both colors are added together. Does not normalize the color.
        /// </summary>
        /// <returns></returns>
        public static ColorPack operator + (ColorPack c1, ColorPack c2)
        {
            Vector<float> r = c1.r + c2.r;
            Vector<float> g = c1.g + c2.g;
            Vector<float> b = c1.b + c2.b;
            return new ColorPack(r, g, b);
        }

        /// <summary>
        /// Computes the color that is multiplied by the scalar. Does not normalize the color.
        /// </summary>
        /// <returns></returns>
        public static ColorPack operator * (float f, ColorPack c)
        {
            Vector<float> vf = new Vector<float>(f);
            return vf * c;
        }

        /// <summary>
        /// Computes the color that is multiplied by the scalar. Does not normalize the color.
        /// </summary>
        /// <returns></returns>
        public static ColorPack operator *(Vector<float> vf, ColorPack c)
        {
            Vector<float> r = vf * c.r;
            Vector<float> g = vf * c.g;
            Vector<float> b = vf * c.b;
            return new ColorPack(r, g, b);
        }

        /// <summary>
        /// Computes the color that is multiplied by the scalar. Does not normalize the color.
        /// </summary>
        /// <returns></returns>
        public static ColorPack operator * (ColorPack c1, ColorPack c2)
        {
            Vector<float> r = c1.r * c2.r;
            Vector<float> g = c1.g * c2.g;
            Vector<float> b = c1.b * c2.b;
            return new ColorPack(r, g, b);
        }

        #endregion

        public override string ToString()
        { return $"({r}, {b}, {g})"; }
    }
}