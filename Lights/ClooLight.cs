﻿using System.Runtime.InteropServices;

namespace Raytracer2.Lights
{
    [StructLayout(LayoutKind.Explicit, Size = 32)]
    public struct ClooLight
    {

        /// <summary>
        /// Represents the origin of the light.
        /// </summary>
        [FieldOffset(0)]
        public Point2 origin;                           // 8 bytes, 0 -> 8

        /// <summary>
        /// Represents the color of the light.
        /// </summary>
        [FieldOffset(8)]
        public Color color;                             // 16 bytes, 8 -> 24

        /// <summary>
        /// Represents the illumination strength of the light.
        /// </summary>
        [FieldOffset(24)]                               // 4 bytes, 24 -> 28
        public float intensity;

    }
}
