﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Raytracer2.Primitives;

namespace Raytracer2.Lights
{
    public struct Light : iClooLight
    {
        private const float attenuationThreshold = 0.001f;

        /// <summary>
        /// Represents the origin of the light.
        /// </summary>
        public Point2 origin;

        /// <summary>
        /// Represents the color of the light.
        /// </summary>
        public Color color;

        /// <summary>
        /// Represents the illumination strength of the light.
        /// </summary>
        public float intensity;

        /// <summary>
        /// Constructs a light with the given parameters.
        /// </summary>
        public Light(Point2 origin, Color color, float intensity)
        {
            this.origin = origin;
            this.color = color;
            this.intensity = intensity;
        }

        /// <summary>
        /// Computes the light radius in which we consider the light to have any impact to the picture.
        /// </summary>
        public float LightRadius
        { get { return intensity * ((float)Math.Sqrt(1 / attenuationThreshold) - 1.0f); } }

        /// <summary>
        /// Computes whether the given point is contained by this light.
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        public bool ContainsPoint(Point2 point)
        {
            float d = origin.SquaredDistanceTo(point);
            float r = LightRadius;

            return d < r * r;
        }

        /// <summary>
        /// Computes whether a point is contained by this light.
        /// </summary>
        public bool ContainsAPoint(Point2[] points)
        {
            float r = LightRadius;

            foreach(Point2 point in points)
            {
                float d = origin.SquaredDistanceTo(point);
                if (d < r * r)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Computes whether all points are contained by this light.
        /// </summary>
        public bool ContainsAllPoints(Point2[] points)
        {
            float r = LightRadius;

            foreach (Point2 point in points)
            {
                float d = origin.SquaredDistanceTo(point);
                if (d > r * r)
                {
                    return false;
                }
            }

            return true;
        }

        public ClooLight ConstructClooLight()
        {
            ClooLight cl;
            cl.origin = origin;
            cl.color = color;
            cl.intensity = intensity;

            return cl;
        }
    }
}