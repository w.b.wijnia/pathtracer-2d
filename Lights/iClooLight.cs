﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raytracer2.Lights
{
    internal interface iClooLight
    {

        ClooLight ConstructClooLight();

    }
}
