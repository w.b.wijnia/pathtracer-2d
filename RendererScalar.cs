﻿
using System;
using System.Numerics;
using System.Threading;
using System.Diagnostics;

using OpenTK.Graphics.OpenGL;

using Raytracer2.Scalar;
using Raytracer2.Stream.Acceleration;
using Raytracer2.Scalar.Primitives;
using Raytracer2.Scalar.Acceleration;

namespace Raytracer2
{
    internal class RendererScalar : Renderer
    {
        // -- tweakable values -- \\

        /// <summary>
        /// Represents the size of the square that a single thread will work on.
        /// </summary>
        private const int squareSize = Constants.numberOfSIMDLanes;

        // -- end of tweakable values -- \\

        /// <summary>
        /// Used to signal the threads that they can start working.
        /// </summary>
        private EventWaitHandle signalToThread;

        /// <summary>
        /// Used to signal to main that the thread is done.
        /// </summary>
        private EventWaitHandle[] signalToMain;

        /// <summary>
        /// Contains the threads used for rendering.
        /// </summary>
        private Thread[] ts;

        /// <summary>
        /// Used by the threads to compute random numbers.
        /// </summary>
        private iRandom[] rngs;

        /// <summary>
        /// Represents the number of jobs left for the renderer.
        /// </summary>
        private int jobs;

        /// <summary>
        /// Represents the number of (logical) processors.
        /// </summary>
        private int processors;

        /// <summary>
        /// Represents the color buffer that we fill during rendering.
        /// </summary>
        protected Color[] outputBuffer;

        BoundingVolumeHierarchy<Circle> bvh;


        Stopwatch w = new Stopwatch();
        Stopwatch bvhConstruction = new Stopwatch();

        private int raysPerPixel = 1;

        /// <summary>
        /// Constructs a new camera with a resolution of w(idth) * h(eight). 
        /// </summary>
        public RendererScalar(int w, int h, int rs)
            : base(w, h)
        {
            raysPerPixel = rs;
            processors = System.Environment.ProcessorCount;

            ts = new Thread[processors];
            rngs = new iRandom[processors];
            signalToMain = new EventWaitHandle[processors];
            signalToThread = new EventWaitHandle(false, EventResetMode.ManualReset);

            for (int j = 0, l = processors; j < l; j++)
            {
                rngs[j] = new RXor((uint)(1023 + j));
                signalToMain[j] = new EventWaitHandle(false, EventResetMode.ManualReset);

                // we need to createa a new variable, or things may go bad.
                int threadNumber = j;
                ts[threadNumber] = new Thread(() => ThreadWorker(threadNumber));
                ts[threadNumber].Start();
            }

            // construct the screen and our internal buffer.
            outputBuffer = new Color[w * h];

            screenid = GL.GenTexture();
            GL.BindTexture(TextureTarget.Texture2D, screenid);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, width, height, 0, PixelFormat.Rgba, PixelType.Float, outputBuffer);
        }

        public override void Render(Camera camera, Hierarchy hierarchy)
        {
            tickNumber++;

            // ------------------------------------------------------- \\
            // BVH construction                                        \\

            bvhConstruction.Start();
            bvh = new BoundingVolumeHierarchy<Circle>(hierarchy.GetOccluders);
            bvhConstruction.Stop();
            Diagnostic.TimeAccelerationStructure = bvhConstruction.ElapsedMilliseconds;
            bvhConstruction.Reset();

            w.Reset();
            w.Start();

            StartAndWaitForThreads();

            GL.DeleteTextures(1, ref screenid);

            screenid = GL.GenTexture();
            GL.BindTexture(TextureTarget.Texture2D, screenid);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, width, height, 0, PixelFormat.Rgba, PixelType.Float, outputBuffer);

            w.Stop();
            Diagnostic.TimeRender = w.ElapsedMilliseconds;
        }

        private void StartAndWaitForThreads()
        {
            // compute the number of jobs. Due to integer rounding, we always add one additional (half or empty) job.
            jobs = ((width / squareSize) + 1) * ((height / squareSize) + 1);

            // reset the 'i am done' signals, then put the workers to work.
            for (int j = 0; j < processors; j++)
            {
                signalToMain[j].Reset();
            }

            // workers be like: vrrroooooooommM!!
            signalToThread.Set();

            // wait for the workers to be done.
            for (int j = 0; j < processors; j++)
            {
                signalToMain[j].WaitOne(-1);        // main be like: zzzzzzzz....
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="threadNumber"></param>
        private void ThreadWorker(int threadNumber)
        {
            Stopwatch diffuseWatch = new Stopwatch();
            Stopwatch lightWatch = new Stopwatch();
            Stopwatch combineWatch = new Stopwatch();

            Span<Color> diffuseSpan = stackalloc Color[squareSize * squareSize];
            Span<Color> lightSpan = stackalloc Color[squareSize * squareSize];

            while (true)
            {
                Interlocked.Add(ref Diagnostic.timeDiffuse, (int)diffuseWatch.ElapsedMilliseconds);
                Interlocked.Add(ref Diagnostic.timeLighting, (int)lightWatch.ElapsedMilliseconds);
                Interlocked.Add(ref Diagnostic.timeCombined, (int)combineWatch.ElapsedMilliseconds);

                diffuseWatch.Reset();
                lightWatch.Reset();
                combineWatch.Reset();

                // wait for the start signal...
                signalToThread.WaitOne(-1);

                while (true)
                {
                    // ... and go!

                    // pick up a job.
                    int job = Interlocked.Decrement(ref jobs);

                    // are we already done?
                    if (job < 0)
                    {
                        // reset our selves, tell the application we're done.
                        signalToMain[threadNumber].Set();

                        break;
                    }

                    // Compute our x / y values.
                    int s = height / squareSize;
                    int x = job / s;
                    int y = job - x * s;

                    // example of computing x and y value. Given that:
                    //  - width         = 400
                    //  - height        = 400
                    //  - squareSize    = 10
                    // this results in 1681 jobs.

                    // say that we have job number 801.
                    // then the x component of the square is 801 / (400 / 10) = 20.
                    // from here on, y is equal to the data dat was 'lost' due to integer division (the remainder). In this case, that is the 1.

                    int xi = x * squareSize;
                    int yi = y * squareSize;

                    diffuseWatch.Start();
                    RenderDiffuseSquare(ref diffuseSpan, ref raysPerPixel, rngs[threadNumber], xi, yi);
                    diffuseWatch.Stop();

                    lightWatch.Start();
                    RenderLightingSquare(ref lightSpan, ref raysPerPixel, rngs[threadNumber], xi, yi);
                    lightWatch.Stop();

                    combineWatch.Start();
                    RenderCombineSquare(ref diffuseSpan, ref lightSpan, xi, yi);
                    combineWatch.Stop();
                }
            }
        }

        private void RenderCombineSquare(ref Span<Color> diffuseBuffer, ref Span<Color> lightingBuffer, int gx, int gy)
        {
            float ratio = 1.0f / tickNumber;

            for (int sy = 0; sy < squareSize; sy++)
            {
                int iy = Math.Min(sy + gy, height - 1);

                for (int sx = 0; sx < squareSize; sx++)
                {
                    int ix = Math.Min(sx + gx, width - 1);

                    outputBuffer[ix + iy * width] = diffuseBuffer[sx + sy * squareSize] * lightingBuffer[sx + sy * squareSize];
                }
            }
        }

        private void RenderDiffuseSquare(ref Span<Color> localBuffer, ref int rays, iRandom rng, int gx, int gy)
        {
            float ratio = 1.0f / tickNumber;
            float impact = (1.0f / 1);

            // ------------------------------------------------------- \\
            // Compute the color values in a 'local' buffer.           \\

            for (int sy = 0; sy < squareSize; sy++)
            {
                for (int sx = 0; sx < squareSize; sx++)
                {
                    int iy = Math.Min(sy + gy, height - 1);
                    int ix = Math.Min(sx + gx, width - 1);

                    localBuffer[sx + sy * squareSize] = new Color(0, 0, 0);

                    for (int rn = 0, l = 1; rn < l; rn++)
                    { localBuffer[sx + sy * squareSize] += impact * RenderDiffuseWork(rng, ix, iy); }
                }
            }
        }

        private Color RenderDiffuseWork(iRandom rng, int x, int y)
        {
            Color output = new Color(1, 1, 1);

            // -- determine the origin of the ray -- \\
            float dx = ((float)x - width) / width + 0.5f;
            float dy = ((float)y - height) / height + 0.5f;

            float ox = 1.0f / width * (rng.Float() - 0.5f);
            float oy = 1.0f / height * (rng.Float() - 0.5f);
            Point2 rayOrigin = this.camera.origin + new Scalar.Vector2((ox + dx) * this.camera.halfExtend.x, (oy + dy * this.camera.halfExtend.y));

            // -- determine the color of the ray -- \\

            //if (qt.OnEdge(rayOrigin))
            //{ return new Color(1, 0, 1); }
            //else
//            if (bvh.OnNode(rayOrigin))
//            { return new Color(1, 0, 1); }
//            else
            {
                RenderComponent component;
                if (bvh.Contains(ref rayOrigin, out component))
                {
                    // retrieve the material.
                    Material primitiveMaterial = component.m;

                    float e = System.Math.Min(primitiveMaterial.emissive, 1.0f);
                    Color mixed = new Color(
                        e * output.r + (1 - e) * primitiveMaterial.diffuse.r,
                        e * output.g + (1 - e) * primitiveMaterial.diffuse.g,
                        e * output.b + (1 - e) * primitiveMaterial.diffuse.b
                    );

                    output = mixed;
                }
            }

            return output;
        }

        private void RenderLightingSquare(ref Span<Color> localBuffer, ref int rays, iRandom rng, int gx, int gy)
        {
            float ratio = 1.0f / tickNumber;
            float impact = (1.0f / rays);

            // ------------------------------------------------------- \\
            // Compute the color values in a 'local' buffer.           \\

            for (int sy = 0; sy < squareSize; sy++)
            {
                for (int sx = 0; sx < squareSize; sx++)
                {
                    int iy = Math.Min(sy + gy, height - 1);
                    int ix = Math.Min(sx + gx, width - 1);

                    localBuffer[sx + sy * squareSize] = new Color(0, 0, 0);

                    for (int rn = 0, l = raysPerPixel; rn < l; rn++)
                    { localBuffer[sx + sy * squareSize] += impact * RenderLightingWork(rng, ix, iy); }
                }
            }
        }

        private Color RenderLightingWork(iRandom rng, int x, int y)
        {
            Color output = new Color(0, 0, 0);

            // -- determine the origin of the ray -- \\
            float dx = ((float)x - width) / width + 0.5f;
            float dy = ((float)y - height) / height + 0.5f;

            float ox = 1.0f / width * (rng.Float() - 0.5f);
            float oy = 1.0f / height * (rng.Float() - 0.5f);
            Point2 rayOrigin = this.camera.origin + new Scalar.Vector2((ox + dx) * this.camera.halfExtend.x, (oy + dy) * this.camera.halfExtend.y);

            // -- determine what lights influence this location -- \\
            RenderComponent[] components = bvh.efficientPrimitives;
            for (int j = 0; j < components.Length; j++)
            {
                RenderComponent lightPrimitive = components[j];
                Material lightMaterial = lightPrimitive.m;

                if (lightMaterial.emissive < 0.01f)
                { continue; }

                // -- compute the ray for the given light -- \\
                Point2 lightOrigin = lightPrimitive.c.RandomLocationInPrimitive(rng);
                Scalar.Vector2 direction = rayOrigin.DirectionTo(lightOrigin).Normalized();

                Ray lightRay = new Ray(rayOrigin, direction);
                lightRay.Intersect(lightPrimitive.c);

                Ray sceneRay = new Ray(rayOrigin, direction);
                sceneRay.t = lightRay.t - 0.001f;

                // -- if it may influence this pixel, compute whether or not it is occluded. -- \\
                bool intersected = bvh.Traverse(ref sceneRay);
                if (!intersected)
                {
                    float attenuation = 1 / ((lightRay.t + 1) * (lightRay.t + 1));
                    float strength = attenuation * lightMaterial.emissive;

                    Color result = strength * lightMaterial.diffuse;
                    output += result;
                }
            }

            return 2.0f * output;
        }
    }
}