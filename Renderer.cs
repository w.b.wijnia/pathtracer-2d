﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Raytracer2.Scalar.Primitives;
using Raytracer2.Scalar.Acceleration;
using Raytracer2.Scalar;
namespace Raytracer2
{
    internal abstract class Renderer
    {
        protected Camera camera;

        /// <summary>
        /// Represents the width and height of the screen.
        /// </summary>
        protected int width, height;

        /// <summary>
        /// Represents the integer reference to the OpenGL texture.
        /// </summary>
        public int screenid;

        /// <summary>
        /// Represents the tick number of this frame. It is used during accumalation of the frames.
        /// </summary>
        protected int tickNumber;

        /// <summary>
        /// Constructs a new camera with a resolution of w(idth) * h(eight). 
        /// </summary>
        public Renderer(int w, int h)
        {
            // receive data
            width = w;
            height = h;
        }

        /// <summary>
        /// Renders the given hierarchies.
        /// </summary>
        public abstract void Render(Camera camera, Hierarchy hierarchy);

        public void ResetAccumalation()
        { this.tickNumber = 0; }
    }
}
