﻿
using System;
using System.Threading;


using Raytracer2.Scalar;
using Raytracer2.Pack;

namespace Raytracer2.Stream
{
    internal struct ColorStream : iStream<Color, ColorPack>
    {

        private float[] r, g, b;

        /// <summary>
        /// Represents the next element that is free.
        /// </summary>
        private int nextFreeElement;

        internal ColorStream(int n)
        {
            this.r = new float[n];
            this.g = new float[n];
            this.b = new float[n];

            this.nextFreeElement = 0;
        }

        public int MaximumCapacity => r.Length;

        public int UsedCapacity => nextFreeElement;

        public ColorPack ExtractFromStream(int offset)
        {
            return new ColorPack(in r, in g, in b, offset);
        }

        public void StoreInStream(in Color c)
        {
            int id = Interlocked.Increment(ref nextFreeElement);
            this.r[id] = c.r;
            this.g[id] = c.g;
            this.b[id] = c.b;
        }

        public void StoreInStream(in ColorPack s, int offset)
        {
            s.r.CopyTo(r, offset);
            s.b.CopyTo(b, offset);
            s.g.CopyTo(g, offset);
        }

        public void Clear()
        {
            this.nextFreeElement = 0;
        }

        public void SwapInStream(int a, int c)
        {
            float lr = r[a];
            r[a] = r[c];
            r[c] = lr;

            float lg = g[a];
            g[a] = g[c];
            g[c] = lg;

            float lb = b[a];
            b[a] = b[c];
            b[c] = lb;
        }
    }
}