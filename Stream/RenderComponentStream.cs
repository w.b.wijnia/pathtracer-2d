﻿
using System;
using System.Threading;

using Raytracer2.Scalar;
using Raytracer2.Pack;

using Raytracer2.Stream.Primitives;

namespace Raytracer2.Stream
{
    internal struct RenderComponentStream : iStream<RenderComponent, RenderComponentPack>
    {

        CircleStream cs;

        MaterialStream ms;

        public RenderComponentStream(int n)
        {
            this.cs = new CircleStream(n);
            this.ms = new MaterialStream(n);
        }

        public int MaximumCapacity => cs.MaximumCapacity;

        public int UsedCapacity => cs.UsedCapacity;

        public RenderComponentPack ExtractFromStream(int offset)
        {
            return new RenderComponentPack(
                cs.ExtractFromStream(offset),
                ms.ExtractFromStream(offset)
                );
        }

        public void StoreInStream(in RenderComponent r)
        {
            cs.StoreInStream(r.c);
            ms.StoreInStream(r.m);
        }

        public void StoreInStream(in RenderComponentPack s, int offset)
        {
            cs.StoreInStream(s.cps, offset);
            ms.StoreInStream(s.mp, offset);
        }

        public void Clear()
        {
            cs.Clear();
            ms.Clear();
        }

        public void SwapInStream(int a, int b)
        {
            cs.SwapInStream(a, b);
            ms.SwapInStream(a, b);
        }
    }
}