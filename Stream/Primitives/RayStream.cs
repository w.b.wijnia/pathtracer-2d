﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Raytracer2.Scalar.Primitives;
using Raytracer2.Pack.Primitives;

namespace Raytracer2.Stream.Primitives
{
    struct RayStream
    {

        public Ray r { get; }

        public RayPack rp { get; }

        public RayStream (Ray r)
        {
            this.r = r;
            this.rp = new RayPack(r);
        }
    }
}