﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Threading;

using Raytracer2.Scalar.Primitives;
using Raytracer2.Pack.Primitives;

namespace Raytracer2.Stream.Primitives
{
    internal struct RangeStream : iStream<Range, RangePack>
    {

        private float[] mins;

        private float[] maxs;

        private int nextFreeElement;

        public int MaximumCapacity => mins.Length;

        public int UsedCapacity => nextFreeElement;

        public RangeStream(int n)
        {
            this.mins = new float[n];
            this.maxs = new float[n];
            this.nextFreeElement = 0;
        }

        public void StoreInStream(in Range r)
        { StoreInStream(r.min, r.max); }

        public void StoreInStream(float min, float max)
        {
            int index = Interlocked.Increment(ref nextFreeElement);
            mins[index] = min;
            maxs[index] = max;
        }

        public void StoreInStream(in RangePack s, int offset)
        {
            s.min.CopyTo(mins, offset);
            s.max.CopyTo(maxs, offset);
        }

        /// <summary>
        /// Performance-wise, this overload is advised. Extracts a couple of elements from the stream and stores them in the struct. Number of structs is equal to the length of the SIMD lanes available.
        /// </summary>
        /// <param name="offset">The offset of where to start within the stream.</param>
        /// <returns></returns>
        public RangePack ExtractFromStream(int offset)
        { return new RangePack(in mins, in maxs, offset); }

        public void Clear()
        {
            this.nextFreeElement = 0;
        }

        public void SwapInStream(int a, int b)
        {
            float lmin = mins[a];
            mins[a] = mins[b];
            mins[b] = lmin;

            float lmax = maxs[a];
            maxs[a] = maxs[b];
            maxs[b] = lmax;
        }
    }
}