﻿
using System;
using System.Threading;

using Raytracer2.Scalar;
using Raytracer2.Scalar.Primitives;

using Raytracer2.Pack;
using Raytracer2.Pack.Primitives;

namespace Raytracer2.Stream.Primitives
{
    internal struct CircleStream : iStream<Circle, CirclePack>
    {
        /// <summary>
        /// Represents the buffer of points.
        /// </summary>
        private PointStream ps;

        /// <summary>
        /// Represents the buffer of radia.
        /// </summary>
        private float[] rs;

        /// <summary>
        /// Represents the next element that is free.
        /// </summary>
        private int nextFreeElement;

        public int MaximumCapacity => rs.Length;

        public int UsedCapacity => nextFreeElement;

        public CircleStream(int size)
        {
            this.ps = new PointStream(size);
            this.rs = new float[size];
            this.nextFreeElement = 0;
        }

        public void StoreInStream(in Circle c)
        { StoreInStream(c.origin, c.radius); }


        public void StoreInStream(in CirclePack s, int offset)
        {
            ps.StoreInStream(s.origin, offset);
            s.radius.CopyTo(rs, offset);
        }

        public void StoreInStream(Point2 p, float r)
        {
            int index = Interlocked.Increment(ref nextFreeElement);
            ps.StoreInStream(p);
            rs[index] = r;
        }

        /// <summary>
        /// Performance-wise, this overload is advised. Extracts a couple of elements from the stream and stores them in the struct. Number of structs is equal to the length of the SIMD lanes available.
        /// </summary>
        /// <param name="offset">The offset of where to start within the stream.</param>
        /// <returns></returns>
        public CirclePack ExtractFromStream(int offset)
        {
            PointPack pointSet = this.ps.ExtractFromStream(offset);
            return new CirclePack(in pointSet, in rs, offset);
        }

        public void Clear()
        {
            ps.Clear();
            this.nextFreeElement = 0;
        }

        public void SwapInStream(int a, int b)
        {
            ps.SwapInStream(a, b);

            float lr = rs[a];
            rs[a] = rs[b];
            rs[b] = lr;
        }
    }
}