﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raytracer2.Stream
{
    interface iStream<T, S>
    {

        S ExtractFromStream(int offset);

        void StoreInStream(in T r);

        void StoreInStream(in S s, int offset);

        void SwapInStream(int a, int b);

        int MaximumCapacity { get; }

        int UsedCapacity { get; }

        void Clear();

    }
}