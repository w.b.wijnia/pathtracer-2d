﻿
using System.Numerics;
using System.Collections.Generic;

using Raytracer2.Scalar;
using Raytracer2.Pack;

using Raytracer2.Scalar.Primitives;
using Raytracer2.Pack.Primitives;
using Raytracer2.Stream.Primitives;

using Raytracer2.Scalar.Acceleration;

namespace Raytracer2.Stream.Acceleration
{
    internal class BoundingVolumeHierarchyStream
    {

        private enum TraverseMode
        {
            PrimitiveWise,
            BoundingboxWise,
            HierarchyWise
        };

        private TraverseMode traverseMode = TraverseMode.PrimitiveWise;

#if DEBUG 
        private const int SIMDWidth = 4;
#else
        private const int SIMDWidth = 8;
#endif

        internal RenderComponentStream rcs;

        public BoundingVolumeHierarchyStream(RenderComponent[] rc)
        {
            rcs = new RenderComponentStream(1000);
            
            for(int j = 0; j < rc.Length; j++)
            { rcs.StoreInStream(in rc[j]); }
        }

        public int Traverse(ref RayStream rs)
        {

            switch (traverseMode)
            {
                case TraverseMode.PrimitiveWise:
                    return TraversePrimitiveWise(ref rs);

                case TraverseMode.BoundingboxWise:
                    return TraverseBoundingboxWise(ref rs);

                case TraverseMode.HierarchyWise:
                    return TraverseHierarchyWise(ref rs);

                default:
                    throw new System.Exception("Unknown traverse mode: " + traverseMode.ToString());
            }
        }

        private int TraversePrimitiveWise(ref RayStream rs)
        {

            Ray r = rs.r;
            RayPack rp = rs.rp;


            for(int j = 0; j < rcs.UsedCapacity; j += SIMDWidth)
            {
                RenderComponentPack rcp = rcs.ExtractFromStream(j);
                Vector<int> intersections = rcp.cps.Intersect(ref rp);

                if(Vector.Dot(intersections, intersections) > 0)
                { return 1; }
            }

            return 0;
        }

        private int TraverseBoundingboxWise(ref RayStream rs)
        {
            return -1;
        }

        private int TraverseHierarchyWise(ref RayStream rs)
        {
            return -1;
        }
    }
}