﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NUnit.Framework;

using Raytracer2.Scalar;
using Raytracer2.Scalar.Primitives;
using Raytracer2.Scalar.Acceleration;

namespace Raytracer2.Stream.Acceleration
{
    internal class QuadTree
    {

        private const int numberOfChildren = 4;

        /// <summary>
        /// The boundary of this quadtree.
        /// </summary>
        public BoundingBox boundary;

        /// <summary>
        /// The number of entities allowed in a cell. If there are more, we split.
        /// </summary>
        private const int occupancy = 4;

        /// <summary>
        /// Represents the current number of entities within this cell.
        /// </summary>
        private int currentOccupancy;

        /// <summary>
        /// Represents the circles within this cell.
        /// </summary>
        private RenderComponent[] circles;

        /// <summary>
        /// Determines whether this node is divided or not.
        /// </summary>
        private bool divided = false;

        /// <summary>
        /// Represents the parent of this quad tree node. If the parent is nill, then this node is the root.
        /// </summary>
        private QuadTree parent;

        /// <summary>
        /// Represents the children of this quad tree node. They are null if this node has not been divided.
        /// </summary>
        private QuadTree[] children;

        public QuadTree(Span<RenderComponent> points)
        {
            throw new NotImplementedException();
        }

        public QuadTree(BoundingBox boundary)
        {
            this.boundary = boundary;
            this.circles = new RenderComponent[occupancy];
        }

        private QuadTree(QuadTree parent, BoundingBox boundary)
        {
            this.parent = parent;
            this.boundary = boundary;
            this.circles = new RenderComponent[occupancy];
        }

        public bool OnEdge(Point2 p)
        {
            if (boundary.OnEdge(p))
            { return true; }

            if (this.divided)
            {
                foreach (QuadTree qt in children)
                {
                    bool onEdge = qt.OnEdge(p);
                    if (onEdge)
                    { return true; }
                }
            }

            return false;
        }

        public void FindComponents(BoundingBox other, ref RenderComponentStream stream)
        {
            // if we are divided, check in which kiddo the point belongs.
            if (this.divided)
            {
                for (int j = 0, l = numberOfChildren; j < l; j++)
                {
                    QuadTree qt = children[j];
                    if (qt.boundary.CollidesWith(other))
                    { qt.FindComponents(other, ref stream); }
                }
            }
            // otherwise check our local entities.
            else
            {
                for (int j = 0, l = currentOccupancy; j < l; j++)
                {
                    RenderComponent entity = circles[j];
                    BoundingBox bb = entity.ComputeBoundingBox();
                    if (bb.CollidesWith(other))
                    { stream.StoreInStream(entity); }
                }
            }
        }

        public void FindComponents(BoundingBox other, out RenderComponent[] entities)
        {
            List<RenderComponent> les = new List<RenderComponent>();
            FindComponentsInternal(other, ref les);
            entities = les.ToArray();
        }

        private void FindComponentsInternal(BoundingBox other, ref List<RenderComponent> les)
        {
            // if we are divided, check in which kiddo the point belongs.
            if (this.divided)
            {
                for (int j = 0, l = numberOfChildren; j < l; j++)
                {
                    QuadTree qt = children[j];
                    if (qt.boundary.CollidesWith(other))
                    { qt.FindComponentsInternal(other, ref les); }
                }
            }
            // otherwise check our local entities.
            else
            {
                for (int j = 0, l = currentOccupancy; j < l; j++)
                {
                    RenderComponent entity = circles[j];
                    BoundingBox bb = entity.ComputeBoundingBox();
                    if(bb.CollidesWith(other))
                    { les.Add(entity); }
                }
            }
        }

        public bool FindComponent(BoundingBox other, out RenderComponent entity)
        {
            entity = default(RenderComponent);
            return FindComponentInternal(other, ref entity);
        }

        private bool FindComponentInternal(BoundingBox other, ref RenderComponent output)
        {
            // if we are divided, check in which kiddo the point belongs.
            if (this.divided)
            {
                for (int j = 0, l = numberOfChildren; j < l; j++)
                {
                    QuadTree qt = children[j];
                    if (qt.boundary.CollidesWith(other))
                    { return qt.FindComponentInternal(other, ref output); }
                }
            }
            // otherwise check our local entities.
            else
            {
                for (int j = 0, l = currentOccupancy; j < l; j++)
                {
                    RenderComponent entity = circles[j];
                    if (entity.CollidesWith(other))
                    {
                        output = entity;
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Attempts to find an entity that contains the given point. If no entity is
        /// is found, the function will return false. If an entity is found, the
        /// function will return true and the found entity will be stored in
        /// the 'output' parameter.
        /// </summary>
        public bool FindContainingEntity(Point2 p, out RenderComponent output)
        {
            output = default(RenderComponent);
            return FindContainingEntityInternal(p, ref output);
        }

        private bool FindContainingEntityInternal(Point2 p, ref RenderComponent output)
        {
            // if we are divided, check in which kiddo the point belongs.
            if (this.divided)
            {
                for (int j = 0, l = numberOfChildren; j < l; j++)
                {
                    QuadTree qt = children[j];
                    if (qt.boundary.Contains(p))
                    { return qt.FindContainingEntity(p, out output); }
                }
            }
            // otherwise check our local entities.
            else
            {
                for (int j = 0, l = currentOccupancy; j < l; j++)
                {
                    RenderComponent entity = circles[j];
                    if (entity.ContainsPoints(p))
                    {
                        output = entity;
                        return true;
                    }
                }
            }

            return false;
        }

        public void Insert(RenderComponent p)
        {
            // we've been divided already. So many fractions, so much compromise.
            if(this.divided)
            {
                InsertIntoChildren(p);
                return;
            }

            // check if we're full
            if (this.currentOccupancy == occupancy)
            {
                // we're full! Help! Subdivide, quickly!
                Subdivide();

                // send our current entities to the subdivision.
                for(int j = 0; j < occupancy; j++)
                { InsertIntoChildren(circles[j]); }

                // and insert the new entity!!
                InsertIntoChildren(p);

                // remove our current entities.
                circles = null;
            }
            else
            {
                // we can take another entity. Fill it in!
                this.circles[currentOccupancy] = p;
                currentOccupancy++;
            }
        }

        private void InsertIntoChildren(RenderComponent p)
        {
            // compute the bounding box.
            BoundingBox bb = p.ComputeBoundingBox();

            // go over each kiddo.
            for (int j = 0, l = numberOfChildren; j < l; j++)
            {
                // if we collide with the given kiddo, insert this entity into the kiddo.
                QuadTree qt = children[j];
                if (qt.boundary.CollidesWith(bb))
                {
                    qt.Insert(p);
                    return;
                }
            }
        }

        private void Subdivide()
        {
            // we're a quad tree, so we need four children.
            const int numberOfChildren = 4;
            BoundingBox bb = this.boundary;

            // every kiddo is 1/4th of the previous area. They go clockwise, each taking a corner.
            this.children = new QuadTree[numberOfChildren];
            children[0] = new QuadTree(this,     // 0 = north west
                new BoundingBox(new Range(bb.rx.min, bb.rx.Centroid()), new Range(bb.ry.min, bb.ry.Centroid())));
            children[1] = new QuadTree(this,     // 1 = north east
                new BoundingBox(new Range(bb.rx.Centroid(), bb.rx.max), new Range(bb.ry.min, bb.ry.Centroid())));
            children[2] = new QuadTree(this,     // 2 = south east
                new BoundingBox(new Range(bb.rx.Centroid(), bb.rx.max), new Range(bb.ry.Centroid(), bb.ry.max)));
            children[3] = new QuadTree(this,     // 3 = south west
                new BoundingBox(new Range(bb.rx.min, bb.rx.Centroid()), new Range(bb.ry.Centroid(), bb.ry.max)));

            // we're no longer one whole shape :(.
            this.divided = true;
        }
    }
}
