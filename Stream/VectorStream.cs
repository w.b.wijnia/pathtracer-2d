﻿
using System;
using System.Threading;

using Raytracer2.Scalar;
using Raytracer2.Pack;

namespace Raytracer2.Stream
{
    internal struct VectorStream : iStream<Vector2, VectorPack>
    {

        private float[] xs;

        private float[] ys;

        /// <summary>
        /// Represents the next element that is free.
        /// </summary>
        private int nextFreeElement;

        public int MaximumCapacity => xs.Length;

        public int UsedCapacity => nextFreeElement;

        public VectorStream(int n)
        {
            this.xs = new float[n];
            this.ys = new float[n];

            this.nextFreeElement = 0;
        }

        public void StoreInStream(in VectorPack s, int offset)
        {
            s.x.CopyTo(this.xs, offset);
            s.y.CopyTo(this.ys, offset);
        }

        public void StoreInStream(in Vector2 c)
        { StoreInStream(c.x, c.y); }

        private void StoreInStream(float x, float y)
        {
            int index = Interlocked.Increment(ref nextFreeElement);
            xs[index] = x;
            ys[index] = y;
        }

        /// <summary>
        /// Performance-wise, this overload is advised. Extracts a couple of elements from the stream and stores them in the struct. Number of structs is equal to the length of the SIMD lanes available.
        /// </summary>
        /// <param name="offset">The offset of where to start within the stream.</param>
        /// <returns></returns>
        public VectorPack ExtractFromStream(int offset)
        { return new VectorPack(in xs, in ys, offset); }

        public void Clear()
        {
            this.nextFreeElement = 0;
        }

        public void SwapInStream(int a, int b)
        {
            float lx = xs[a];
            xs[a] = xs[b];
            xs[b] = lx;

            float ly = ys[a];
            ys[a] = ys[b];
            ys[b] = ly;
        }
    }
}