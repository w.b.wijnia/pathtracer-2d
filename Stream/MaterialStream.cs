﻿
using System;
using System.Threading;

using Raytracer2.Scalar;
using Raytracer2.Pack;

namespace Raytracer2.Stream
{
    internal struct MaterialStream : iStream<Material, MaterialPack>
    {
        ColorStream cs;

        float[] es;

        /// <summary>
        /// Represents the next element that is free.
        /// </summary>
        private int nextFreeElement;

        public MaterialStream(int n)
        {
            this.cs = new ColorStream(n);
            this.es = new float[n];

            this.nextFreeElement = 0;
        }

        public int MaximumCapacity => es.Length;

        public int UsedCapacity => nextFreeElement;

        public MaterialPack ExtractFromStream(int offset)
        {
            ColorPack cp = cs.ExtractFromStream(offset);
            return new MaterialPack(cp, es, offset);
        }

        public void StoreInStream(in Material r)
        {
            int id = Interlocked.Increment(ref nextFreeElement);
            this.cs.StoreInStream(r.diffuse);
            this.es[id] = r.emissive;
        }

        public void StoreInStream(in MaterialPack s, int offset)
        {
            cs.StoreInStream(in s.cp, offset);
            s.ep.CopyTo(es, offset);
        }

        public void Clear()
        {
            cs.Clear();
            this.nextFreeElement = 0;
        }

        public void SwapInStream(int a, int b)
        {
            cs.SwapInStream(a, b);

            float local = es[a];
            es[a] = es[b];
            es[b] = local;
        }
    }
}
