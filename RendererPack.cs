﻿//#define PACKED_ON_ONE_PIXEL

using System;

using OpenTK.Graphics.OpenGL;

using System.Threading;
using System.Diagnostics;

using Raytracer2.Scalar.Primitives;
using Raytracer2.Pack.Primitives;

using Raytracer2.Scalar.Acceleration;
using Raytracer2.Pack.Acceleration;
using Raytracer2.Scalar;
using Raytracer2.Pack;

using System.Numerics;

namespace Raytracer2
{
    internal class RendererPack : Renderer
    {
        // -- tweakable values -- \\

        /// <summary>
        /// Represents the size of the square that a single thread will work on.
        /// </summary>
        private const int squareSize = Constants.numberOfSIMDLanes;

        // -- end of tweakable values -- \\

        /// <summary>
        /// Used to signal the threads that they can start working.
        /// </summary>
        private EventWaitHandle signalToThread;

        /// <summary>
        /// Used to signal to main that the thread is done.
        /// </summary>
        private EventWaitHandle[] signalToMain;

        /// <summary>
        /// Contains the threads used for rendering.
        /// </summary>
        private Thread[] ts;

        /// <summary>
        /// Used by the threads to compute random numbers.
        /// </summary>
        private iRandom[] rngs;

        /// <summary>
        /// Represents the number of jobs left for the renderer.
        /// </summary>
        private int jobs;

        /// <summary>
        /// Represents the number of (logical) processors.
        /// </summary>
        private int processors;

        /// <summary>
        /// Represents the color buffer that we fill during rendering.
        /// </summary>
        protected Color[] outputBuffer;

        BoundingVolumeHierarchy<Circle> bvh;
        BoundingVolumeHierarchyPack bvhp;

        Stopwatch w = new Stopwatch();
        Stopwatch bvhConstruction = new Stopwatch();

        private int raysPerPixel = 1;

        Vector<float> vw, vh;

        /// <summary>
        /// Constructs a new camera with a resolution of w(idth) * h(eight). 
        /// </summary>
        public RendererPack(int w, int h, int rs)
            : base(w, h)
        {
            raysPerPixel = rs;
            processors = System.Environment.ProcessorCount;

            this.vw = new Vector<float>(width);
            this.vh = new Vector<float>(height);

            ts = new Thread[processors];
            rngs = new iRandom[processors];
            signalToMain = new EventWaitHandle[processors];
            signalToThread = new EventWaitHandle(false, EventResetMode.ManualReset);

            for (int j = 0, l = processors; j < l; j++)
            {
                rngs[j] = new RXor((uint)(821 + j));
                signalToMain[j] = new EventWaitHandle(false, EventResetMode.ManualReset);

                // we need to createa a new variable, or things may go bad.
                int threadNumber = j;
                ts[threadNumber] = new Thread(() => ThreadWorker(threadNumber));
                ts[threadNumber].Start();
            }

            // construct the screen and our internal buffer.
            outputBuffer = new Color[w * h];

            screenid = GL.GenTexture();
            GL.BindTexture(TextureTarget.Texture2D, screenid);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, width, height, 0, PixelFormat.Rgba, PixelType.Float, outputBuffer);
        }

        public override void Render(Camera camera, Hierarchy hierarchy)
        {
            tickNumber++;

            this.camera = camera;

            // ------------------------------------------------------- \\
            // BVH construction                                        \\

            bvhConstruction.Start();
            bvh = new BoundingVolumeHierarchy<Circle>(hierarchy.GetOccluders);
            bvhConstruction.Stop();
            Diagnostic.TimeAccelerationStructure = bvhConstruction.ElapsedMilliseconds;
            bvhConstruction.Reset();

            this.bvhp = new BoundingVolumeHierarchyPack(bvh);

            w.Reset();
            w.Start();

            StartAndWaitForThreads();

            GL.DeleteTextures(1, ref screenid);

            screenid = GL.GenTexture();
            GL.BindTexture(TextureTarget.Texture2D, screenid);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, width, height, 0, PixelFormat.Rgba, PixelType.Float, outputBuffer);

            w.Stop();
            Diagnostic.TimeRender = w.ElapsedMilliseconds;
        }

        private void StartAndWaitForThreads()
        {
            // compute the number of jobs. Due to integer rounding, we always add one additional (half or empty) job.
            jobs = ((width / squareSize) + 1) * ((height / squareSize) + 1);

            // reset the 'i am done' signals, then put the workers to work.
            for (int j = 0; j < processors; j++)
            {
                signalToMain[j].Reset();
            }

            // workers be like: vrrroooooooommM!!
            signalToThread.Set();

            // wait for the workers to be done.
            for (int j = 0; j < processors; j++)
            {
                signalToMain[j].WaitOne(-1);        // main be like: zzzzzzzz....
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="threadNumber"></param>
        private void ThreadWorker(int threadNumber)
        {
            Stopwatch diffuseWatch = new Stopwatch();
            Stopwatch lightWatch = new Stopwatch();
            Stopwatch combineWatch = new Stopwatch();

            Span<Color> diffuseSpan = stackalloc Color[squareSize * squareSize];
            Span<Color> lightSpan = stackalloc Color[squareSize * squareSize];

            while (true)
            {
                Interlocked.Add(ref Diagnostic.timeDiffuse, (int)diffuseWatch.ElapsedMilliseconds);
                Interlocked.Add(ref Diagnostic.timeLighting, (int)lightWatch.ElapsedMilliseconds);
                Interlocked.Add(ref Diagnostic.timeCombined, (int)combineWatch.ElapsedMilliseconds);

                diffuseWatch.Reset();
                lightWatch.Reset();
                combineWatch.Reset();

                // wait for the start signal...
                signalToThread.WaitOne(-1);

                while (true)
                {
                    // ... and go!

                    // pick up a job.
                    int job = Interlocked.Decrement(ref jobs);

                    // are we already done?
                    if (job < 0)
                    {
                        // reset our selves, tell the application we're done.
                        signalToMain[threadNumber].Set();

                        break;
                    }

                    // Compute our x / y values.
                    int s = height / squareSize;
                    int x = job / s;
                    int y = job - x * s;

                    // example of computing x and y value. Given that:
                    //  - width         = 400
                    //  - height        = 400
                    //  - squareSize    = 10
                    // this results in 1681 jobs.

                    // say that we have job number 801.
                    // then the x component of the square is 801 / (400 / 10) = 20.
                    // from here on, y is equal to the data dat was 'lost' due to integer division (the remainder). In this case, that is the 1.

                    int xi = x * squareSize;
                    int yi = y * squareSize;

                    diffuseWatch.Start();
                    RenderDiffuseSquare(ref diffuseSpan, ref raysPerPixel, rngs[threadNumber], xi, yi);
                    diffuseWatch.Stop();

                    lightWatch.Start();
                    RenderLightingSquarePacked(ref lightSpan, ref raysPerPixel, rngs[threadNumber], xi, yi);
                    lightWatch.Stop();

                    combineWatch.Start();
                    RenderCombineSquare(ref diffuseSpan, ref lightSpan, xi, yi);
                    combineWatch.Stop();
                }
            }
        }

        private void RenderCombineSquare(ref Span<Color> diffuseBuffer, ref Span<Color> lightingBuffer, int gx, int gy)
        {
            float ratio = 1.0f / tickNumber;

            for (int sy = 0; sy < squareSize; sy++)
            {
                int iy = Math.Min(sy + gy, height - 1);

                for (int sx = 0; sx < squareSize; sx++)
                {
                    int ix = Math.Min(sx + gx, width - 1);

                    outputBuffer[ix + iy * width] = diffuseBuffer[sx + sy * squareSize] * lightingBuffer[sx + sy * squareSize];
                }
            }
        }

        private void RenderDiffuseSquare(ref Span<Color> localBuffer, ref int rays, iRandom rng, int gx, int gy)
        {
            float ratio = 1.0f / tickNumber;
            float impact = (1.0f / 1);

            // ------------------------------------------------------- \\
            // Compute the color values in a 'local' buffer.           \\

            for (int sy = 0; sy < squareSize; sy++)
            {
                for (int sx = 0; sx < squareSize; sx++)
                {
                    int iy = Math.Min(sy + gy, height - 1);
                    int ix = Math.Min(sx + gx, width - 1);

                    localBuffer[sx + sy * squareSize] = new Color(0, 0, 0);

                    for (int rn = 0, l = 1; rn < l; rn++)
                    { localBuffer[sx + sy * squareSize] += impact * RenderDiffuseWork(rng, ix, iy); }
                }
            }
        }

        private Color RenderDiffuseWork(iRandom rng, int x, int y)
        {
            Color output = new Color(1, 1, 1);

            // -- determine the origin of the ray -- \\
            float dx = ((float)x - width) / width + 0.5f;
            float dy = ((float)y - height) / height + 0.5f;

            float ox = 1.0f / width * (0.5f);
            float oy = 1.0f / height * (0.5f);
            Point2 rayOrigin = this.camera.origin + new Scalar.Vector2((ox + dx) * this.camera.halfExtend.x, (oy + dy * this.camera.halfExtend.y));

            // -- determine the color of the ray -- \\

            //if (bvh.OnNode(rayOrigin))
            //{ return new Color(1, 0, 1); }
            //else
            {
                RenderComponent component;
                if (bvh.Contains(ref rayOrigin, out component))
                {
                    // retrieve the material.
                    Material primitiveMaterial = component.m;

                    float e = System.Math.Min(primitiveMaterial.emissive, 1.0f);
                    Color mixed = new Color(
                        e * output.r + (1 - e) * primitiveMaterial.diffuse.r,
                        e * output.g + (1 - e) * primitiveMaterial.diffuse.g,
                        e * output.b + (1 - e) * primitiveMaterial.diffuse.b
                    );

                    output = mixed;
                }
            }

            return output;
        }

        private void RenderLightingSquarePacked(ref Span<Color> localBuffer, ref int rays, iRandom rng, int gx, int gy)
        {
            float ratio = 1.0f / tickNumber;
            float impact = (1.0f / rays);

            // ------------------------------------------------------- \\
            // Compute the color values in a 'local' buffer.           \\

            for (int sy = 0; sy < squareSize; sy++)
            {
#if PACKED_ON_ONE_PIXEL
                for (int sx = 0; sx < squareSize; sx++)
                {
                    int iy = Math.Min(sy + gy, height - 1);
                    int ix = Math.Min(sx + gx, width - 1);

                    Vector<float> vy = new Vector<float>(iy);
                    Vector<float> vx = new Vector<float>(ix);

                    localBuffer[sx + sy * squareSize] = new Color(0, 0, 0);

                    for (int rn = 0, l = raysPerPixel; rn < l; rn++)
                    {
                        ColorPack cp = (impact * (0.125f)) * RenderLightingWorkPacked(rng, vx, vy);
                        Color c = new Color(0, 0, 0);

                        for(int j = 0; j < 8; j++)
                        {
                            c.r += cp.r[j];
                            c.g += cp.g[j];
                            c.b += cp.b[j];
                        }
                        

                        localBuffer[sx + sy * squareSize] += c;
                    }
                }
#else
                for (int sx = 0; sx < squareSize; sx++)
                {
                    localBuffer[sx + sy * squareSize] = new Color(0, 0, 0);
                }
                {
                    int iy = Math.Min(sy + gy, height - 1);
                    Vector<float> vy = new Vector<float>(iy);

                    float[] ax = new float[Constants.numberOfSIMDLanes];
                    for (int j = 0; j < Constants.numberOfSIMDLanes; j++) { ax[j] = gx + j; }
                    Vector<float> vx = new Vector<float>(ax);

                    

                    ColorPack cp = new ColorPack();
                    for (int rn = 0, l = raysPerPixel; rn < l; rn++)
                    { cp += impact * RenderLightingWorkPacked(rng, vx, vy); }

                    for (int j = 0; j < Constants.numberOfSIMDLanes; j++)
                    {
                        Color c = new Color(cp.r[j], cp.g[j], cp.b[j]);
                        localBuffer[j + sy * squareSize] = c;
                    }
                }
#endif
            }
        }

        private ColorPack RenderLightingWorkPacked(iRandom rng, Vector<float> vx, Vector<float> vy)
        {
            Vector<float> pointFive = new Vector<float>(0.5f);
            Vector<float> ones = new Vector<float>(1.0f);
            Vector<float> epsilonv = new Vector<float>(0.0001f);

            // ------------------------------------------------------- \\
            // The default color.                                      \\

            float[] b = new float[] { 0, 0, 0, 0, 0, 0, 0, 0 };
            ColorPack output = new ColorPack(b, b, b);

            // -- determine the origin of the ray -- \\
            Vector<float> dx = (vx - vw) / vw + pointFive;
            Vector<float> dy = (vy - vh) / vh + pointFive;

            Vector<float> ox = ones / vw * (rng.Floats() - pointFive);
            Vector<float> oy = ones / vh * (rng.Floats() - pointFive);
            PointPack rayOrigin = new PointPack(this.camera.origin) + new VectorPack(this.camera.halfExtend.x * (ox + dx), this.camera.halfExtend.y * (oy + dy));

            // -- determine what lights influence this location -- \\
            RenderComponentPack[] components = bvhp.efficientPrimitives;
            for (int j = 0; j < components.Length; j++)
            {
                RenderComponentPack lightPrimitive = components[j];
                MaterialPack lightMaterial = lightPrimitive.mp;

                if (lightMaterial.ep[0] < 0.01f)
                { continue; }

                // -- compute the ray for the given light -- \\
                PointPack lightOrigin = lightPrimitive.cps.RandomPosition(rng);
                VectorPack direction = rayOrigin.DirectionTo(lightOrigin).Normalized();

                RayPack lightRay = new RayPack(rayOrigin, direction, new Vector<float>(float.MaxValue));
                lightRay.Intersect(lightPrimitive.cps);

                RayPack sceneRay = new RayPack(rayOrigin, direction, lightRay.t - epsilonv);

                // -- if it may influence this pixel, compute whether or not it is occluded. -- \\
                Vector<int> occluded = bvhp.Traverse(ref sceneRay);

                Vector<float> attenuation = ones / ((lightRay.t + ones) * (lightRay.t + ones));
                ColorPack result = (lightMaterial.ep * attenuation) * lightMaterial.cp;

                output.r = Vector.ConditionalSelect(occluded, output.r, output.r + result.r);
                output.g = Vector.ConditionalSelect(occluded, output.g, output.g + result.g);
                output.b = Vector.ConditionalSelect(occluded, output.b, output.b + result.b);
            }

            return 2.0f * output;
        }
    }
}