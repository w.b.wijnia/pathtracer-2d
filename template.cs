﻿using System;
using System.Diagnostics;
using System.Collections.Generic;

using Raytracer2.Stream.Acceleration;

using Raytracer2.Scalar.Acceleration;
using Raytracer2.Scalar.Primitives;
using Raytracer2.Scalar;

using BenchmarkDotNet.Running;
using Raytracer2.Benchmarking;

using OpenTK;
using OpenTK.Input;
using OpenTK.Graphics.OpenGL;

namespace Raytracer2
{
    public class OpenTKApp : GameWindow
    {
        enum RendererToUse
        {
            // CPU versions
            Scalar, Packed, Stream,

            // GPU versions
            ScalarCloo, PackedCloo, StreamCloo
        };

        RendererToUse selection = RendererToUse.Stream;

        const int raysPerPixel = 1;
        const bool animate = true;
        const int w = 640;
        const int h = 480;
        const int o = 0;

        static bool terminated = false;

        Renderer renderer;
        Camera camera;
        BoundingVolumeHierarchy<Circle> bvh;

        Stopwatch otherWatch = new Stopwatch();
        Stopwatch totalWatch = new Stopwatch();
        Stopwatch hierarchyWatch = new Stopwatch();

        List<RenderComponent> primitives = new List<RenderComponent>();
        Hierarchy hierarchy;

        protected override void OnLoad(EventArgs e)
        {
            this.VSync = VSyncMode.Off;

            // called during application initialization
            GL.ClearColor(0, 0, 0, 0);
            GL.Enable(EnableCap.Texture2D);
            GL.Disable(EnableCap.DepthTest);
            GL.Hint(HintTarget.PerspectiveCorrectionHint, HintMode.Nicest);

            List<Material> materials = new List<Material>();
            materials.Add(new Material(new Color(1.0f, 0.0f, 0.0f), 0.0f));
            materials.Add(new Material(new Color(0.0f, 1.0f, 0.4f), 0.0f));
            materials.Add(new Material(new Color(0.0f, 0.0f, 1.0f), 0.0f));
            materials.Add(new Material(new Color(0.78f, 0.2f, .80f), 0.0f));

            materials.Add(new Material(new Color(081f / 255f, 186f / 255f, 056f / 255f), 1.0f));
            materials.Add(new Material(new Color(057f / 255f, 234f / 255f, 197f / 255f), 1.0f));
            materials.Add(new Material(new Color(125f / 255f, 069f / 255f, 224f / 255f), 1.0f));
            materials.Add(new Material(new Color(223f / 255f, 072f / 255f, 229f / 255f), 1.0f));
            materials.Add(new Material(new Color(232f / 255f, 085f / 255f, 104f / 255f), 1.0f));
            materials.Add(new Material(new Color(234f / 255f, 219f / 255f, 077f / 255f), 1.0f));
            materials.Add(new Material(new Color(237f / 255f, 178f / 255f, 088f / 255f), 1.0f));

            // seeds with noise: 1, 2, 3, 4
            Random rng = new Random(6);
            for (int j = 0; j < 200; j++)
            {
                Point2 p = new Point2(0, 0) + (21 * ((float)rng.NextDouble() + 0.1f)) * new Scalar.Vector2((float)rng.NextDouble() - 0.5f, (float)rng.NextDouble() - 0.5f).Normalized();
                primitives.Add(new RenderComponent(
                    new Circle(p, (float)rng.NextDouble() * 0.15f + 0.05f),
                    materials[rng.Next(0, 4)])
                );
            }

            for (int j = 0; j < 30; j++)
            {
                Point2 p = new Point2(0, 0) + (21 * ((float)rng.NextDouble() + 0.1f)) * new Scalar.Vector2((float)rng.NextDouble() - 0.5f, (float)rng.NextDouble() - 0.5f).Normalized();
                primitives.Add(new RenderComponent(
                    new Circle(p, (float)rng.NextDouble() * 0.25f + 0.1f),
                    materials[rng.Next(4, 11)])
                );
            }

            ClientSize = new System.Drawing.Size(w, h);
            PrepareRenderer(selection);
        }

        private void PrepareRenderer(RendererToUse rtu)
        {
            this.hierarchy = new Hierarchy(10000);

            this.camera = new Camera(
                new Scalar.Point2(0, 0),
                3 * new Scalar.Vector2(15, 15 * ((float)h / w))
                );

            switch (rtu)
            {

                case RendererToUse.Packed:
                    renderer = new RendererPack(
                        w >> o, h >> o,
                        raysPerPixel);
                    break;

                case RendererToUse.PackedCloo:
                    renderer = new RendererCloo(
                        w >> o, h >> o,
                        bvh,
                        raysPerPixel);
                    break;

                case RendererToUse.Stream:
                    renderer = new RendererStream(
                        w >> o, h >> o,
                        raysPerPixel);
                    break;

                case RendererToUse.StreamCloo:

                    break;

                case RendererToUse.Scalar:
                    renderer = new RendererScalar(
                        w >> o, h >> o,
                        raysPerPixel);
                    break;

                case RendererToUse.ScalarCloo:

                    break;


                default:

                    Console.Error.Write("Unknown renderer chosen. Intentionally crashing the application.");

                break;
            }

        }

        protected override void OnUnload(EventArgs e)
        {
            // called upon app close
            GL.DeleteTextures(1, ref renderer.screenid);
            Environment.Exit(0);      // bypass wait for key on CTRL-F5
        }
        protected override void OnResize(EventArgs e)
        {
            // called upon window resize. Note: does not change the size of the pixel buffer.
            GL.Viewport(0, 0, Width, Height);
            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadIdentity();
            GL.Ortho(-1.0, 1.0, -1.0, 1.0, 0.0, 4.0);
        }
        protected override void OnUpdateFrame(FrameEventArgs e)
        {
            if (animate)
            {
                for (int j = 0, l = primitives.Count; j < l; j++)
                {
                    if (primitives[j].m.emissive < 0.01f)
                    {
                        RenderComponent rc = primitives[j];
                        rc.c.origin = rc.c.origin.Rotated(0.25f);
                        primitives[j] = rc;
                    }
                }

                renderer.ResetAccumalation();
            }

            hierarchyWatch.Reset();
            hierarchyWatch.Start();

            hierarchy.Construct(primitives.ToArray());

            hierarchyWatch.Stop();
            Diagnostic.TimeHierarchy = hierarchyWatch.ElapsedMilliseconds;
        }

        protected override void OnRenderFrame(FrameEventArgs e)
        {
            totalWatch.Start();
            otherWatch.Stop();
            Diagnostic.TimeOther = otherWatch.ElapsedMilliseconds;
            otherWatch.Reset();

            Diagnostic.WriteToConsole();
            Diagnostic.Reset();

            if (terminated)
            {
                Exit();
                return;
            }

            // ------------------------------------------------------- \\
            // Rendering                                               \\

            renderer.Render(camera, hierarchy);

            GL.ClearColor(System.Drawing.Color.Black);
            GL.Color3(1.0f, 1.0f, 1.0f);

            // convert MyApplication.screen to OpenGL texture
            GL.BindTexture(TextureTarget.Texture2D, renderer.screenid);

            // draw screen filling quad
            GL.Begin(PrimitiveType.Quads);
            GL.TexCoord2(0.0f, 1.0f); GL.Vertex2(-1.0f, -1.0f);
            GL.TexCoord2(1.0f, 1.0f); GL.Vertex2(1.0f, -1.0f);
            GL.TexCoord2(1.0f, 0.0f); GL.Vertex2(1.0f, 1.0f);
            GL.TexCoord2(0.0f, 0.0f); GL.Vertex2(-1.0f, 1.0f);
            GL.End();

            // tell OpenTK we're done rendering
            SwapBuffers();

            GL.Flush();

            otherWatch.Start();
            totalWatch.Stop();
            Diagnostic.timeTotal = (int)totalWatch.ElapsedMilliseconds;
            totalWatch.Reset();
        }

        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            base.OnMouseDown(e);

            int x = e.X;
            int y = e.Y;

            if (x >= 0 && x <= ClientSize.Width)
            {
                if (y >= 0 && y <= ClientSize.Height)
                {
                    renderer.ResetAccumalation();
                }
            }
        }

        public static void Main(string[] args)
        {
            //var summary = BenchmarkRunner.Run<CircleStreamBenchmarks>(); Console.ReadLine();

            // entry point
            using (OpenTKApp app = new OpenTKApp()) { app.Run(120, 0.0); }

        }
    }
}