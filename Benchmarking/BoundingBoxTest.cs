﻿
using System;
using System.Numerics;

using Raytracer2.Scalar;
using Raytracer2.Scalar.Acceleration;
using Raytracer2.Scalar.Primitives;
using Raytracer2.Pack.Primitives;
using Raytracer2.Pack.Acceleration;

using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Columns;
using BenchmarkDotNet.Configs;
using BenchmarkDotNet.Diagnosers;
using BenchmarkDotNet.Environments;
using BenchmarkDotNet.Exporters;
using BenchmarkDotNet.Jobs;
using BenchmarkDotNet.Loggers;
using BenchmarkDotNet.Order;
using BenchmarkDotNet.Reports;
using BenchmarkDotNet.Running;
using BenchmarkDotNet.Toolchains.CsProj;



namespace Raytracer2.Benchmarking
{

    [MemoryDiagnoser]
    [DisassemblyDiagnoser(printAsm: true, printSource: true)]
    [RyuJitX64Job]
    public class BoundingBoxTest
    {
        int n = 8000 * 8 * 8;

        Ray[] rs;
        BoundingBox bb;

        RayPack[] rps;
        BoundingBoxPack bbp;

        [Benchmark]
        public void Intersection()
        {
            for (int j = 0, l = n; j < l; j++)
            {
                float t;
                bb.CollidesWith(rs[j], out t);
            }
        }

        [Benchmark]
        public void IntersectionPacked()
        {
            for (int j = 0, l = n / 8; j < l; j++)
            {
                Vector<float> t;
                bbp.CollidesWith(ref rps[j], out t);
            }
        }

        public BoundingBoxTest()
        {
            Random rng = new Random(1);

            bb = new BoundingBox(new Range(-1, 1), new Range(-1, 1));
            bbp = new BoundingBoxPack(bb);

            rs = new Ray[n];
            rps = new RayPack[n / 8];

            for(int j = 0, l = n / 8; j < l; j++)
            {
                float[] ox = new float[8];
                float[] oy = new float[8];
                float[] dx = new float[8];
                float[] dy = new float[8];
                float[] ts = new float[8];

                for(int i = 0; i < 8; i++)
                {
                    Point2 p = new Point2(
                        10 * (float)rng.NextDouble(),
                        10 * (float)rng.NextDouble()
                        );

                    ox[i] = p.x;
                    oy[i] = p.y;

                    float t = float.MaxValue;
                    ts[i] = t;

                    Scalar.Vector2 v = new Scalar.Vector2(
                        (float)rng.NextDouble() - 0.5f,
                        (float)rng.NextDouble() - 0.5f
                        ).Normalized();

                    dx[i] = v.x;
                    dy[i] = v.y;

                    rs[j * 8 + i] = new Ray(p, v, t);
                }

                rps[j] = new RayPack(ox, oy, dx, dy, ts);
            }
        }
    }
}