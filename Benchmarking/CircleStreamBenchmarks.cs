﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Columns;
using BenchmarkDotNet.Configs;
using BenchmarkDotNet.Diagnosers;
using BenchmarkDotNet.Environments;
using BenchmarkDotNet.Exporters;
using BenchmarkDotNet.Jobs;
using BenchmarkDotNet.Loggers;
using BenchmarkDotNet.Order;
using BenchmarkDotNet.Reports;
using BenchmarkDotNet.Running;
using BenchmarkDotNet.Toolchains.CsProj;

using Raytracer2.Stream.Primitives;
using Raytracer2.Pack.Primitives;
using Raytracer2.Scalar.Primitives;
using Raytracer2.Scalar;

namespace Raytracer2.Benchmarking
{
    [MemoryDiagnoser]
    [DisassemblyDiagnoser(printAsm: true, printSource: true)]
    [RyuJitX64Job]
    public class CircleStreamBenchmarks
    {
        int n = 64000 * 8;
        private CircleStream cStream;

        [Benchmark]
        public void ExtractSetThroughArray()
        {
            for (int j = 0, l = n - 8; j < l; j +=8)
            { CirclePack cs = cStream.ExtractFromStream(j); }
        }

        [Benchmark]
        public void ExtractSetThroughSpan()
        {
            for (int j = 0, l = n - 8; j < l; j++)
            { CirclePack cs = cStream.ExtractFromStream(j); }
        }

        [GlobalSetup]
        public void Setup()
        {
            cStream = new CircleStream(n);

            for(int j = 0; j < n - 8; j += 8)
            { cStream.StoreInStream(new Circle(new Point2(j, j), j)); }
        }

    }
}