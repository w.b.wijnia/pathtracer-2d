﻿
using System.Numerics;

namespace Raytracer2
{
    // Implemented from:
    // https://excamera.com/sphinx/article-xorshift.html
    public struct RXor : iRandom
    {
        /// <summary>
        /// Represents the maximum SIMD width that we garantue to support.
        /// </summary>
        private const int maximumSIMDWidth = 16;

        /// <summary>
        /// Represents the seed that is used to initialize the buffer.
        /// </summary>
        private uint seed;

        /// <summary>
        /// Represnts the buffer of random values. The vectors are initialised from this buffer.
        /// </summary>
        private float[] buffer;

        /// <summary>
        /// Represents the current offset within the buffer of random values.
        /// </summary>
        private int offset;

        /// <summary>
        /// Constructs an RXor struct and initialises the buffer with random values.
        /// </summary>
        public RXor(uint seed, int bufferSize = 4096)
        {
            this.seed = seed;
            this.offset = 0;

            this.buffer = new float[bufferSize];
            for (int j = 0; j < bufferSize; j++)
            { buffer[j] = ComputeRandomValue(); }
        }

        /// <summary>
        /// Computes the next seed to use for the next random number.
        /// </summary>
        /// <returns></returns>
        private float ComputeRandomValue()
        {
            seed ^= seed << 13;
            seed ^= seed >> 17;
            seed ^= seed << 5;
            return ((float)seed) / ((float)uint.MaxValue);
        }

        public float Float()
        {
            this.offset = (int)(ComputeRandomValue() * (buffer.Length - maximumSIMDWidth));
            return buffer[offset];
        }

        /// <summary>
        /// Extracts a set of floats from the buffer in a non-repeative manner.
        /// </summary>
        public Vector<float> Floats()
        {
            this.offset = (int)(ComputeRandomValue() * (buffer.Length - maximumSIMDWidth));
            return new Vector<float>(buffer, offset);
        }
    }
}