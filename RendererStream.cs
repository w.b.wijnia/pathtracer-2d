﻿using System;

using OpenTK.Graphics.OpenGL;

using System.Threading;
using System.Diagnostics;

using Raytracer2.Scalar.Primitives;
using Raytracer2.Pack.Primitives;

using Raytracer2.Scalar.Acceleration;
using Raytracer2.Pack.Acceleration;
using Raytracer2.Scalar;
using Raytracer2.Pack;

using Raytracer2.Stream;
using Raytracer2.Stream.Primitives;
using Raytracer2.Stream.Acceleration;

namespace Raytracer2
{
    internal class RendererStream : Renderer
    {
        // -- tweakable values -- \\

        /// <summary>
        /// Represents the size of the square that a single thread will work on.
        /// </summary>
        private const int squareSize = 16;

        // -- end of tweakable values -- \\

        /// <summary>
        /// Used to signal the threads that they can start working.
        /// </summary>
        private EventWaitHandle signalToThread;

        /// <summary>
        /// Used to signal to main that the thread is done.
        /// </summary>
        private EventWaitHandle[] signalToMain;

        /// <summary>
        /// Contains the threads used for rendering.
        /// </summary>
        private Thread[] ts;

        /// <summary>
        /// Used by the threads to compute random numbers.
        /// </summary>
        private iRandom[] rngs;

        /// <summary>
        /// Represents the number of jobs left for the renderer.
        /// </summary>
        private int jobs;

        /// <summary>
        /// Represents the number of (logical) processors.
        /// </summary>
        private int processors;

        /// <summary>
        /// Represents the color buffer that we fill during rendering.
        /// </summary>
        protected Color[] outputBuffer;

        Stopwatch renderTotal = new Stopwatch();
        Stopwatch quadTreeConstruction = new Stopwatch();

        QuadTree qt;

        Hierarchy hierarchy;

        /// <summary>
        /// Constructs a new camera with a resolution of w(idth) * h(eight). 
        /// </summary>
        public RendererStream(int w, int h, int rs)
            : base(w, h)
        {
            processors = System.Environment.ProcessorCount;

            ts = new Thread[processors];
            rngs = new iRandom[processors];
            signalToMain = new EventWaitHandle[processors];
            signalToThread = new EventWaitHandle(false, EventResetMode.ManualReset);

            for (int j = 0, l = processors; j < l; j++)
            {
                rngs[j] = new RXor((uint)(821 + j));
                signalToMain[j] = new EventWaitHandle(false, EventResetMode.ManualReset);

                // we need to createa a new variable, or things may go bad.
                int threadNumber = j;
                ts[threadNumber] = new Thread(() => ThreadWorker(threadNumber));
                ts[threadNumber].Start();
            }

            // construct the screen and our internal buffer.
            outputBuffer = new Color[w * h];

            screenid = GL.GenTexture();
            GL.BindTexture(TextureTarget.Texture2D, screenid);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, width, height, 0, PixelFormat.Rgba, PixelType.Float, outputBuffer);
        }

        public override void Render(Camera camera, Hierarchy hierarchy)
        {
            tickNumber++;

            // store some data
            this.camera = camera;
            this.hierarchy = hierarchy;

            // -------------------------------------------------------- \\
            // Build the acceleration structure.                        \\

            quadTreeConstruction.Reset();
            quadTreeConstruction.Start();
            qt = new QuadTree(new BoundingBox(
                new Range(camera.origin.x - camera.halfExtend.x, camera.origin.x + camera.halfExtend.x),
                new Range(camera.origin.y - camera.halfExtend.y, camera.origin.y + camera.halfExtend.y)
            ));

            Span<RenderComponent> occluders = hierarchy.GetOccluders;
            for (int j = 0, l = occluders.Length; j < l; j++)
            { qt.Insert(occluders[j]); }

            quadTreeConstruction.Stop();
            Diagnostic.TimeAccelerationStructure = quadTreeConstruction.ElapsedMilliseconds;

            // -------------------------------------------------------- \\
            // Render the scene.                                        \\

            renderTotal.Reset();
            renderTotal.Start();

            StartAndWaitForThreads();

            GL.DeleteTextures(1, ref screenid);

            screenid = GL.GenTexture();
            GL.BindTexture(TextureTarget.Texture2D, screenid);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, width, height, 0, PixelFormat.Rgba, PixelType.Float, outputBuffer);

            renderTotal.Stop();
            Diagnostic.TimeRender = renderTotal.ElapsedMilliseconds;
        }

        private void StartAndWaitForThreads()
        {
            // compute the number of jobs. Due to integer rounding, we always add one additional (half or empty) job.
            jobs = ((width / squareSize) + 1) * ((height / squareSize) + 1);

            // reset the 'i am done' signals, then put the workers to work.
            for (int j = 0; j < processors; j++)
            {
                signalToMain[j].Reset();
            }

            // workers be like: vrrroooooooommM!!
            signalToThread.Set();

            // wait for the workers to be done.
            for (int j = 0; j < processors; j++)
            {
                signalToMain[j].WaitOne(-1);        // main be like: zzzzzzzz....
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="threadNumber"></param>
        private void ThreadWorker(int threadNumber)
        {
            Stopwatch diffuseWatch = new Stopwatch();
            Stopwatch lightWatch = new Stopwatch();
            Stopwatch combineWatch = new Stopwatch();

            Span<Color> diffuseSpan = stackalloc Color[squareSize * squareSize];
            Span<Color> lightSpan = stackalloc Color[squareSize * squareSize];
            RenderComponentStream rsc = new RenderComponentStream(1000);

            while (true)
            {
                Interlocked.Add(ref Diagnostic.timeDiffuse, (int)((float)diffuseWatch.ElapsedMilliseconds / processors));
                Interlocked.Add(ref Diagnostic.timeLighting, (int)((float)lightWatch.ElapsedMilliseconds / processors));
                Interlocked.Add(ref Diagnostic.timeCombined, (int)((float)combineWatch.ElapsedMilliseconds / processors));

                diffuseWatch.Reset();
                lightWatch.Reset();
                combineWatch.Reset();

                // wait for the start signal...
                signalToThread.WaitOne(-1);

                while (true)
                {
                    diffuseSpan.Clear();
                    lightSpan.Clear();

                    // ... and go!

                    // pick up a job.
                    int job = Interlocked.Decrement(ref jobs);

                    // are we already done?
                    if (job < 0)
                    {
                        // reset our selves, tell the application we're done.
                        signalToMain[threadNumber].Set();

                        break;
                    }

                    // Compute our x / y values.
                    int s = height / squareSize;
                    int x = job / s;
                    int y = job - x * s;

                    // example of computing x and y value. Given that:
                    //  - width         = 400
                    //  - height        = 400
                    //  - squareSize    = 10
                    // this results in 1681 jobs.

                    // say that we have job number 801.
                    // then the x component of the square is 801 / (400 / 10) = 20.
                    // from here on, y is equal to the data dat was 'lost' due to integer division (the remainder). In this case, that is the 1.

                    int xi = x * squareSize;
                    int yi = y * squareSize;

                    diffuseWatch.Start();
                    RenderDiffuseSquare(ref diffuseSpan, ref rsc, rngs[threadNumber], xi, yi);
                    diffuseWatch.Stop();

                    lightWatch.Start();
                    RenderLightingSquare(ref lightSpan, ref rsc, rngs[threadNumber], xi, yi);
                    lightWatch.Stop();

                    combineWatch.Start();
                    RenderCombineSquare(ref diffuseSpan, ref lightSpan, xi, yi);
                    combineWatch.Stop();
                }
            }
        }

        private void RenderCombineSquare(ref Span<Color> diffuseBuffer, ref Span<Color> lightingBuffer, int gx, int gy)
        {
            float ratio = 1.0f / tickNumber;

            for (int sy = 0; sy < squareSize; sy++)
            {
                int iy = Math.Min(sy + gy, height - 1);

                for (int sx = 0; sx < squareSize; sx++)
                {
                    int ix = Math.Min(sx + gx, width - 1);

                    outputBuffer[ix + iy * width] = diffuseBuffer[sx + sy * squareSize] * lightingBuffer[sx + sy * squareSize];
                }
            }
        }

        private void RenderDiffuseSquare(ref Span<Color> localBuffer, ref RenderComponentStream rsc, iRandom rng, int gx, int gy)
        {
            // -------------------------------------------------------- \\
            // Make all the required allocations                        \\

            Span<Point2> ros = stackalloc Point2[squareSize * squareSize];

            // -------------------------------------------------------- \\
            // Pre-compute all of the origins.                          \\

            for (int y = 0; y < squareSize; y++)
            {
                for (int x = 0; x < squareSize; x++)
                {
                    int index = x + y * squareSize;

                    float dx = ((float)(gx + x) - width) / width + 0.5f;
                    float dy = ((float)(gy + y) - height) / height + 0.5f;

                    float ox = 1.0f / width * (rng.Float() - 0.5f);
                    float oy = 1.0f / height * (rng.Float() - 0.5f);
                    ros[index] = this.camera.origin + new Vector2((ox + dx) * this.camera.halfExtend.x, (oy + dy) * this.camera.halfExtend.y);
                }
            }

            // -------------------------------------------------------- \\
            // Construct the encapsulating bounding box.                \\

            BoundingBox encapsulatingBoundingBox = ComputeEncapsulatingBoundingBox(
                this.camera, this.width, this.height, gx, gy);

            // -------------------------------------------------------- \\
            // Our default color is white.                              \\

            for (int y = 0; y < squareSize; y++)
            {
                for (int x = 0; x < squareSize; x++)
                {
                    int index = x + y * squareSize;
                    localBuffer[index] = new Color(1.0f, 1.0f, 1.0f);
                }
            }

            // -------------------------------------------------------- \\
            // Retrieve the entities that we may contain any of         \\
            // the points.                                              \\

            RenderComponent[] rcs;
            qt.FindComponents(encapsulatingBoundingBox, out rcs);

            for (int r = 0, l = rcs.Length; r < l; r++)
            {
                RenderComponent rc = rcs[r];
                for (int y = 0; y < squareSize; y++)
                {
                    for (int x = 0; x < squareSize; x++)
                    {

                        // -------------------------------------------------------- \\
                        // Determine if the origin is inside the entity.            \\

                        int index = x + y * squareSize;
                        if (rc.ContainsPoints(ros[index]))
                        { 
                            rc.ContainsPoints(ros[index]);
                            // retrieve the material.
                            Material primitiveMaterial = rc.m;

                            float e = Math.Min(primitiveMaterial.emissive, 1.0f);
                            Color mixed = new Color(
                                e + (1 - e) * primitiveMaterial.diffuse.r,
                                e + (1 - e) * primitiveMaterial.diffuse.g,
                                e + (1 - e) * primitiveMaterial.diffuse.b
                            );

                            localBuffer[index] = mixed;
                        }
                    }
                }
            }
        }

        private void RenderLightingSquare(ref Span<Color> buffer, ref RenderComponentStream rcs, iRandom rng, int gx, int gy)
        {
            // -------------------------------------------------------- \\
            // Make all the required allocations                        \\

            Span<Point2> ros = stackalloc Point2[squareSize * squareSize];
            Span<float> rts = stackalloc float[squareSize * squareSize];
            Span<Vector2> rds = stackalloc Vector2[squareSize * squareSize];
            Span<int> ris = stackalloc int[squareSize * squareSize];

            // -------------------------------------------------------- \\
            // Pre-compute all of the origins.                          \\

            for (int y = 0; y < squareSize; y++)
            {
                for (int x = 0; x < squareSize; x++)
                {
                    int index = x + y * squareSize;

                    float dx = ((float)(gx + x) - width) / width + 0.5f;
                    float dy = ((float)(gy + y) - height) / height + 0.5f;

                    float ox = 1.0f / width * (rng.Float() - 0.5f);
                    float oy = 1.0f / height * (rng.Float() - 0.5f);
                    ros[index] = this.camera.origin + new Vector2((ox + dx) * this.camera.halfExtend.x, (oy + dy) * this.camera.halfExtend.y);
                }
            }

            // -------------------------------------------------------- \\
            // Construct the encapsulating bounding box.                \\

            BoundingBox rayEncapsulatingBB = ComputeEncapsulatingBoundingBox(
                this.camera, this.width, this.height, gx, gy);

            // -------------------------------------------------------- \\
            // Loop over primitives -> lights -> rays                   \\

            Span<RenderComponent> lightSources = hierarchy.GetLightSources;
            for (int i = 0, l = lightSources.Length; i < l; i++)
            {
                RenderComponent lightComponent = lightSources[i];
                BoundingBox lightEncapsulatingBB = lightComponent.ComputeBoundingBox();
                Material lightMaterial = lightComponent.m;

                // -------------------------------------------------------- \\
                // Pre-compute all distances and directions to the light.   \\

                for (int y = 0; y < squareSize; y++)
                {
                    for (int x = 0; x < squareSize; x++)
                    {
                        int index = x + y * squareSize;

                        Point2 rayOrigin = ros[index];
                        Point2 lightOrigin = lightComponent.c.RandomLocationInPrimitive(rng);
                        Ray lightRay = new Ray(rayOrigin, rayOrigin.DirectionTo(lightOrigin).Normalized());
                        lightRay.Intersect(lightComponent.c);

                        rds[index] = lightRay.direction;
                        rts[index] = lightRay.t - 0.01f;
                    }
                }

                // -------------------------------------------------------- \\
                // Compute all primitives to which we can possibly          \\
                // intersect.                                               \\

                ris.Clear();
                rcs.Clear();
                BoundingBox generalEncapsulatingBB = BoundingBox.Combine(rayEncapsulatingBB, lightEncapsulatingBB);
                qt.FindComponents(generalEncapsulatingBB, ref rcs);

                // -------------------------------------------------------- \\
                // Determine hit / miss                                     \\

                for (int j = 0; j < rcs.UsedCapacity; j += Constants.numberOfSIMDLanes)
                {
                    RenderComponentPack rcp = rcs.ExtractFromStream(j);
                    for (int y = 0; y < squareSize; y++)
                    {
                        for (int x = 0; x < squareSize; x++)
                        {
                            int index = x + y * squareSize;
                            if (ris[index] == 0)
                            {
                                RayPack sceneRay = new RayPack(new Ray(ros[index], rds[index], rts[index]));
                                System.Numerics.Vector<int> intersectData = sceneRay.Intersect(rcp.cps);
                                ris[index] += System.Numerics.Vector.Dot(intersectData, intersectData);
                            }
                            else
                            { continue; }
                        }
                    }
                }

                // -------------------------------------------------------- \\
                // Compute the influence of the light.                      \\

                for (int y = 0; y < squareSize; y++)
                {
                    for (int x = 0; x < squareSize; x++)
                    {
                        int index = x + y * squareSize;
                        if (ris[index] == 0)
                        {
                            float t = rts[index] + 1.0f;
                            float attenuation = 2.0f / (t * t);
                            float strength = attenuation * lightMaterial.emissive;

                            buffer[index] += strength * lightMaterial.diffuse;
                        }
                    }
                }
            }
        }

        private BoundingBox ComputeEncapsulatingBoundingBox(Camera camera, int width, int height, int gx, int gy)
        {
            float oxm = 1.0f / width * 0.5f;
            float dxMin = ((float)(gx + 0) - width) / width + 0.5f;
            float xMin = camera.origin.x + (dxMin - oxm) * camera.halfExtend.x;
            float dxMax = ((float)(gx + squareSize) - width) / width + 0.5f;
            float xMax = camera.origin.x + (dxMax + oxm) * camera.halfExtend.x;

            float oym = 1.0f / height * 0.5f;
            float dyMin = ((float)(gy + 0) - height) / height + 0.5f;
            float yMin = camera.origin.y + (dyMin - oym) * camera.halfExtend.y;
            float dyMax = ((float)(gy + squareSize) - height) / height + 0.5f;
            float yMax = camera.origin.y + (dyMax + oym) * camera.halfExtend.y;

            return new BoundingBox(
                new Range(xMin, xMax),
                new Range(yMin, yMax)
                );
        }
    }
}