﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Numerics;

namespace Raytracer2
{
    internal static class Constants
    {

#if DEBUG
        internal const int numberOfSIMDLanes = 4;
#else
        internal const int numberOfSIMDLanes = 8;
#endif

    }
}
