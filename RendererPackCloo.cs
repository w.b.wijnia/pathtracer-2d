﻿using System;
using System.Runtime;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.IO;

using System.Diagnostics;

using Cloo;
using Cloo.Bindings;

using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK;

using Raytracer2.Scalar;
using Raytracer2.Scalar.Primitives;
using Raytracer2.Scalar.Acceleration;
using Raytracer2.Cloo.Pack;

namespace Raytracer2
{
    internal class RendererCloo : Renderer
    {
        int light = 11;
        /*
             // static arguments
            __write_only image2d_t pixels,

            // dynamic arguments
            const struct Camera camera,                                 // represents the camera.
            __constant const struct Circle* circles, int numCircles,    // represents the circles in the scene.
            __constant const struct int* emissiveCircles, int numEmissiveCircles,
            __constant const struct Material* materials, int numMaterials,

            const int r1,                                               // represents a random value. Used for the rng seed.
            const int r2,                                               // represents a random value. Used for the rng seed.
            const int tickNumber  
         */

        // represents the argument positions within the kernel.
        private const int clKernel_castRays_pixels = 0;
        private const int clKernel_castRays_camera = 1;
        private const int clKernel_castRays_circles = 2;
        private const int clKernel_castRays_numCircles = 3;
        private const int clKernel_castRays_emissiveCircles = 4;
        private const int clKernel_castRays_numEmissiveCircles = 5;
        private const int clKernel_castRays_materials = 6;
        private const int clKernel_castRays_numMaterials = 7;
        private const int clKernel_castRays_BVHNodesGlobal = 8;
        private const int clKernel_castRays_NumBVHNodes = 9;
        private const int clKernel_castRays_r1 = 10;
        private const int clKernel_castRays_r2 = 11;
        private const int clKernel_castRays_tickNumber = 12;
        private const int clKernel_castRays_BVHNodesLocal = 13;

        private static bool initializedBefore = false;

        private iRandom rng;

        private ComputeBuffer<int> bufferEmissiveCircles;
        private ComputeBuffer<ClooMaterial> bufferMaterial;
        private ComputeBuffer<CircleCloo> bufferCircles;
        private ComputeBuffer<BoundingNode> bufferBVHNodesGlobal;

        ComputeImage2D imageCombine;
        ComputeImage2D imageColor;
        ComputeImage2D imageIllumination;
        float[] outputData;

        private ComputeContext context;
        private ComputePlatform platform;
        private ComputeCommandQueue queue;
        
        private ComputeProgram program;

        private KernelManager kernels;

        Stopwatch bvhConstruction = new Stopwatch();

        [System.Runtime.InteropServices.DllImport("opengl32", SetLastError = true)]
        static extern IntPtr wglGetCurrentDC();

        /// <summary>
        /// Constructs a new camera with a resolution of w(idth) * h(eight). 
        /// </summary>
        public RendererCloo(int w, int h, BoundingVolumeHierarchy<Circle> p, int rays)
            : base(w, h)
        {
            if (!RendererCloo.initializedBefore)
            { RendererCloo.initializedBefore = true; }
            else
            { throw new Exception("Cloo renderer has already been initialised before."); }

            rng = new RXor(5);

            // ------------------------------------------------------- \\
            // Construct the context.                                  \\

            platform = ComputePlatform.Platforms[0];

            // retrieve the OpenGL handles and initialise the OpenCL context.
            IntPtr glHandle = (GraphicsContext.CurrentContext as IGraphicsContextInternal).Context.Handle;
            IntPtr wglHandle = wglGetCurrentDC();

            ComputeContextNotifier notify = new ComputeContextNotifier(Notify);

            // http://www.cs.uu.nl/docs/vakken/b3cc/Bron/Con11OpenCL2.pdf
            ComputeContextProperty prop1 = new ComputeContextProperty(ComputeContextPropertyName.Platform, platform.Handle.Value);
            ComputeContextProperty prop2 = new ComputeContextProperty(ComputeContextPropertyName.CL_GL_CONTEXT_KHR, glHandle);
            ComputeContextProperty prop3 = new ComputeContextProperty(ComputeContextPropertyName.CL_WGL_HDC_KHR, wglHandle);
            ComputeContextPropertyList cpl = new ComputeContextPropertyList(new ComputeContextProperty[] { prop1, prop2, prop3 });
            context = new ComputeContext(ComputeDeviceTypes.Gpu, cpl, notify, IntPtr.Zero);

            // ------------------------------------------------------- \\
            // Construct the images for input and output.              \\

            int diffuseID = GL.GenTexture();
            int illuminationID = GL.GenTexture();
            screenid = GL.GenTexture();

            outputData = new float[w * h * 4];
            imageCombine = ConstructImage(screenid, w, h);
            imageColor = ConstructImage(diffuseID, w, h);
            imageIllumination = ConstructImage(illuminationID, w, h);
            outputData = null;

            screenid = illuminationID;


            // ------------------------------------------------------- \\
            // Construct the queue.                                    \\

            // Initialise OpenCL.
            // create a command queue with first gpu found
            queue = new ComputeCommandQueue(context,
            context.Devices[0], ComputeCommandQueueFlags.None);

            // ------------------------------------------------------- \\
            // Load and compile the program.                           \\

            string clSource = $"#define numberOfRays {rays}";

            string[] files = Directory.GetFiles("Kernels/Source/");
            foreach(string file in files)
            {
                using (StreamReader r = new StreamReader(file))
                { clSource += r.ReadToEnd(); }
            }

            // create program with opencl source
            program = new ComputeProgram(context, clSource);

            // compile opencl source
            try
            { program.Build(null, null, null, IntPtr.Zero); }
            catch
            {
                Console.WriteLine(AddLineNumbers(clSource));
                Console.WriteLine(program.GetBuildLog(program.Devices[0]));
            }

            // ------------------------------------------------------- \\
            // Load and compile the kernels.                           \\

            // load chosen kernel from program
            ComputeKernel light = program.CreateKernel("computeLight");
            ComputeKernel diffuse = program.CreateKernel("computeDiffuse");
            ComputeKernel combine = program.CreateKernel("computeCombine");

            kernels = new KernelManager(diffuse, light, combine);
            kernels.Initialise(context, imageColor, imageIllumination, imageCombine);
        }

        private ComputeImage2D ConstructImage(int id, int w, int h)
        {
            GL.BindTexture(TextureTarget.Texture2D, id);

            // Tell OpenGL how to use thet exture.
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Nearest);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Nearest);

            // Initialise the OpeNGL texture.
            GL.TexImage2D(
                TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba32f,
                w, h, 0,
                PixelFormat.Rgb, PixelType.Float,
                outputData
                );

            // Bind an OpenCL compute image to the texture.
            return ComputeImage2D.CreateFromGLTexture2D(
                context,                            // the context we live in (constructed beforehand).
                ComputeMemoryFlags.ReadWrite,       // what type of operations we want. Take note that we do not tell where the texture is.
                (int)TextureTarget.Texture2D,       // the type of texture.
                0,                                  // the mip level ... ?
                id                       // the OpenGL texture ID (made by GL.GenTexture())
            );
        }

        static void Notify(string Message, IntPtr p1, IntPtr p2, IntPtr p3)
        {
            Console.WriteLine("----------------------------");
            Console.WriteLine(Message);
        }

        public override void Render(Camera camera, Hierarchy hierarchy)
        {

            // ------------------------------------------------------- \\
            // BVH construction                                        \\

            bvhConstruction.Start();
            BoundingVolumeHierarchy<Circle> bvh = new BoundingVolumeHierarchy<Circle>(hierarchy.GetOccluders);
            bvhConstruction.Stop();
            Diagnostic.TimeAccelerationStructure = bvhConstruction.ElapsedMilliseconds;
            bvhConstruction.Reset();

            // ------------------------------------------------------- \\
            // Gather data and updat the kernels.                      \\

            // construct the camera
            ClooCamera clooCamera = new ClooCamera(camera.origin, camera.halfExtend);

            // gather materials
            ClooMaterial[] clooMaterials = new ClooMaterial[] { };
            for (int j = 0, l = clooMaterials.Length; j < l; j++)
            { clooMaterials[j] = new ClooMaterial(/**/); }

            RenderComponent[] rcs = bvh.efficientPrimitives;
            RenderComponentCloo[] clooRcs = new RenderComponentCloo[rcs.Length];
            for(int j = 0, l = rcs.Length; j < l; j++)
            { clooRcs[j] = new RenderComponentCloo(rcs[j]); }

            // gather bvh structure
            BoundingNode[] nodes = bvh.nodePool;
            BoundingNodeCloo[] clooNodes = new BoundingNodeCloo[nodes.Length];
            for(int j = 0, l = nodes.Length; j < l; j++)
            { clooNodes[j] = new BoundingNodeCloo(nodes[j]); }

            // gather the random data
            System.Numerics.Vector4 rngs1 = 1889.0f * new System.Numerics.Vector4(rng.Float(), rng.Float(), rng.Float(), rng.Float());
            System.Numerics.Vector4 rngs2 = 2297.0f * new System.Numerics.Vector4(rng.Float(), rng.Float(), rng.Float(), rng.Float());

            kernels.Update(queue, clooMaterials, clooRcs, clooNodes, clooCamera, rngs1, rngs2);
            queue.Finish();

            // ------------------------------------------------------- \\
            // Render things!                                          \\

            List<ComputeMemory> c = new List<ComputeMemory>() { imageCombine, imageColor, imageIllumination };
            queue.AcquireGLObjects(c, null);

            ComputeEventList evList = new ComputeEventList();
            //queue.Execute(kernels.diffuse, new long[] { 0 }, new long[] { width, height }, new long[] { 20, 20 }, null);
            queue.Execute(kernels.illumination, new long[] { 0 }, new long[] { width >> 1, height >> 1 }, new long[] { 10, 10 }, null);

            //queue.Execute(kernels.combine, new long[] { 0 }, new long[] { width, height }, new long[] { 20, 20 }, evList);
            queue.Finish();
            queue.ReleaseGLObjects(c, null);
        }

        private String AddLineNumbers(String raw)
        {
            string[] data = raw.Split('\n');
            string[] output = new string[data.Length];

            for (int j = 0, l = data.Length; j < l; j++)
            { output[j] = $"{j.ToString("000")}: " + data[j]; }

            return string.Join("\r\n", output);
        }
    }

    internal readonly struct ClooCamera
    {
        internal readonly Vector4 orx;
        internal readonly Vector4 ory;

        internal readonly Vector4 ofx;
        internal readonly Vector4 ofy;

        public ClooCamera(Point2 origin, Scalar.Vector2 offset)
        {
            this.orx = new Vector4(origin.x);
            this.ory = new Vector4(origin.y);
            this.ofx = new Vector4(offset.x);
            this.ofy = new Vector4(offset.y);
        }
    }
}