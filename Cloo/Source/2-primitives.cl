
int4 RayToCircleIntersection(struct Ray* r, struct Circle cir)
{
    // -------------------------------------------------------- \\
    // Store some constants that are used later.                \\
    
    const float4 zeros = (float4)(0.0f);
    const float4 twos = (float4)(2.0f);
    const float4 fours = (float4)(4.0f);

    // -------------------------------------------------------- \\
    // Compute collision through ABC-formula.                   \\

    float4 p = (*r).ox - cir.ox;
    float4 q = (*r).oy - cir.oy;

    // hypot: Compute the value of the square root of x^2 + y^2 without undue overflow or underflow
    float4 a = (*r).dx * (*r).dx + (*r).dy * (*r).dy;
    float4 b = twos * p * (*r).dx + twos * q * (*r).dy;
    float4 c = p * p + q * q - cir.r * cir.r;

    float4 dSqrt = b * b - fours * a * c;

    // -------------------------------------------------------- \\
    // Possible early branch out.                               \\

    int4 maskCollision = dSqrt > zeros;
    
    if(!any(maskCollision))
    { return maskCollision; }   

    // -------------------------------------------------------- \\
    // Compute the smallest t's, only select the t's that are   \\
    // considered to be valid.                                  \\

    float4 D = sqrt(dSqrt);
    float4 tm = ((-b) - D) / (twos * a);

    int4 maskInScope = (tm > zeros) & (tm < (*r).t);
    int4 mask = maskCollision & maskInScope;

    (*r).t = select((*r).t, tm, mask);

    return mask;
}

int4 RayToCircleIntersections(
    __global struct RenderComponent* rcs, int numRcs, 
    struct Ray* r
    )
{
    int4 out = (int4)(0);

    for(int j = 0, l = numRcs; j < l; j++)
    {

        // -------------------------------------------------------- \\
        // Go over all the render components. Store the updated     \\
        // values inside out. If all values of out are set, return. \\

        struct RenderComponent rc = rcs[j];
        struct Circle c = rc.circle;
        out = out & RayToCircleIntersection(r, c);

        if(all(out))
        { return out; }
    }

    return out;
}

int4 CircleContainsPoint(struct Circle c, float4 px, float4 py)
{ 
    float4 dx = c.ox - px;
    float4 dy = c.oy - py;
    float4 d = hypot(dx, dy);

    return d < (c.r * c.r);
}

int4 CirclesContainPoint(
    __global struct RenderComponent* rcs, int numRcs, 
    float4 px, float4 py,
    int4* indices)
{
    const int4 zeros = (int4)(0);
    int4 output = (int4)(0);
    
    for(int j = 0, l = numRcs; j < l; j++)
    {
        
        // -------------------------------------------------------- \\
        // Go over all the render components. Keep track of the     \\
        // render components that the points have hit inside        \\
        // recom. If all values of recom have been set, return.     \\

        struct RenderComponent rc = rcs[j];
        struct Circle c = rc.circle;

        int4 localOutput = CircleContainsPoint(c, px, py);
        (*indices) = select((*indices), (int4)(j), localOutput);
        output = output | localOutput;
        
        if(all(output))
        { return output; }
    }

    return output;
}

void RandomPointInCircle(
    // input
    struct Circle* c, struct RXOr* rng,
    // output 
    float4* rx, float4* ry)
{
    const float4 pointFive = (float4)(0.5f);
    (*rx) = (*c).ox + (*c).r * (GenerateRandomFloat4(rng) - pointFive);
    (*ry) = (*c).oy + (*c).r * (GenerateRandomFloat4(rng) - pointFive);
}