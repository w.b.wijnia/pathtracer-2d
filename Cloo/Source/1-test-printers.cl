
void WriteOutFloat4(float4 f4)
{ printf("(%1.4f, %1.4f, %1.4f, %1.4f)\r\n", f4.x, f4.y, f4.z, f4.w); }

void WriteOutInt4(int4 i4)
{ printf("(%i, %i, %i, %i)\r\n", i4.x, i4.y, i4.z, i4.w); }