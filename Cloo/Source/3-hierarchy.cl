
int4 RayToBoundingBoxIntersection(struct Ray r, struct BoundingBox bb, float4* t)
{
    const float4 maxfloat = (float4)(MAXFLOAT);
    const float4 zeros = (float4)(0.0f);
    const float4 ones = (float4)(1.0f);

    // ------------------------------------------------------- \\
    // compute the collision.                                  \\

    float4 dirfracx = ones / r.dx;
    float4 dirfracy = ones / r.dy;

    float4 t1 = (bb.rx.min - r.ox) * dirfracx;
    float4 t2 = (bb.rx.max - r.ox) * dirfracx;
    float4 t3 = (bb.ry.min - r.oy) * dirfracy;
    float4 t4 = (bb.ry.max - r.oy) * dirfracy;

    float4 tmin = fmax(fmin(t1, t2), fmin(t3, t4));
    float4 tmax = fmin(fmax(t1, t2), fmax(t3, t4));

    int4 lessThanZeroMax = tmax > zeros;
    int4 greaterMask = tmin < tmax;
    int4 combinedMask = lessThanZeroMax | greaterMask;

    (*t) = select(maxfloat, tmin, combinedMask);
    return combinedMask;
}

bool NodeIsLeaf(struct BVHNode* node)
{ return (*node).left == (-1); }

int4 TraverseBVH (
    __global struct RenderComponent* rcs,
    __global struct BVHNode* nodes,
    struct Ray* r
    )
{
    // -------------------------------------------------------- \\
    // Initialize the search!                                   \\

    int4 collided = (int4)(0);

    struct StackInteger s;
    PushStackInteger(&s, 0);

    // -------------------------------------------------------- \\
    // Start moving through the BVH, node per node.             \\

    while(!IsEmptyStackInteger(&s))
    {

        // -------------------------------------------------------- \\
        // Retrieve the bounding box, determine if we're hitting    \\
        // the bounding box. Take note: does not update the t's     \\
        // inside the ray struct.                                   \\

        int index = PopStackInteger(&s);
        struct BVHNode node = nodes[index];
        struct BoundingBox boundingbox = node.bb;

        float4 tbb;
        int4 collidesWithRay = 
            RayToBoundingBoxIntersection(
                (*r), boundingbox, &tbb
            );

        // -------------------------------------------------------- \\
        // Determines if we _any_ of the rays hit the boundingbox   \\
        // at a desireable distance.                                \\

        int4 anyTSmaller = tbb < (*r).t;
        int4 mask = collidesWithRay & anyTSmaller;
        if(any(mask))
        {
            if(NodeIsLeaf(&node))
            {

                // -------------------------------------------------------- \\
                // We're dealing with a leaf! Go through all the primitives \\
                // that reside within this leaf. If we're saturated,        \\
                // then we branch out early.                                \\

                for(int j = node.first, l = node.first + node.count; j < l; j++)
                {
                    struct RenderComponent rc = rcs[j];
                    struct Circle c = rc.circle;

                    collided = collided | RayToCircleIntersection(r, c);
                    if(all(collided))
                    { return collided; }
                }
            }
            else
            {
                PushStackInteger(&s, node.left);
                PushStackInteger(&s, node.left + 1);
            }
        }
    }

    return collided;
}