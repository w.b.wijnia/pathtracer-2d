
void PushStackInteger(struct StackInteger* s, int i)
{
    if((*s).head >= stackSize)
    { 
        //printf("Stack is exceeding %i. Behaviour is undefined.", stackSize);
        return; 
    }

    (*s).stack[(*s).head] = i;
    (*s).head = (*s).head + 1;
}

int PopStackInteger(struct StackInteger* s)
{
    if((*s).head < 0)
    {
        //printf("Stack being popped while empty. Behaviour is undefined.", stackSize);
        return -1;
    }

    (*s).head = (*s).head - 1;
    int v = (*s).stack[(*s).head];
    return v;
}

bool IsEmptyStackInteger(struct StackInteger* s)
{ return (*s).head == 0; }


