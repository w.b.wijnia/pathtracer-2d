
kernel void computeCombine(
    __read_only image2d_t diffuse,
    __read_only image2d_t illumination,

    __write_only image2d_t output
    )
{
    // -------------------------------------------------------- \\
    // Determine image x / y and dimensions                     \\

    int x = get_global_id(0);
    int y = get_global_id(1);

    // -------------------------------------------------------- \\
    // Compute coordinates, combine the different images.       \\

    int2 uv = (int2)(x, y);

    float4 c = read_imagef(diffuse, uv);
    float4 i = read_imagef(illumination, uv);

    float4 r = c * i;
    write_imagef(output, uv, r);
}