

kernel void computeDiffuse(
    __write_only image2d_t pixels,

    // dynamic arguments
    const struct Camera camera,                                     // represents the camera.
    __global struct RenderComponent* rcs, int numRcs,        // represents the circles in the scene.
    __global struct Material* materials, int numMaterials,

    // represents the BVH hierarchy.
    __global struct BVHNode* gNodes, uint numNodes,

    float4 r1,                                               // represents a random value. Used for the rng seed.
    float4 r2                                               // represents a random value. Used for the rng seed.
    )
    {

    // ------------------------------------------------------- \\
    // -- Determine image x / y and dimensions              -- \\

    int4 ivw = (int4)get_image_width(pixels);
    int4 ivh = (int4)get_image_height(pixels);
    float4 vw = convert_float4(ivw);
    float4 vh = convert_float4(ivh);

    float4 vx, vy;
    int4 gi; 

    ComputePixelIndices4(
        ivw, ivh,
        &vx, &vy,
        &gi
        );

    // ------------------------------------------------------- \\
    // -- Construct the random number generator (rng)       -- \\

    struct RXOr rng;
    rng.seed = 
        convert_uint4(
            ((vx + vy) * vx + (vx - vy) * vy + (float4)(1)) * r1 + 
            (83.0f, 89.0f, 17.0f, 19.0f) * r2
        );

    // ------------------------------------------------------- \\
    // -- Compute the origin of the pixels.                  -- \\

    float4 rox, roy;
    ComputeOrigin4(
        camera, &rng, vx, vy, vw, vh,
        &rox, &roy
    );

    // -------------------------------------------------------- \\
    // Compute the (base) color of the pixel.                   \\

    // determine our base (material) color.
    float4 r = (float4)(1.0f);
    float4 g = (float4)(1.0f);
    float4 b = (float4)(1.0f);

    int4 indices;
    int4 mask = CirclesContainPoint(rcs, numRcs, rox, roy, &indices);
    if(any(mask))
    { 
        __private int indicesArray[4];
        vstore4(indices, (uint)0, indicesArray);
        __private int maskArray[4];
        vstore4(mask, (uint)0, maskArray);

        for(int j = 0, l = 4; j < l; j++)
        {
            if(maskArray[j] == 0)
            { continue; }

            struct Material m = materials[rcs[indicesArray[j]].material];

            r = mix(m.r, r, m.e);
            g = mix(m.g, g, m.e);
            b = mix(m.b, b, m.e);
        }
    }

    write_imagef(pixels, (int2)(vx.x, vy.x), (float4)(r.x, g.x, b.x, 1.0f));
    write_imagef(pixels, (int2)(vx.y, vy.y), (float4)(r.y, g.y, b.y, 1.0f));
    write_imagef(pixels, (int2)(vx.z, vy.z), (float4)(r.z, g.z, b.z, 1.0f));
    write_imagef(pixels, (int2)(vx.w, vy.w), (float4)(r.w, g.w, b.w, 1.0f));
}