
void ComputeOrigin4(
    // input
    struct Camera camera, struct RXOr* rng, float4 vx, float4 vy, float4 vw, float4 vh,
    // output
    float4* ox, float4* oy 
    )
{
    const float4 pointFive = (float4)(0.5f);
    const float4 ones = (float4)(1.0f);

    float4 x1 = (vx - vw) / vw + pointFive;
    float4 y1 = (vy - vh) / vh + pointFive;

    float4 x2 = (ones / vw) * (GenerateRandomFloat4(rng) - pointFive);
    float4 y2 = (ones / vh) * (GenerateRandomFloat4(rng) - pointFive);

    (*ox) = camera.orx + (x1 + x2) * camera.ofx;
    (*oy) = camera.ory + (y1 + y2) * camera.ofy;
}

void ComputePixelIndices4(
    int4 ivw, int4 ivh,
    float4* vx, float4* vy,
    int4* ig
) 
{
    int sx = get_local_size(0);
    int sy = get_local_size(1);

    int lx = get_local_id(0);
    int ly = get_local_id(1);
    
    int gx = get_group_id(0);
    int gy = get_group_id(1);

    int x = 2 * (gx * sx + lx);
    int y = 2 * (gy * sy + ly);

    int4 ivx = (int4)(x, x + 1, x, x + 1);
    int4 ivy = (int4)(y, y, y + 1, y + 1);

    (*ig) = ivx + (y * ivw);
    (*vx) = convert_float4(ivx);
    (*vy) = convert_float4(ivy);
}