
kernel void computeLight(    
    __write_only image2d_t pixels,

    // dynamic arguments
    const struct Camera camera,                                     // represents the camera.
    __global struct RenderComponent* rcs, int numRcs,        // represents the circles in the scene.
    __global struct Material* materials, int numMaterials,

    // represents the BVH hierarchy.
    __global struct BVHNode* bvhs, uint numNodes,

    float4 r1,                                               // represents a random value. Used for the rng seed.
    float4 r2                                                // represents a random value. Used for the rng seed.
    )
    {

    // ------------------------------------------------------- \\
    // -- Determine image x / y and dimensions              -- \\

    int4 ivw = (int4)get_image_width(pixels);
    int4 ivh = (int4)get_image_height(pixels);
    float4 vw = convert_float4(ivw);
    float4 vh = convert_float4(ivh);

    float4 vx, vy;
    int4 gi; 

    ComputePixelIndices4(
        ivw, ivh,
        &vx, &vy,
        &gi
        );

    // -------------------------------------------------------- \\
    // Construct the random number generator (rng)              \\

    struct RXOr rng;
    rng.seed = 
        convert_uint4(
            ((vx + vy) * vx + (vx - vy) * vy + (float4)(1)) * r1 + 
            (83, 89, 17, 19) * r2
        );

    // ------------------------------------------------------- \\
    // Set some initial / constant values.                     \\

    float4 r = (float4)(0.0f);
    float4 g = (float4)(0.0f);
    float4 b = (float4)(0.0f);

    const float4 ones = (float4)(1.0f);

    // -------------------------------------------------------- \\
    // Compute the origin of our ray.                           \\

    float4 rox, roy;
    ComputeOrigin4(
        camera, &rng, vx, vy, vw, vh,
        &rox, &roy
    );

    // if(indexGlobal == 0)
    // {printf("---------------------------------------");}

    for(int j = 0, l = numRcs; j < l; j++)
    {
        // -------------------------------------------------------- \\
        // Retrieve all the relevant data. Check if this circle     \\
        // is emissive or not. If not, then it's not relevant.      \\

        struct RenderComponent rc = rcs[j];
        struct Circle c = rc.circle;
        struct Material m = materials[rc.material];

        // if(indexGlobal == 0)
        // {WriteOutRenderComponent(rc);}    

        if(m.e.x < 0.01f)
        { continue; }

        // -------------------------------------------------------- \\
        // The rendercomponent is considered to be a lamp. Compute  \\
        // the data for ray(s) that take us to the lamp.            \\

        // compute origin within the light.
        float4 lox, loy;
        RandomPointInCircle(&c, &rng, &lox, &loy);

        // compute the direction from the origin of the ray towards
        // the random origin of the light.
        float4 rdx = lox - rox;
        float4 rdy = loy - roy;
        float4 rds = rsqrt(rdx * rdx + rdy * rdy);
        rdx = rds * rdx;
        rdy = rds * rdy;

        // -------------------------------------------------------- \\
        // Construct the ray to determine the distance from the     \\
        // origin of the ray towards the light.                     \\

        struct Ray lightRay = {
            .ox = rox,
            .oy = roy,
            .dx = rdx,
            .dy = rdy,
            .t = (float4)(MAXFLOAT)
        };

        RayToCircleIntersection(&lightRay, c);

        // -------------------------------------------------------- \\
        // Construct the ray that we'll use to intersect the        \\
        // scene. Value of t is equal to the distance to tbe light. \\

        // todo: reuse lightRay struct? If intersected, color values are not updated
        // and if not intersected, then the original t value is still there.
        struct Ray sceneRay = {
            .ox = rox,
            .oy = roy,
            .dx = rdx,
            .dy = rdy,
            .t = lightRay.t - (float4)(0.00001f)
        };

        int4 occluded = TraverseBVH(rcs, bvhs, &sceneRay);

        // -------------------------------------------------------- \\
        // Compute the impact to the light.                         \\
        // scene. Value of t is equal to the distance to tbe light. \\

        float4 attenuation = ones / ((lightRay.t + ones) * (lightRay.t + ones));
        float4 strength = attenuation * m.e;
        float4 rl = strength * m.r;
        float4 gl = strength * m.g;
        float4 bl = strength * m.b;

        r = select(r + rl, r, occluded);
        g = select(g + gl, g, occluded);
        b = select(b + bl, b, occluded);
    }

    write_imagef(pixels, (int2)(vx.x, vy.x), (float4)(r.x, g.x, b.x, 1.0f));
    write_imagef(pixels, (int2)(vx.y, vy.y), (float4)(r.y, g.y, b.y, 1.0f));
    write_imagef(pixels, (int2)(vx.z, vy.z), (float4)(r.z, g.z, b.z, 1.0f));
    write_imagef(pixels, (int2)(vx.w, vy.w), (float4)(r.w, g.w, b.w, 1.0f));
}