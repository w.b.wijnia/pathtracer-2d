
// https://excamera.com/sphinx/article-xorshift.html
float4 GenerateRandomFloat4(struct RXOr* rng)
{
    (*rng).seed ^= (*rng).seed << 13;
    (*rng).seed ^= (*rng).seed >> 17;
    (*rng).seed ^= (*rng).seed << 5;
    return (convert_float4((*rng).seed)) / ((float4)(UINT_MAX));
}