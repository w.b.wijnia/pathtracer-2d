
void NormalizationTest()
{
    float4 lox = (float4)(0.0f);
    float4 loy = (float4)(3.0f);

    float4 rox = (float4)(0.0f);
    float4 roy = (float4)(1.0f);

    float4 rdx = lox - rox;
    float4 rdy = loy - roy;
    float4 rds = rsqrt(rdx * rdx + rdy * rdy);
    rdx = rds * rdx;
    rdy = rds * rdy;

    printf("xs: ");
    WriteOutFloat4(rdx);
    printf("ys: ");
    WriteOutFloat4(rdy);
}

void RayToCircleTests()
{
    struct Ray r;
    r.ox = (float4)(1.0f);
    r.oy = (float4)(0.0f);
    r.dx = (float4)(-1.0f);
    r.dy = (float4)(0.0f);
    r.t = (float4)(MAXFLOAT);

    struct Circle c;
    c.ox = (float4)(0.0f);
    c.oy = (float4)(0.0f);
    c.r = (float4)(0.25f);

    int4 mask = RayToCircleIntersection(&r, c);
    printf("T's:  ");
    WriteOutFloat4(r.t);
    printf("Mask: ");
    WriteOutInt4(mask);
}

void WriteOutCircle(struct Circle c)
{
    printf("Size of Circle: %i\r\n", sizeof(c));
    WriteOutFloat4(c.ox);
    WriteOutFloat4(c.oy);
    WriteOutFloat4(c.r);
}

void WriteOutRenderComponent(struct RenderComponent rc)
{
    printf("Size of RC: %i\r\n", sizeof(rc));
    WriteOutCircle(rc.circle);
    printf("%i\r\n", rc.material);
    printf("%i\r\n", rc.flags);
}

void WriteOutRange(struct Range r)
{
    printf("Size of range: %i\r\n", sizeof(r));
    printf("r.min: "); WriteOutFloat4(r.min);
    printf("r.max: "); WriteOutFloat4(r.max);
}

void WriteOutBoundingBox(struct BoundingBox bb)
{
    printf("Size of boundingbox: %i\r\n", sizeof(bb));
    WriteOutRange(bb.rx);
    WriteOutRange(bb.ry);
}

void WriteOutBVHNode(struct BVHNode b)
{
    printf("Size of bvhnode: %i\r\n", sizeof(b));
    WriteOutBoundingBox(b.bb);
    printf("left:  %i\r\n", b.left);
    printf("first: %i\r\n", b.first);
    printf("count: %i\r\n", b.count);
}