
// added: #define numberOfRays <value>

// -------------------------------------------------------- \\
// functionality in gens.cl                                 \\

struct RXOr {
    uint4 seed;
};

// -------------------------------------------------------- \\
// functionality in: hierarchy.cl                           \\

struct Range {
    float4 min;
    float4 max;
};

struct BoundingBox{
    struct Range rx;
    struct Range ry;
};

struct BVHNode {
    struct BoundingBox bb;
    int left;
	int first;
    int count;
};

// -------------------------------------------------------- \\
// functionality in: kernel-xxx.cl                          \\
// primitives.cl                                            \\

struct Circle {

    // origin
    float4 ox;
    float4 oy;

    // radia
    float4 r;
};

struct RenderComponent{
    struct Circle circle;
    int material;
    int flags;  
};

struct Ray {

    // origin
    float4 ox;
    float4 oy;

    // direction
    float4 dx;
    float4 dy;

    // 't' value of 'nearest' intersection.
    float4 t;
};

// -------------------------------------------------------- \\
// functionality in: kernel-xxx.cl                          \\

struct Material
{
    float4 r;
    float4 g;
    float4 b;
    float4 e;

    float textureStartX;
    float textureStartY;
    float textureWidth;
    float textureHeight;
};

struct Camera {

    // origin
    float4 orx;
    float4 ory;

    // offset (approaches 'fov')
    float4 ofx;
    float4 ofy;
};

// -------------------------------------------------------- \\
// functionality in: kernel-xxx.cl                          \\
// stack.cl                                                 \\

#define stackSize 64

struct StackInteger {
    int stack[stackSize];
    int head; 
};