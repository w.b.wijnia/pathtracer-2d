struct Range CombineRanges(struct Range a, struct Range b)
{
    float min = a.min < b.min ? a.min : b.min;
    float max = a.max > b.max ? a.max : b.max;

    struct Range r;
    r.min = min;
    r.max = max;
    return r;
}

struct BoundingBox CombineBoundingBoxes(struct BoundingBox a, struct BoundingBox b)
{
    struct BoundingBox bb;
    struct Range rx = CombineRanges(a.rx, b.rx);
    struct Range ry = CombineRanges(a.ry, b.ry);
    
    bb.rx = rx;
    bb.ry = ry;

    return bb;
}

struct BoundingBox BoundingBoxFromCircle(struct Circle circle)
{
    struct BoundingBox bb;
    struct Range rx;
    struct Range ry;

    rx.min = circle.origin.x - circle.r;
    rx.max = circle.origin.x + circle.r;

    ry.min = circle.origin.y - circle.r;
    ry.max = circle.origin.y + circle.r;

    bb.rx = rx;
    bb.ry = ry;

    return bb;
}

float CenterRange(struct Range r)
{ return (r.max - r.min) * 0.5f + r.min; }

float2 CenterBoundingBox(struct BoundingBox bb)
{ 
    float2 center = (float2)(
        CenterRange(bb.rx),
        CenterRange(bb.ry)
    );

    return center;
}

bool ContainsPointRange(struct Range r, float v)
{ return r.min <= v && v <= r.max; }

bool ContainsPointBoundingBox(struct BoundingBox bb, float2 p)
{
    bool rx = ContainsPointRange(bb.rx, p.x);
    bool ry = ContainsPointRange(bb.ry, p.y);
    return rx && ry;
}

bool OverlapRanges(struct Range r1, struct Range r2)
{ return r1.min <= r2.max && r2.min <= r1.max; }

bool OverlapBoundingBoxes(struct BoundingBox b1, struct BoundingBox b2)
{
    bool xOverlap = OverlapRanges(b1.rx, b2.rx);
    bool yOverlap = OverlapRanges(b1.ry, b2.ry);

    return xOverlap && yOverlap;
}

bool ContainsBVH(
    __constant struct Circle* cs,
    __local struct BVHNode* nodes,
    float2 p, int* ps)
{
    struct StackInteger s;
    PushStackInteger(&s, 0);

    // for all nodes.
    while (!IsEmptyStackInteger(&s))
    {
        // find the actual bb, see if we're contained.
        int index = PopStackInteger(&s);
        struct BVHNode node = nodes[index];
        struct BoundingBox boundingbox = node.bb;
        if (ContainsPointBoundingBox(boundingbox, p))
        {
            if (NodeIsLeaf(node))
            {
                // if we are contaned and the node is a leaf, check if we're contained by the primitive.
                int idl = boundingbox.primitive;
                struct Circle c = cs[idl];
                if (CircleContains(c, p))
                {
                    // hooray!
                    (*ps) = idl;
                    return true;
                }
            }
            else
            {
                // if we are contained and the node is not a leaf, continue our journey.
                PushStackInteger(&s, node.left);
                PushStackInteger(&s, node.left + 1);
            }
        }
    }

    return false;
}

// -------------------------------------------------------- \\
// -- Copy applicable portion of the BVH to local memory -- \\

if(indexLocal == 0)
{
    CopyToLocalMemory(  
        gNodes, lNodes, numNodes
    );
}

barrier(CLK_LOCAL_MEM_FENCE);

float2 ComputeOrigin(struct Camera camera, int x, int y, int w, int h, struct RXOr* rng)
{
    float dx = ((float)x - w) / w + 0.5f;
    float dy = ((float)y - h) / h + 0.5f;

    float ox = (1.0f / w) * (GenerateRandomFloat(rng) - 0.5f);
    float oy = (1.0f / h) * (GenerateRandomFloat(rng) - 0.5f);

    return camera.origin + (float2)((ox + dx) * camera.offset.x, (oy + dy) * camera.offset.y);
}