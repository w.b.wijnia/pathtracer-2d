﻿
using System.Runtime.InteropServices;

using Raytracer2.Scalar;

namespace Raytracer2.Cloo.Pack
{
    [StructLayout(LayoutKind.Sequential, Size =64)]
    public struct RenderComponentCloo
    {
        /// <summary>
        /// Represents the primitive that this graphics element contains.
        /// </summary>
        public CircleCloo circle;

        /// <summary>
        /// Represents the material used by this primitve.
        /// </summary>
        public ClooMaterial material;

        /// <summary>
        /// Represents a set of visual effects.
        /// 1 = Cast shadows            (lighting kernel)
        /// 2 = Illuminates             (lighting kernel)
        /// 3 = Visible to camera       (diffuse kernel)
        /// </summary>
        internal int flags;

        public RenderComponentCloo(RenderComponent rc)
        {
            this.circle = new CircleCloo(rc.c);
            this.material = new ClooMaterial(rc.m);
            flags = 0;
        }
    }
}