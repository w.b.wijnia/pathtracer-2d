﻿
using System.Runtime.InteropServices;

using Raytracer2.Scalar.Acceleration;

namespace Raytracer2.Cloo.Pack
{
    [StructLayout(LayoutKind.Sequential, Size = 80)]
    internal readonly struct BoundingNodeCloo
    {
        public readonly BoundingBoxCloo boundingbox;

        public readonly int left;
        public readonly int first;
        public readonly int count;

        public BoundingNodeCloo(BoundingNode bbn)
        {
            this.boundingbox = new BoundingBoxCloo(bbn.BoundingBox);
            this.left = bbn.Left;
            this.first = bbn.First;
            this.count = bbn.Count;
        }

    }
}
