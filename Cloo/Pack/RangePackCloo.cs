﻿
using System.Numerics;
using System.Runtime.InteropServices;

using Raytracer2.Scalar.Primitives;

namespace Raytracer2.Cloo.Pack
{
    [StructLayout(LayoutKind.Sequential)]
    public readonly struct RangeCloo
    {
        public readonly Vector4 min;
        public readonly Vector4 max;

        public RangeCloo(Range r)
        {
            this.min = new Vector4(r.min);
            this.max = new Vector4(r.max);
        }
    }
}