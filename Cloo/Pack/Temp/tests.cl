void TestRayCircleIntersection(int index)
{
    if(index == 0)
    {
        struct Ray ray;
        ray.origin = (float2)(0, 0);
        ray.direction = (float2)(1, 1);

        struct Circle circle;
        circle.origin = (float2)(4, 8);
        circle.r = 4;

        float t = 0;
        bool intersect = false;

        intersect = CircleIntersects(ray, circle, &t);

        printf("TestRayCircleIntersection\r\n");
        printf("Should be: 1, 4.0000. %d, %f \r\n", intersect, t);
    }
}

void TestPointInCircle(int index)
{
    if(index == 0)
    {
        float2 p1 = (float2)(0, 0);
        float2 p2 = (float2)(4, 5);

        struct Circle circle;
        circle.origin = (float2)(4, 8);
        circle.r = 4;

        bool p1Contained = CircleContains(circle, p1);
        bool p2Contained = CircleContains(circle, p2);

        printf("TestPointInCircle\r\n");
        printf("Should be: 0, 1. %d, %d \r\n", p1Contained, p2Contained);
    }
}

void RunTests(int index)
{
    TestRayCircleIntersection(index);
    TestPointInCircle(index);
}

// void TestInput(    
//     const struct Camera camera,                                 // represents the camera.
//     __constant const struct Circle* circles, int numCircles,    // represents the circles in the scene.
//     __constant const int* emissiveCircles, int numEmissiveCircles,
//     __constant const struct Material* materials, int numMaterials,

//     const int r1,                                               // represents a random value. Used for the rng seed.
//     const int r2,                                               // represents a random value. Used for the rng seed.
//     const int tickNumber                                        // represents the number of frames in which the frame did not change.
//                                                                 // useful for accumalation buffers. Not used as of yet.)
// )
// {
//     printf("r1: %d \r\n", r1);
//     printf("r2: %d \r\n", r2);
//     printf("tn: %d \r\n", tickNumber);

//     printf("bytes: %d \r\n", sizeof(circles[0]));
//     for(int j = 0; j < numCircles; j++)
//     {
//         struct Circle circle = circles[j];
//         printf("Circle %d: (%f, %f), %f, %d \r\n", j, circle.origin.x, circle.origin.y, circle.r, circle.mat_id);
//     }

//     for(int j = 0; j < numEmissiveCircles; j++)
//     {
//         printf("emission circle: %d \r\n", emissiveCircles[j]);
//     }

//     printf("bytes: %d \r\n", sizeof(materials[0]));
//     for(int j = 0; j < numMaterials; j++)
//     {
//         struct Material material = materials[j];
//         printf("Material %d: %f, %f, %f, %f \r\n", j, material.r, material.g, material.b, material.emission);
//     }
// }