﻿
// added: #define numberOfRays <value>

// importa: gens.cl
// imports: primitives.cl
// imports: lights.cl
// imports: tests.cl
// imports materials.cl
// imports boundingbox.cl

// 16 bytes


void CopyToLocalMemory(
    __constant struct BVHNode* gNodes,
    __local struct BVHNode* lNodes,
    uint poolLength
    )
{
    for(int j = 0; j < poolLength; j++)
    {
        lNodes[j] = gNodes[j];
    }

    return;

    // struct BoundingBox combinedBoundingBox = CombineBoundingBoxes(origin, light); 

    // int freeInPool = 1;
    // lNodes[0] = gNodes[0];

    // for(int j = 0; j < freeInPool; j++)
    // {
    //     struct BVHNode root = lNodes[j];
    //     struct BoundingBox rootBoundingBox = root.bb;

    //     if(!IsBVHLeaf(root))
    //     {
    //         bool overlap = OverlapBoundingBoxes(combinedBoundingBox, rootBoundingBox);
    //         if(overlap)
    //         {
    //             lNodes[freeInPool] = gNodes[root.left];
    //             lNodes[freeInPool + 1] = gNodes[root.left + 1];

    //             root.left = freeInPool;
    //             freeInPool += 2;
    //         }
    //         else
    //         {
    //             root.left = 0;
    //         }

    //         lNodes[j] = root;
    //     }
    // }
}

struct BoundingBox ComputeWorkgroupOriginBoundingBox(
    struct Camera camera, 
    int sx, int sy, int gx, int gy,
    int w, int h
    )
{
    float xmin, xmax, ymin, ymax;

    // compute the min and amx values, depending on the given group (size).
    xmin = camera.origin.x + ((float)sx * gx - w) / w + 0.5f;
    xmax = camera.origin.x + ((float)sx * (gx + 1) - w) / w + 0.5f;

    ymin = camera.origin.y + ((float)sy * gy - h) / h + 0.5f;
    ymax = camera.origin.y + ((float)sy * (gy+ 1) - h) / h + 0.5f;

    // compute the maximum possible offset due to the random origin being generated.
    xmin += (1.0f / w) * -0.5f;
    xmax += (1.0f / w) * 0.5f;
    ymin += (1.0f / h) * -0.5f;
    ymax += (1.0f / h) * 0.5f;

    // construct the boundingbox and returns it.
    struct BoundingBox bb;
    struct Range rx, ry;
    rx.min = xmin;
    rx.max = xmax;
    ry.min = ymin;
    ry.max = ymax;
    bb.rx = rx;
    bb.ry = ry;

    return bb;
}

float2 ComputeOrigin(struct Camera camera, int x, int y, int w, int h, struct RXOr* rng)
{
    float dx = ((float)x - w) / w + 0.5f;
    float dy = ((float)y - h) / h + 0.5f;

    float ox = (1.0f / w) * (GenerateRXOr(rng) - 0.5f);
    float oy = (1.0f / h) * (GenerateRXOr(rng) - 0.5f);

    return camera.origin + (float2)((ox + dx) * camera.offset.x, (oy + dy) * camera.offset.y);
}

kernel void castRays(
    // static arguments
    //__read_only image2d_t albedo,
    //__read_only image2d_t normals,
    //__read_only image2d_t opacity,

    __write_only image2d_t pixels,

    // dynamic arguments
    const struct Camera camera,                                     // represents the camera.
    __constant const struct Circle* circles, int numCircles,        // represents the circles in the scene.
    __constant const int* emissiveCircles, int numEmissiveCircles,
    __constant const struct Material* materials, int numMaterials,

    // represents the BVH hierarchy.
    __constant const struct BVHNode* gNodes, uint numNodes,

    const int r1,                                               // represents a random value. Used for the rng seed.
    const int r2,                                               // represents a random value. Used for the rng seed.
    const int tickNumber,                                       // represents the number of frames in which the frame did not change.
                                                                // useful for accumalation buffers. Not used as of yet.

    // all local (workgroup) variables
    __local struct BVHNode* lNodes
    )
    {

    // ------------------------------------------------------- \\
    // -- Determine image x / y and dimensions              -- \\

    int sx = get_local_size(0);
    int sy = get_local_size(1);

    int lx = get_local_id(0);
    int ly = get_local_id(1);
    
    int gx = get_group_id(0);
    int gy = get_group_id(1);

    int x = gx * sx + lx;
    int y = gy * sy + ly;

    int w = get_image_width(pixels);
    int h = get_image_height(pixels);
 
    int indexGlobal = x + (y * w);
    int indexLocal = lx + (ly * sx);

    // ------------------------------------------------------- \\
    // -- Construct the random number generator (rng)       -- \\

    struct RXOr rng;
    rng.seed = ((x + y) * x + (x - y) * y) * r1 + indexGlobal * r2;

    // ------------------------------------------------------- \\
    // -- Compute the bounding box of the workgroup.        -- \\

    // struct BoundingBox workgroupBoundingBox = ComputeWorkgroupOriginBoundingBox(
    //     camera, 
    //     sx, sy, gx, gy, w, h
    // ); 

    // -------------------------------------------------------- \\
    // -- Copy applicable portion of the BVH to local memory -- \\

    if(indexLocal == 0)
    {
        CopyToLocalMemory(  
            gNodes, lNodes, numNodes
        );
    }

    barrier(CLK_LOCAL_MEM_FENCE);

    // ------------------------------------------------------- \\
    // -- Set some initial / constant values.               -- \\

    float4 output = (float4)(0.0f, 0.0f, 0.0f, 0.0f);
    float rayImpact = 1.0f / numberOfRays;

    // ------------------------------------------------------- \\
    // -- Compute the origin / materials of various rays.   -- \\

    float2 origins[numberOfRays];
    struct Material groundMaterials[numberOfRays];

    // ------------------------------------------------------- \\
    // -- Compute the illumination of the pixel.            -- \\

    for(int o = 0; o < numEmissiveCircles; o++)
    {
        // store all information of the light in private memory.
        int eIndex = emissiveCircles[o];
        struct Circle lightPrimitive = circles[eIndex];
        struct Material lightMaterial = materials[lightPrimitive.mat_id];
        struct BoundingBox lightBoundingBox = BoundingBoxFromCircle(lightPrimitive);

        // https://www.khronos.org/registry/OpenCL/sdk/2.1/docs/man/xhtml/attributes-loopUnroll.html (opencl 2.1 only)
        // https://www.khronos.org/registry/OpenCL/extensions/nv/cl_nv_pragma_unroll.txt
        // #pragma unroll
        for(int j = 0; j < numberOfRays; j++)
        {

            // ------------------------------------------------------- \\
            // -- Construct (finalize) the ray                      -- \\

            float2 rayOrigin = origins[j];
            struct Material groundMaterial = groundMaterials[j];

            // finalize the construction of the ray. We need an origin of the light
            // in order to compute the direction.
            float2 lightRandomOrigin = CircleRandomPoint(&lightPrimitive, &rng);
            float2 rayDirection = lightRandomOrigin - rayOrigin;

            struct Ray ray;
            ray.origin = rayOrigin;
            ray.direction = fast_normalize(rayDirection);

            // ------------------------------------------------------- \\
            // -- Compute the smallest distance to the light,       -- \\
            // -- determine if the light is worth computing.        -- \\

            // compute the smallest distance towards the light.
            float lightDistance;
            float lightAttenuation;
            float lightStrength;
            if(CircleContains(lightPrimitive, ray.origin))
            { 
                lightDistance = 0; 
                lightAttenuation = 1;
                lightStrength = lightMaterial.color.w;
            }
            else
            { 
                CircleIntersects(ray, lightPrimitive, &lightDistance); 
                lightAttenuation = 1 / ((lightDistance + 1) * (lightDistance + 1));
                lightStrength = lightMaterial.color.w * lightAttenuation;
            }

            // compute whether this light is 'worth' computing.
            float r = GenerateRXOr(&rng) * 0.5f;    
            if(lightStrength < r*r*r*r)
            { continue; }

            // ------------------------------------------------------- \\
            // -- Compute whether or not the light is occluded.     -- \\

            float t;
            bool intersected = TraverseBVH
            (
                circles, numCircles, 
                lNodes, numNodes,

                ray, &t,  lightDistance, eIndex
            );

            bool occluded = intersected && t > 0 && t < lightDistance;
            if(!occluded)
            {
                // compute the total effect of the light.
                output = output + rayImpact * lightStrength * groundMaterial.color * lightMaterial.color;
            }
        }
    }

    // ------------------------------------------------------- \\
    // -- Write out the pixel.                              -- \\

    //float ratio = 1.0f / tickNumber;
    //float4 prev = read_imagef(pixels, samp, (int2)(x, y));
    //write_imagef(pixels, (int2)(x, y), (1 - ratio) * prev + ratio * output);

    write_imagef(pixels, (int2)(x, y), 2*output);
}