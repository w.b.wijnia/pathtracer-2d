﻿
using System;
using System.Numerics;
using System.Runtime.InteropServices;

using Raytracer2.Scalar;

namespace Raytracer2.Cloo.Pack
{
    [StructLayout(LayoutKind.Sequential)]
    public readonly struct ClooMaterial
    {
        public readonly Vector4 r;

        public readonly Vector4 g;

        // ...
        public readonly Vector4 b;
        public readonly Vector4 e;

        public readonly float textureStartX;
        public readonly float textureStartY;
        public readonly float textureWidth;
        public readonly float textureHeight;

        public ClooMaterial(Material m)
        {
            this.r = new Vector4(m.diffuse.r);
            this.g = new Vector4(m.diffuse.g);
            this.b = new Vector4(m.diffuse.b);
            this.e = new Vector4(m.emissive);

            this.textureStartX = 0;
            this.textureStartY = 0;
            this.textureWidth = 0;
            this.textureHeight = 0;
        }
    }
}