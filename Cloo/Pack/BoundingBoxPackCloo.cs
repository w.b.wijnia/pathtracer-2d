﻿
using System.Runtime.InteropServices;

using Raytracer2.Scalar.Acceleration;
using Raytracer2.Cloo.Pack;

namespace Raytracer2.Cloo.Pack
{
    [StructLayout(LayoutKind.Sequential)]
    public readonly struct BoundingBoxCloo
    {
        public readonly RangeCloo rx;
        public readonly RangeCloo ry;

        public BoundingBoxCloo(BoundingBox bb)
        {
            this.rx = new RangeCloo(bb.rx);
            this.ry = new RangeCloo(bb.ry);
        }
    }
}
