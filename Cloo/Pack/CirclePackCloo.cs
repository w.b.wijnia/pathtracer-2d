﻿using System.Runtime.InteropServices;
using System.Numerics;

using Raytracer2.Scalar.Primitives;

namespace Raytracer2.Cloo.Pack
{
    [StructLayout(LayoutKind.Sequential, Size = 48)]
    public readonly struct CircleCloo
    {
        public readonly Vector4 ox;
        public readonly Vector4 oy;
        public readonly Vector4 r;

        public CircleCloo(Circle other)
        {
            this.ox = new Vector4(other.origin.x);
            this.oy = new Vector4(other.origin.y);
            this.r = new Vector4(other.radius);
        }
    }
}