﻿
using Cloo;

using System;
using System.Numerics;

using Raytracer2.Scalar;
using Raytracer2.Cloo.Pack;

namespace Raytracer2.Cloo.Pack
{
    internal class KernelManager
    {
        internal ComputeKernel diffuse;
        internal ComputeKernel illumination;
        internal ComputeKernel combine;

        private ComputeImage2D diffuseImage;
        private ComputeImage2D illuminationImage;
        private ComputeImage2D combineImage;

        private ComputeBuffer<ClooMaterial> bufferMaterial;
        private ComputeBuffer<RenderComponentCloo> bufferRenderComponents;
        private ComputeBuffer<BoundingNodeCloo> bufferBVHNodes;

        public KernelManager(ComputeKernel diffuse, ComputeKernel illumination, ComputeKernel combine)
        {
            this.diffuse = diffuse;
            this.illumination = illumination;
            this.combine = combine;
        }

        public void Initialise(ComputeContext context, ComputeImage2D diffuseImage, ComputeImage2D illuminationImage, ComputeImage2D combineImage)
        {
            // receive the data.
            this.diffuseImage = diffuseImage;
            this.illuminationImage = illuminationImage;
            this.combineImage = combineImage;

            InitialiseBuffers(context);
            InitialiseKernelDiffuse();
            InitialiseKernelIllumination();
            InitialiseKernelCombine();
        }

        private void InitialiseBuffers(ComputeContext context)
        {
            // Setup the circles.
            if (bufferRenderComponents != null)
            { bufferRenderComponents.Dispose(); }

            bufferRenderComponents = new ComputeBuffer<RenderComponentCloo>(context,
            ComputeMemoryFlags.ReadWrite, 3000);

            // Setup the materials.
            if (bufferMaterial != null)
            { bufferMaterial.Dispose(); }

            bufferMaterial = new ComputeBuffer<ClooMaterial>(context,
            ComputeMemoryFlags.ReadWrite, 1000);

            // setup the BVH structure.
            if (bufferBVHNodes != null)
            { bufferBVHNodes.Dispose(); }

            bufferBVHNodes = new ComputeBuffer<BoundingNodeCloo>(context,
            ComputeMemoryFlags.ReadWrite, 2048);
        }

        private void InitialiseKernelDiffuse()
        {
            // see the source file kernel-diffuse.cl
            diffuse.SetMemoryArgument(0, diffuseImage);
            diffuse.SetValueArgument(1, new ClooCamera(new Point2(0, 0), new Scalar.Vector2(0, 0)));
            diffuse.SetMemoryArgument(2, bufferRenderComponents);
            diffuse.SetValueArgument(3, 0);
            diffuse.SetMemoryArgument(4, bufferMaterial);
            diffuse.SetValueArgument(5, 0);
            diffuse.SetMemoryArgument(6, bufferBVHNodes);
            diffuse.SetValueArgument(7, 0);
            diffuse.SetValueArgument(8, new Vector4(0));
            diffuse.SetValueArgument(9, new Vector4(0));
        }

        private void InitialiseKernelIllumination()
        {
            // see the source file kernel-light.cl
            illumination.SetMemoryArgument(0, illuminationImage);

            illumination.SetValueArgument(1, new ClooCamera(new Point2(0, 0), new Scalar.Vector2(0, 0)));
            illumination.SetMemoryArgument(2, bufferRenderComponents);
            illumination.SetValueArgument(3, 0);
            illumination.SetMemoryArgument(4, bufferMaterial);
            illumination.SetValueArgument(5, 0);
            illumination.SetMemoryArgument(6, bufferBVHNodes);
            illumination.SetValueArgument(7, 0);
            illumination.SetValueArgument(8, new Vector4(0));
            illumination.SetValueArgument(9, new Vector4(0));
        }

        private void InitialiseKernelCombine()
        {
            // see the source file kernel-combine.cl
            combine.SetMemoryArgument(0, diffuseImage);
            combine.SetMemoryArgument(1, illuminationImage);
            combine.SetMemoryArgument(2, combineImage);
        }

        public void Update(ComputeCommandQueue queue, ClooMaterial[] materials, RenderComponentCloo[] rcs, BoundingNodeCloo[] nodes, ClooCamera camera, Vector4 rng1, Vector4 rng2)
        {
            UpdateBuffers(queue, materials, rcs, nodes);
            UpdateDiffuse(materials, rcs, nodes, camera, rng1, rng2);
            UpdateIllumination(materials, rcs, nodes, camera, rng1, rng2);
        }

        private void UpdateBuffers(ComputeCommandQueue queue, ClooMaterial[] materials, RenderComponentCloo[] rcs, BoundingNodeCloo[] nodes)
        {
            queue.WriteToBuffer(rcs, bufferRenderComponents, true, 0, 0, rcs.Length, null);
            queue.WriteToBuffer(materials, bufferMaterial, true, 0, 0, materials.Length, null);
            queue.WriteToBuffer(nodes, bufferBVHNodes, true, 0, 0, nodes.Length, null);
        }

        private void UpdateDiffuse(ClooMaterial[] materials, RenderComponentCloo[] rcs, BoundingNodeCloo[] nodes, ClooCamera camera, Vector4 rng1, Vector4 rng2)
        {
            // diffuse.SetMemoryArgument(0, diffuseImage);
            diffuse.SetValueArgument(1, camera);
            // queue.WriteToBuffer(circles, bufferCircles, true, 0, 0, circles.Length, null);
            diffuse.SetValueArgument(3, rcs.Length);
            // queue.WriteToBuffer(materials, bufferMaterial, true, 0, 0, materials.Length, null);
            diffuse.SetValueArgument(5, materials.Length);
            // queue.WriteToBuffer(nodes, bufferBVHNodesGlobal, true, 0, 0, nodes.Length, null);
            diffuse.SetValueArgument(7, (uint)nodes.Length);
            diffuse.SetValueArgument(8, rng1);
            diffuse.SetValueArgument(9, rng2);
        }

        private void UpdateIllumination(ClooMaterial[] materials, RenderComponentCloo[] rcs, BoundingNodeCloo[] nodes, ClooCamera camera, Vector4 rng1, Vector4 rng2)
        {
            illumination.SetValueArgument(1, camera);
            // queue.WriteToBuffer(circles, bufferCircles, true, 0, 0, circles.Length, null);
            illumination.SetValueArgument(3, rcs.Length);
            // queue.WriteToBuffer(materials, bufferMaterial, true, 0, 0, materials.Length, null);
            illumination.SetValueArgument(5, materials.Length);
            // queue.WriteToBuffer(nodes, bufferBVHNodesGlobal, true, 0, 0, nodes.Length, null);
            illumination.SetValueArgument(7, (uint)nodes.Length);
            illumination.SetValueArgument(8, rng1);
            illumination.SetValueArgument(9, rng2);
        }
    }
}
